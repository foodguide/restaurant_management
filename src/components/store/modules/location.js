import Vue from 'vue'
import { event } from 'vue-analytics'
import LocationRepository from '../repositories/LocationRepository'
import LocationStatisticsRepository from '../repositories/LocationStatisticsRepository'
import { HookId } from './hooks'
import { EventName, EventType } from './event'
import { RepresentationStatus } from '../repositories/LocationRepository'
import StatisticsStorageHelper from '../../helper/StatisticsStorageHelper'

const state = {
  locations: [],
  hasFetchedLocations: false,
}

const getAllRepresentations = (state) => {
  const representations = []
  state.locations.forEach((location) => {
    if (location.representations) {
      location.representations.forEach((representation) => {
        representations.push(representation)
      })
    }
  })
  return representations
}

const getters = {
  getLocationsForCurrentUser() {
    return state.locations
  },
  getLocationById: state => id => state.locations.find(location => location.id === id),
  getUnseenOldReviewsTimestamp: state => id => state.locations.find(location => location.id === id).unseenOldReviewsTimestamp,
  getAllRepresentationForCurrentUser: state => getAllRepresentations(state),
  getUnhealthyRepresentations: state => getAllRepresentations(state).filter((representation) => {
    if (!representation.status) return false
    return representation.status === RepresentationStatus.ACCESSTOKEN_EXPIRED || representation.status === RepresentationStatus.PERMISSION_DECLINED
  }),
}

const mutations = {
  fetchLocations(state, payload) {
    state.locations = payload
    state.hasFetchedLocations = true
  },
  updateLocation(state, payload) {
    const index = state.locations.findIndex(
      location => location.id === payload.id,
    )

    if (index >= 0) {
      Vue.set(state.locations, index, { ...payload })
    } else {
      state.locations.push(payload)
    }
  },
  addLocation(state, payload) {
    state.locations.push(payload)
  },
  addRepresentation(state, payload) {
    const locationIndex = state.locations.findIndex(location => location.id === payload.locationId)
    if (locationIndex < 0) return
    const location = state.locations[locationIndex]
    if (!location.representations) {
      location.representations = []
    }
    const representationIndex = location.representations.findIndex(representation => representation.platform === payload.representation.platform)
    if (representationIndex < 0) {
      location.representations.push(payload.representation)
    } else {
      location.representations[representationIndex] = payload.representation
    }
    Vue.set(state.locations, locationIndex, { ...{}, ...location })
  },
  deleteRepresentation(state, payload) {
    const locationIndex = state.locations.findIndex(location => location.id === payload.location.id)
    if (locationIndex < 0) return
    const location = state.locations[locationIndex]
    if (!location.representations || location.representations.length === 0) {
      Vue.set(state.locations, locationIndex, { ...location, ...{ representations: [] } })
      return
    }
    location.representations = location.representations.filter(({ id }) => id !== payload.representation.id)
    Vue.set(state.locations, locationIndex, { ...{}, ...location })
  },
  deleteLocation(state, payload) {
    const index = state.locations.findIndex(location => location.id === payload.id)
    state.locations.splice(index, 1)
  },
  addUnseenOldReviewsTimestamp(state, payload) {
    const locationIndex = state.locations.findIndex(location => location.id === payload.locationId)
    if (locationIndex < 0) return
    const location = state.locations[locationIndex]
    location.unseenOldReviewsTimestamp = payload.unseenOldReviewsTimestamp
    Vue.set(state.locations, locationIndex, location)
  },
  locationStateReset(state) {
    state.locations = []
    state.hasFetchedLocations = false
  },
}

const locationRepository = new LocationRepository()
const locationStatisticsRepository = new LocationStatisticsRepository()

const actions = {
  fetchLocations(context) {
    return locationRepository.findAllForCurrentUser().then((data) => {
      context.commit('fetchLocations', data)
      context.dispatch('onHookEvent', { hookId: HookId.LocationFetch, payload: { locations: data } })
      return data
    })
  },
  createLocation(context, payload) {
    return locationRepository.create(payload).then((data) => {
      if (data.id) payload.id = data.id
      data.userId = locationRepository.getCurrentUserUid()
      context.commit('addLocation', payload)
      const numberOfLocations = context.getters.getLocationsForCurrentUser.length
      context.dispatch('updateCurrentSubscriptionLocationCount', numberOfLocations)
      event('location', 'create', payload.name, payload.id)
      context.dispatch('writeEvent', {
        name: EventName.CreateLocationFinish,
        type: EventType.Action,
        data: { payload },
      })
      return data
    })
  },
  deleteLocation(context, payload) {
    StatisticsStorageHelper.clear(locationRepository.getCurrentUserUid())
    return locationRepository.delete(payload.id).then(() => {
      context.dispatch('writeEvent', {
        name: EventName.DeleteLocationFinish,
        type: EventType.Action,
        data: { location: payload.id },
      })
      context.dispatch('stopListeningForRepresentations', payload.representations)
      return context.dispatch('deleteLocationReviews', payload).then(() => {
        context.commit('deleteLocation', payload)
        const numberOfLocations = context.getters.getLocationsForCurrentUser.length
        context.dispatch('updateCurrentSubscriptionLocationCount', numberOfLocations)
      })
    })
  },
  deleteRepresentation(context, payload) {
    StatisticsStorageHelper.clear(locationRepository.getCurrentUserUid())
    context.dispatch('stopListeningForRepresentations', [payload.representation])
    return locationRepository.deleteRepresentation(payload.location.id, payload.representation).then(() => context.dispatch('deleteRepresentationReviews', payload.representation).then(() => {
      context.dispatch('writeEvent', {
        name: EventName.DeleteRepresentationFinish,
        type: EventType.Action,
        data: { representationId: payload.representation.id },
      })
      context.commit('deleteRepresentation', payload)
    }))
  },
  addRepresentationToLocation(context, payload) {
    StatisticsStorageHelper.clear(locationRepository.getCurrentUserUid())
    return locationRepository.addRepresentationToLocation(payload.locationId, payload).then((data) => {
      context.dispatch('writeEvent', {
        name: EventName.CreateRepresentationFinish,
        type: EventType.Action,
        data: { payload },
      })
      context.commit('addRepresentation', {
        locationId: payload.locationId,
        representation: data,
      })
      return context.dispatch('startListeningForRepresentation', data).then(() => data)
    })
  },
  addFacebookRepresentationToLocation(context, payload) { // TODO: change to generic addRepresentationToLocation()
    StatisticsStorageHelper.clear(locationRepository.getCurrentUserUid())
    return locationRepository.addFacebookRepresentationToLocation(
      payload.locationId,
      payload.representation,
    ).then((data) => {
      context.commit('addRepresentation', {
        locationId: payload.locationId,
        representation: data,
      })
      return data
    })
  },
  addInstagramRepresentationToLocation(context, payload) { // TODO: change to generic addRepresentationToLocation()
    StatisticsStorageHelper.clear(locationRepository.getCurrentUserUid())
    return locationRepository.addInstagramRepresentationToLocation(
      payload.locationId,
      payload.representation,
    ).then((data) => {
      context.commit('addRepresentation', {
        locationId: payload.locationId,
        representation: data,
      })
      return data
    })
  },
  addIFrameRepresentationToLocation(context, payload) { // TODO: change to generic addRepresentationToLocation()
    StatisticsStorageHelper.clear(locationRepository.getCurrentUserUid())
    return locationRepository.addIFrameRepresentationToLocation(
      payload.locationId,
      payload.url,
      payload.platform,
      payload.platformShortcut,
    )
      .then((data) => {
        context.commit('addRepresentation', {
          locationId: payload.locationId,
          representation: data,
        })
      })
  },
  updateLocation(context, payload) {
    return locationRepository.update(payload.id, payload).then((data) => {
      context.commit('updateLocation', data)
      return data
    })
  },
  updateRepresentation(context, { locationId, representation, data }) {
    return locationRepository.updateRepresentation(locationId, representation, data).then(() => {
      const newRepresentation = { ...{}, ...representation, ...data }
      context.commit('addRepresentation', {
        locationId,
        representation: newRepresentation,
      })
      return newRepresentation
    })
  },
  fetchRepresentationCredentials(context) {
    const credentials = context.getters.getFacebookCredentials
    if (!credentials) return Promise.reject(new Error('No facebook credentials found.'))

    const promises = []
    credentials.forEach((credential) => {
      promises.push(context.dispatch('requestFacebookPages', { credential }).then(pages => context.dispatch('requestFacebookLocationsForPages', pages).then(facebookLocations => context.dispatch('requestInstagramPages', { credential }).then((instaPages) => {
        const locations = context.getters.getLocationsForCurrentUser
        const extract = objects => objects.map(({ access_token, id }) => ({ access_token, id }))

        const fbObjects = [
          ...extract(pages), ...extract(facebookLocations),
        ]

        return Promise.all(locations.map((location) => {
          location.representations.forEach((representation, repIndex) => {
            const fbObject = fbObjects.find(fbObject => fbObject.id === representation.externalId)
            if (fbObject) {
              location.representations[repIndex].accessToken = fbObject.access_token
              location.representations[repIndex].status = RepresentationStatus.ACTIVE
            }
            const instaPage = instaPages.find(instaPage => instaPage.instagram_business_account.id === representation.externalId)
            if (instaPage) {
              location.representations[repIndex].accessToken = instaPage.access_token
              location.representations[repIndex].status = RepresentationStatus.ACTIVE
            }
          })
          return context.dispatch('updateRepresentations', location)
        }))
      }))))
    })
    return Promise.all(promises)
  },
  updateRepresentations(context, payload) {
    return locationRepository.updateRepresentations(payload.id, payload.representations).then(() => {
      context.commit('updateLocation', payload)
      return payload
    })
  },
  fetchRepresentationsForLocation(context, payload) {
    return locationRepository.getRepresentationByLocationId(payload.id).then((data) => {
      payload.representations = data
      context.commit('updateLocation', payload)
      return payload
    })
  },
  fetchStatisticsForLocation(context, payload) {
    return locationStatisticsRepository.getStatisticsByLocationId(payload.id).then((data) => {
      if (data.length > 0) {
        const locationStatistics = data[0]
        payload.avgRating = locationStatistics.avgRating
        payload.avgRatingOld = locationStatistics.avgRatingOld
        context.commit('updateLocation', payload)
      }
    })
  },
  fetchRepresentationsForAllLocations(context, locations) {
    context.dispatch('fetchStatisticsForAllLocations', locations)
    const promises = []
    locations.forEach((location) => {
      promises.push(context.dispatch('fetchRepresentationsForLocation', location))
    })
    return Promise.all(promises)
  },
  fetchStatisticsForAllLocations(context, locations) {
    const promises = []
    locations.forEach((location) => {
      promises.push(context.dispatch('fetchStatisticsForLocation', location))
    })
    return Promise.all(promises)
  },
  writeOldUnseenDialogTimestamp(context, payload) {
    locationRepository.writeUnseenOldReviewsTimestamp(payload, new Date()).then((data) => {
      context.commit('addUnseenOldReviewsTimestamp', data)
    })
  },
}

if (process.env.NODE_ENV !== 'production') {
  actions.setTestLocation = (context, payload) => {
    context.commit('fetchLocations', payload)
  }
}

export default {
  state,
  getters,
  actions,
  mutations,
}

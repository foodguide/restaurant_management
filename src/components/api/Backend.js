import axios from 'axios'

export default class Backend {
  constructor() {
    this.authHeader = null
  }

  get authHeader() {
    return this._authHeader
  }

  set authHeader(authHeader) {
    this._authHeader = authHeader
  }

  onNextIdToken(idToken) {
    this.authHeader = `Bearer ${idToken}`
  }

  getHeaders() {
    return {
      Authorization: this.authHeader,
    }
  }

  getOptions(additionalOptions) {
    return {
      headers: this.getHeaders(),
      baseURL: process.env.BACKEND_BASE_URL,
      ...additionalOptions,
    }
  }

  get status() {
    return {
      check: () => axios.get('status/check', this.getOptions()).then(response => response.data),
      authorization: (userId) => {
        if (this.authHeader !== 'Bearer null') {
          return axios.get('status/authorization', this.getOptions({ params: { userId } })).then(response => response.data)
        }
        return Promise.resolve()
      },
    }
  }

  get user() {
    return {
      subscribe: subscriptions => axios.put('/user/subscription', { subscriptions }, this.getOptions()).then(response => response.data),
      unsubscribe: ({ name, type = 'email' }) => {
        const addOptions = {
          data: { name, type },
        }
        return axios.delete('/user/subscription', this.getOptions(addOptions)).then(response => response.data)
      },
      guestNotificationSubscriptions: ({ userId }) => axios.get('/user/notificationsubscriptions', this.getOptions({ params: { userId } })).then(response => response.data),
      updateGuestNotificationSubscriptions: ({ userId, subscriptions }) => axios.put('/user/updatenotificationsubscriptions', { userId, subscriptions }, this.getOptions()),
    }
  }

  get sync() {
    return {
      api: (platforms) => {
        if (!platforms || platforms.length === 0) return Promise.reject('Given platforms array is null, undefined or empty')
        return axios.put('/sync/api/user', { platforms }, this.getOptions()).then(response => response.data)
      },
      crawler: () => axios.put('/sync/crawler/user', null, this.getOptions()).then(response => response.data),
      crawlerForRepresentation: (representationId, locationId) => axios.put('/sync/crawler/representation', { representationId, locationId }, this.getOptions()),
      apiForLocation: (locationId, platforms) => {
        if (!platforms || platforms.length === 0) return Promise.reject('Given platforms array is null, undefined or empty')
        const params = { locationId, platforms }
        return axios.put('/sync/api/location', params, this.getOptions()).then(response => response.data)
      },
    }
  }

  get facebook() {
    return {
      accessToken: (credential) => {
        const { accessToken } = credential
        return axios.get('/facebook/accesstoken', this.getOptions({ params: { accessToken } }))
      },
      account: (credential) => {
        const { accessToken } = credential
        return axios.get('/facebook/account', this.getOptions({ params: { accessToken } }))
      },
    }
  }

  get gmb() {
    return {
      accounts: credential => axios.get('/gmb/accounts', this.getOptions({ params: credential }))
        .then(response => response.data.accounts),
      locations: credential => axios.get('/gmb/locations', this.getOptions({ params: credential }))
        .then(response => response.data.locations),
      accountsForUser: (userId, credentialId) => axios.get('/gmb/accounts-for-user', this.getOptions({ params: { userId, credentialId } }))
        .then(response => response.data.accounts),
      locationsForUser: (userId, credentialId) => axios.get('/gmb/locations-for-user', this.getOptions({ params: { userId, credentialId } }))
        .then(response => response.data.locations),
    }
  }

  get review() {
    return {
      reply: (review, text) => axios.put('/review/reply', { reviewId: review.id, text }, this.getOptions()),
    }
  }

  get payment() {
    return {
      newSubscriptionUrl: planId => axios.get('/payment/new_subscription_url', this.getOptions({ params: { planId } }))
        .then(response => response.data),
      existingSubscriptionUrl: subscriptionId => axios.get('/payment/existing_subscription_url', this.getOptions({ params: { subscriptionId } }))
        .then(response => response.data),
      updatePaymentUrl: () => axios.get('/payment/update_payment_url', this.getOptions())
        .then(response => response.data),
      portalSession: () => axios.get('/payment/portal_session', this.getOptions())
        .then(response => response.data),
      hasCancelledSubscriptions: () => axios.get('/payment/has_cancelled_subscriptions', this.getOptions())
        .then(response => response.data),
      hasActiveSubscriptions: () => axios.get('/payment/has_active_subscriptions', this.getOptions())
        .then(response => response.data.has_active_subscriptions),
      getActiveSubscriptions: () => axios.get('/payment/active_subscriptions', this.getOptions())
        .then(response => response.data),
      updateSubscription: (subscriptionId, data) => {
        data = {
          subscriptionId,
          ...data,
        }
        return axios.put('/payment/subscription', data, this.getOptions())
          .then(response => response.data)
      },
      upgradeSubscriptionToBasicPlan: subscriptionId => axios.put('/payment/subscription-upgrade', { subscriptionId }, this.getOptions())
        .then(response => response.data),
      getHostedPage: hostedPageId => axios.get('/payment/hosted_page', this.getOptions({ params: { hostedPageId } }))
        .then((response) => {
          const { data } = response
          return data
        }),
    }
  }

  get admin() {
    return {
      getUsers: (nextPageToken = null) => {
        const options = nextPageToken ? this.getOptions({ params: { nextPageToken } }) : this.getOptions()
        return axios.get('/user', options).then(response => response.data)
      },
      updatePipedriveId: (userId, pipedriveId) => {
        const data = { userId, pipedriveId }
        return axios.put('/user/updatepipedriveid', data, this.getOptions()).then(response => response.data)
      },
      bulkUpdateReviews: (data) => {
        const options = this.getOptions()
        return axios.put('/review', data, options).then(response => response.data)
      },
      genericSend: (url) => {
        const options = this.getOptions()
        return axios.get(url, options)
      },
      resyncRepresentation: (data) => {
        const options = this.getOptions()
        return axios.put('/sync/admin/representation', data, options).then(response => response.data)
      },
      bulkDeleteReviews: ({ userId, before, after }) => {
        const addOptions = {
          data: { userId, before, after },
        }
        const options = this.getOptions(addOptions)
        return axios.delete('/review', options).then(response => response.data)
      },
      sendNewsletter: (data) => {
        const options = this.getOptions()
        return axios.put('/email/newsletter', data, options).then(response => response.data)
      },
      directReply: (data) => {
        const options = this.getOptions()
        return axios.put('/review/direct-reply', data, options).then(response => response.data)
      },
    }
  }
}

import ExternalDependencyHelper from 'src/components/helper/ExternalDependencyHelper'
import externalDependencies from '../../../../build/external-dependencies'

describe('ExternalDependencyHelper.js', () => {
  it('should load an external dependency', (done) => {
    const url = externalDependencies.find(dependency => dependency.module === 'chargebeeJs').entry
    ExternalDependencyHelper.resolveGlobalVariable('Chargebee', url, document).then(() => {
      expect(Chargebee).to.exist
      done()
    })
  })
})

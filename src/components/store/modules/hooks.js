/* eslint-disable no-param-reassign */
export const HookId = {
  VueStart: 'VueStart',
  LoginStatus: 'LoginStatus',
  SubscriptionStatus: 'SubscriptionStatus',
  UserFetch: 'UserFetch',
  LocationFetch: 'LocationFetch',
  RepresentationFetch: 'RepresentationFetch',
  ReviewFetch: 'ReviewFetch',
  CrawlerSuccess: 'CrawlerSuccess',
  CrawlerError: 'CrawlerError',
  CrawlerProgress: 'CrawlerProgress',
}

export class HookListener {
  static get Identifier() {
    return HookId
  }

  constructor({ hookId, handler, id = null }) {
    this.hookId = hookId
    this.id = id
    this.handler = handler
  }
}

export class HookEvent {
  constructor(id) {
    this.id = id
    this.count = 0
    this.payload = null
  }

  onIsFired(payload = null) {
    this.payload = payload
    this.count = this.count + 1
  }
}


const StoreState = {
  hookListeners: {},
  events: [],
}

const getters = {
  hookListeners(state) {
    return state.hookListeners
  },
  hookListenersById: state => hookId => state.hookListeners[hookId],
}

const mutations = {
  /**
     *
     * @param state
     * @param {HookListener} listener
     */
  addHookListener(state, listener) {
    const { hookId } = listener
    if (!state.hookListeners[hookId]) state.hookListeners[hookId] = []
    state.hookListeners[hookId].push(listener)
    state.hookListeners = { ...{}, ...state.hookListeners }
  },
  /**
     *
     * @param state
     * @param {HookListener} listener
     */
  removeHookListener(state, listener) {
    const { hookId } = listener
    if (!state.hookListeners[hookId]) return
    state.hookListeners[hookId] = state.hookListeners[hookId].filter(addedListener => addedListener !== listener)
    state.hookListeners = { ...{}, ...state.hookListeners }
  },
  updateEvent(state, event) {
    const eventIndex = state.events.findIndex(stateEvent => stateEvent.id === event.id)
    if (eventIndex >= 0) {
      state.events = [...[], ...state.events.map(stateEvent => (stateEvent.id === event.id ? event : stateEvent))]
    } else {
      state.events = [...[], ...state.events, ...[event]]
    }
  },
}

const actions = {
  onHookEvent(context, { hookId, payload = null }) {
    const { state } = context
    const eventIndex = state.events.findIndex(event => event.id === hookId)
    const event = eventIndex >= 0 ? state.events[eventIndex] : new HookEvent(hookId)
    event.onIsFired(payload)
    context.commit('updateEvent', event)
    const listeners = state.hookListeners[hookId]
    if (!listeners || listeners.length === 0) return Promise.resolve()
    const promises = []
    listeners.forEach((listener) => {
      const returnValue = listener.handler(event)
      if (returnValue && typeof returnValue.then === 'function') promises.push(returnValue)
    })
    return promises.length === 0 ? Promise.resolve() : Promise.all(promises)
  },
}

export default {
  getters,
  mutations,
  actions,
  state: StoreState,
}

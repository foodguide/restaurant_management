import axios from 'axios'
import ExternalApiError from './ExternalApiError'

const urls = {
  baseUrl: 'https://review.thefoodguide.de/crawl',
}

export default {
  crawl: options => axios({
    url: urls.baseUrl,
    crossDomain: true,
    method: 'PUT',
    data: {
      start_url: options.startUrl,
      user_id: options.userId,
      location_id: options.locationId,
      platform: options.platform,
    },
    json: true,
  }),
}

export const getCrawlerError = (error) => {
  if (!error.response) throw error
  const options = error.response.data ? error.response.data : null
  const userId = options ? options.received ? options.received.user_id : null : null
  if (!options.code) throw userId ? new ExternalApiError(userId, error.message, error.status) : error
  const requestParams = options.received
  const { payload } = options
  switch (options.code) {
    case 1:
      return new IncompleteCrawlerResultException(requestParams, payload)
    case 2:
      return new EmptyCrawlerResultException(requestParams, payload)
    case 3:
      return new BadRequestCrawlerError(requestParams, payload)
    case 4:
      return new InternalCrawlerAPIErrorException(requestParams, payload)
    default:
      return null
  }
}

export const getUserFriendlyCrawlerErrorMessage = (error) => {
  const crawlerError = getCrawlerError(error)
  return crawlerError ? crawlerError.userFriendlyMessage : null
}

class CrawlerException extends Error {
  constructor(requestParams, payload) {
    super()
    this.startUrl = requestParams.start_url
    this.userId = requestParams.user_id
    this.locationId = requestParams.location_id
    this.platform = requestParams.platform
    this.payload = payload
  }
}

export class IncompleteCrawlerResultException extends CrawlerException {
  constructor(requestParams, payload) {
    super(requestParams, payload)
    this.message = 'Crawler result is incomplete.'
    this.name = 'IncompleteCrawlerResultException'
    this.stack = new Error().stack
    this.userFriendlyMessage = `Das Laden der Bewertungen von ${requestParams.start_url} konnte nicht abgeschlossen werden.`
  }
}

export class EmptyCrawlerResultException extends CrawlerException {
  constructor(requestParams, payload) {
    super(requestParams, payload)
    this.message = 'Crawler result is empty.'
    this.name = 'EmptyCrawlerResultException'
    this.stack = new Error().stack
    this.userFriendlyMessage = `Das Laden von ${requestParams.start_url} lieferte keine Bewertungen.`
  }
}

export class InternalCrawlerAPIErrorException extends CrawlerException {
  constructor(requestParams, payload) {
    super(requestParams, payload)
    this.message = 'Crawler API encountered an internal server error.'
    this.name = 'InternalCrawlerAPIErrorException'
    this.stack = new Error().stack
    this.userFriendlyMessage = `Beim Laden von ${requestParams.start_url} ist ein Fehler passiert. Bitte probiere es später erneut.`
  }
}

export class BadRequestCrawlerError extends CrawlerException {
  constructor(requestParams, payload) {
    super(requestParams, payload)
    this.message = 'Malformed crawler request'
    this.name = 'BadReqeuestCrawlerError'
    this.stack = new Error().stack
    this.userFriendlyMessage = `Die eingegebene Adresse ${requestParams.start_url} ist ungültig.`
  }
}

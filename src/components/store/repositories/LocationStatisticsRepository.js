import BaseRepository from './BaseRepository'

class LocationStatisticsRepository extends BaseRepository {
  get managedCollection() {
    return 'locationStatistics'
  }

  getStatisticsByLocationId(id) {
    return this.commitRead(this.collection.where('locationId', '==', id).get())
  }
}

export { LocationStatisticsRepository as default }

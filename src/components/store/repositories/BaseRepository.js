import firestore from 'components/api/Firestore'
import FirebaseApp from '../../api/Firebase'

class BaseRepository {
  get documentAttributes() {
    return this._documentAttributes
  }

  get collection() {
    return firestore.collection(this.managedCollection)
  }

  get managedCollection() {
    return this._managedCollection
  }

  set managedCollection(managedCollection) {
    this._managedCollection = managedCollection
  }

  create(data) {
    const filtered = this.filterAllowedAttributes(data)
    filtered.userId = FirebaseApp.auth().currentUser.uid
    if (filtered.id) return this.commitWrite(this.collection.doc(filtered.id).set(filtered))
    return this.commitWrite(this.collection.add(filtered))
  }

  update(id, data) {
    const filtered = this.filterAllowedAttributes(data)
    filtered.userId = FirebaseApp.auth().currentUser.uid
    return this.commitWrite(this.collection.doc(id).update(filtered))
  }

  updateOrCreate(id, data) {
    if (!data) return Promise.resolve(null)
    const filtered = this.filterAllowedAttributes(data)
    filtered.userId = this.getCurrentUserUid()
    return this.commitWrite(this.collection.doc(id).set(filtered, { merge: true }))
  }

  findAllForCurrentUser() {
    return this.commitRead(this.collection.where('userId', '==', this.getCurrentUserUid()).get())
  }

  findAllForUser(userId) {
    return this.commitRead(this.collection.where('userId', '==', userId).get())
  }

  findAllForAdmin() {
    return this.commitRead(this.collection.get())
  }

  findById(id) {
    return this.commitRead(this.collection.where('id', '==', id).get())
  }

  getCurrentUserUid() {
    return FirebaseApp.auth().currentUser ? FirebaseApp.auth().currentUser.uid : null
  }

  delete(id) {
    return this.collection.doc(id).delete()
  }

  deleteQuerySnapshot(snapshot) {
    const batch = firestore.batch()
    snapshot.forEach(doc => batch.delete(doc.ref))
    return batch.commit()
  }

  deleteWhere(field, comparator, value) {
    const query = this.collection.where(field, comparator, value)
    return this.deleteQuery(query)
  }

  deleteQuery(query) {
    const userId = this.getCurrentUserUid()
    if (!userId) return Promise.reject(new Error('No valid user id found'))
    query = query.where('userId', '==', userId)
    return query.get()
      .then(querySnapshot => this.deleteQuerySnapshot(querySnapshot))
  }

  collectData(document) {
    if (!document.exists) return null
    const data = document.data()
    data.id = document.id
    return data
  }

  filterAllowedAttributes(raw) {
    if (!raw) return raw
    return Object.keys(raw)
      .filter(key => this.documentAttributes.includes(key))
      .reduce((obj, key) => {
        obj[key] = raw[key]
        return obj
      }, {})
  }

  commitRead(promise) {
    return promise.then((querySnapshot) => {
      if (!querySnapshot) return
      return typeof querySnapshot.forEach !== 'function'
        ? this.collectData(querySnapshot)
        : querySnapshot.docs.map(document => this.collectData(document))
    })
  }

  commitWrite(promise) {
    return promise
  }
}

export { BaseRepository as default }

import Vue from 'vue'
import ReplyTaskRepository from '../repositories/ReplyTaskRepository'

const storeState = {
  tasks: [],
}

const getters = {
  runningReplyTasksIds: state => state.tasks.map(task => task.id),
}

const mutations = {
  updateTask(state, payload) {
    const index = storeState.tasks.findIndex(task => task.id === payload.id)
    if (index >= 0) {
      Vue.set(storeState.tasks, index, payload)
    } else {
      storeState.tasks.push(payload)
    }
  },
  removeTask(state, payload) {
    const index = storeState.tasks.findIndex(task => task.id === payload.id)
    if (index >= 0) storeState.tasks.splice(index, 1)
  },
}

const replyTaskRepository = new ReplyTaskRepository()

const actions = {
  listenToReplyTasks(context) {
    return replyTaskRepository.listenOnReplyTasks((data) => {
      if (!data) return
      Reflect.ownKeys(data).forEach((key) => {
        context.commit('updateTask', data[key])
      })
    }, (data) => {
      context.commit('removeTask', data)
      context.dispatch('setReviewReplied', {
        reviewId: data.id,
        text: data.text,
      })
    })
  },
  addReplyTask(context, payload) {
    context.commit('updateTask', payload)
  },
}

export default {
  state: storeState,
  getters,
  actions,
  mutations,
}

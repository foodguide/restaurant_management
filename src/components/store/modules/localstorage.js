import { capitalize } from 'components/helper/StringHelper'

/*
If you want to add some new keys to the local storage just add a key to the keys array.
This file will then add getters in the getters objects and setters in the mutation object.
For example the 'hasSeenOnboardingScreen' key will generate this.$store.getters.hasSeenOnboardingScreen and
this.$store.commit('setHasSeenOnboardingScreen', true)
The local storage persistency and the loading is handled by the vuex local storage plugin.
 */

const keys = [
  'hasSeenOnboardingScreen',
  'hasSeenLocationIntroduction',
  'hasSeenReviewIntroduction',
  'lastApiReviewSync',
]

const getters = {}
const mutations = {}
const state = {}

keys.forEach((key) => {
  state[key] = null
  getters[key] = state => (state[key] ? state[key] : null)
  mutations[`set${capitalize(key)}`] = (state, payload) => {
    state[key] = payload
  }
})

export default {
  state,
  getters,
  mutations,
  actions: {},
}

// At first we setup things not related to vue
// install jQuery
// Then we setup vue and start installation
import Vue from 'vue/dist/vue.esm.js'

// Load styling files
import 'bootstrap/dist/css/bootstrap.css'
import './assets/sass/paper-dashboard.scss'

import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'

// Installing plugins

// Setup Vuex Store
import Vuex from 'vuex'
import { adminStoreOptions } from 'components/store/index.js'

// Setup Vue Router
import VueRouter from 'vue-router'
import { installErrorLogging } from 'components/debug/ErrorLogger'
import VueI18n from 'vue-i18n'
import routes from './routes/admin_routes'

// Import and call additional setup routines
import { installAdminPlugins } from './plugins'

import { addAdminGuard } from './routes/admin_router'

import VueClipboard from 'vue-clipboard2'

// Admin Component
import Admin from './Admin.vue'
import { messages } from './lang/en'

window.$ = require('jquery')
window.JQuery = require('jquery')

// Overwriting the Promise class from the standard lib with the implementation from Bluebird
Promise = require('bluebird')

Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false,
  },
})

// Adding service worker for precaching
if (process.env.NODE_ENV === 'production' && 'serviceWorker' in navigator) {
  navigator.serviceWorker.register('/service-worker.js')
}
installAdminPlugins(Vue)
const store = new Vuex.Store(adminStoreOptions)
if (window.Cypress) {
  window.__store__ = store
}
const router = new VueRouter({
  routes,
  linkActiveClass: 'active',
})
installErrorLogging(Vue, store)
addAdminGuard(router, store)

VueClipboard.config.autoSetContainer = true
Vue.use(VueClipboard)

const defaultLocale = 'en'
Vue.use(VueI18n)
const i18n = new VueI18n({
  locale: defaultLocale,
  fallbackLocale: defaultLocale,
  messages,
})

export const createRootComponent = (el, component) => new Vue({
  i18n,
  el,
  store,
  render: h => h(component),
  router,
})

// Setting up root component
const admin = createRootComponent('#admin', Admin)
// export default main
export const { AdminArea } = admin

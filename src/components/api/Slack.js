import axios from 'axios'

const url = 'https://hooks.slack.com/services/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'

const api = {
  logError: (attachments) => {
    axios({
      url,
      method: 'post',
      headers: {},
      data: JSON.stringify({
        text: '',
        attachments,
      }),
    }).then((response) => {
      console.log(response)
    }).catch((error) => {
      console.log(error)
    })
  },
  logText: (text) => {
    axios({
      url,
      method: 'post',
      headers: {},
      data: JSON.stringify({
        text,
      }),
    }).then((response) => {
      console.log(response)
    }).catch((error) => {
      console.log(error)
    })
  },
}
export default api

export const Slack = api

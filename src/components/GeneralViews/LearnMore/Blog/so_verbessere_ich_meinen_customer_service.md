﻿# So verbessere ich meinen Customer Service

Im Customer Service dreht sich alles um die Interaktion mit dem Kunden, um neue User zu gewinnen und eine langanhaltende Bindung zu erzielen. Nicht nur das Produkt selber, auch der Service der mitgeliefert wird, bestimmt die Erfahrung des Kunden mit deinem Unternehmen - und ob er bei euch bleibt. Wie du deinen Customer Service auf ein neues Level hebst, erfährst du in unseren 4 Tipps. Deine Kunden werden es merken.

-   **Persönlicher Support**
    
Dieser beginnt ganz einfach damit, den Namen eines Kunden in Telefongesprächen zu erinnern und ihn damit anzusprechen. Besonders gut funktioniert das, wenn du ihn gleich, nachdem du ihn gehört hast wiederholst. Diese Reproduktion wird im Gedächtnis nachhallen und du vergisst ihn weniger schnell. Der Kunde freut sich, wenn du dir die Zeit nimmst auf Socialmedia Kommentare und Nachrichten persönlich zu antworten. Personalisierte Karten zu Weihnachten kommen ebenfalls gut an. Wichtig ist, dem User das Gefühl zu geben, dass er als Individuum nicht nur wahrgenommen wird, sondern wichtig ist. Beim Verwalten und Beobachten von Online Bewertungen greift Respondo - Bewertungsmanagement gerne unter die Arme, damit der Support wirklich keine Lücken aufweist und jede Stimme gehört wird.

-   **Erst zuhören, dann handeln**
    
Gut zu zu hören, ist der Schlüssel zu erstklassigem Customer Service. Der Kunde muss das Gefühl haben, verstanden zu werden. Um diesen Service zu gewährleisten, ist es gut, das Gesagte des Gegenübers grob zusammenzufassen und zu wiederholen um zu überprüfen, ob alles richtig aufgenommen wurde. Weitere Gegenfragen helfen, das Problem zu identifizieren und Lösungsvorschläge machen zu können.

-   **Stelle die richtigen Mitarbeiter ein**
   
Egal, in welcher Branche dein Unternehmen tätig ist, die Mitarbeiter sind das A und O. Ob in der Gastronomie, im Callcenter oder Pflegedienst, in den meisten Gewerben, haben die Angestellten direkten Kontakt mit den Kunden und sind somit verantwortlich für die Erfahrung, die der Kunde mit deinem Unternehmen macht. Achte bei der Auswahl deiner Mitarbeiter auf Zuverlässigkeit, Ausstrahlung und ehrliches Interesse an deinem Unternehmen und dem damit verbundenen Job, den sie ausführen.

-   **Seid rund um die Uhr erreichbar**

Wir in unserem kleinen Schichtplaner-Team wissen selber, dass durchgängiger Service sehr schwierig zu gewährleisten ist. Mitarbeiter brauchen Urlaubstage und werden manchmal krank. Trotzdem, versucht einfach, euer Möglichstes zu tun. Setzt euch ein Limit von höchstens 24 Stunden, in denen ihr jede Anfrage beantwortet habt. Gerade bei Online-Angeboten ist es wichtig, möglichst lange telefonisch erreichbar zu sein. Dafür kann es praktisch sein, Mitarbeiter in Schichten einzusetzen. Natürlich kann dieser Service nicht immer gegeben sein, richtet dafür ein Online Helpcenter ein, das einige Fragen direkt beantworten kann und verweist darauf am besten direkt in einer automatischen Abwesenheitsmail.

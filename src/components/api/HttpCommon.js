import Axios from 'axios'

const headers = {}
headers['App-Version'] = '0.1.0'
headers['Device-Model'] = 'browser'
headers['Device-Platform'] = 'browser'
headers['Device-Version'] = '1.0.0'
headers['Device-Uuid'] = 'test'
headers['Device-Locale'] = 'de'
headers['Content-Type'] = 'application/x-www-form-urlencoded'
const baseURL = 'http://localhost:8080'
const timeout = 60000
// customization for different environments
/*
if (process.env.NODE_ENV === 'production') {
    baseURL = 'http://stage.thefoodguide.de'
    timeout = 10000
} else if (process.env.NODE_ENV === 'development') {
    headers.Authorization = 'Basic ZmctZGV2OmZvb2RpZWZvb2Rmb29k'
} */

const httpOptions = {
  baseURL,
  timeout,
  headers,
  withCredentials: true,
}
export default Axios.create(httpOptions)

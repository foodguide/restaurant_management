import { spec } from 'cypress/support/mountVue'
import Login from 'src/components/GeneralViews/Login'
import Registration from 'src/components/GeneralViews/Registration'

const storeOptions = {
  state: {
    calledFacebookLogin: false,
    forgotEmail: null,
    calledLogin: false,
    loginEmail: null,
    loginPw: null,
  },
  mutations: {
    setFacebookLogin(state, payload) {
      state.calledFacebookLogin = payload
    },
    setForgotEmail(state, payload) {
      state.forgotEmail = payload.email
    },
    login(state, payload) {
      state.loginEmail = payload.email
      state.loginPw = payload.password
    },
    reset(state) {
      state.calledFacebookLogin = false
      state.forgotEmail = null
      state.calledLogin = false
      state.loginEmail = null
      state.loginPw = null
    },
  },
  actions: {
    loginFacebook(context) {
      context.commit('setFacebookLogin', true)
    },
    forgotPassword(context, payload) {
      context.commit('setForgotEmail', payload)
      return Promise.resolve()
    },
    login(context, payload) {
      context.commit('login', payload)
    },
  },
}

const routes = [
  {
    path: '/',
    component: Login,
  },
  {
    path: '/register',
    redirect: '/auth/register',
  },
  {
    path: '/auth',
    redirect: '/auth/register',
    children: [
      {
        path: 'register',
        component: Registration,
      },
    ],
  },
]

describe('Login.vue', () => {
  let unitTest
  before((done) => {
    cy.unitTest(Login, routes, storeOptions).then((result) => {
      unitTest = result
      done()
    })
  })

  afterEach(() => {
    unitTest.wrapper.vm.$store.commit('reset')
    unitTest.componentWrapper.setData({
      formEmail: '',
      formPassword: '',
      sending: false,
      sendingShort: false,
      showError: false,
      errorMessage: '',
      showForgotPasswordDialog: false,
      showPasswordOkDialog: false,
      showFailDialog: false,
      showOkDialog: false,
      passwordResetErrorMessage: false,
      emailInput: '',
    })
  })

  it('should return the correct css validation-class', () => {
    // invalid and touched (textfield was used)
    let cssClass = unitTest.mountedComponent.getValidationClass({
      $invalid: true,
      $dirty: true,
    })
    expect(cssClass['md-invalid']).to.be.true
    // invalid but not touched (textfield wasn't used before)
    cssClass = unitTest.mountedComponent.getValidationClass({
      $invalid: true,
      $dirty: false,
    })
    expect(cssClass['md-invalid']).to.be.false
    // valid and touched (textfield was used before)
    cssClass = unitTest.mountedComponent.getValidationClass({
      $invalid: false,
      $dirty: true,
    })
    expect(cssClass['md-invalid']).to.be.false
    // valid and not touched (textfield wasn't used before)
    cssClass = unitTest.mountedComponent.getValidationClass({
      $invalid: false,
      $dirty: false,
    })
    expect(cssClass['md-invalid']).to.be.false
  })

  it('calls the onFacebookLogin method when facebook button is clicked', (done) => {
    cy.get('[data-cy=facebook-button]').click().then(() => {
      unitTest.wrapper.vm.$nextTick(() => {
        expect(unitTest.getState().calledFacebookLogin).to.be.true
        done()
      })
    })
  })

  it('should give the user the possibility to reset his password', (done) => {
    const email = 'test@test.de'
    cy.get('[data-cy=password-button]').click().then(() => {
      spec.get('[data-cy=password-dialog]').then((dialog) => {
        expect(dialog).to.exist
        expect(dialog).to.be.visible
        expect(unitTest.mountedComponent.showForgotPasswordDialog).to.be.true
        unitTest.type('[data-cy=forgot-email]', email).then(() => {
          unitTest.click('[data-cy=confirm-forgot-password-button]').then(() => {
            const state = unitTest.getState()
            expect(state).to.exist
            expect(state.forgotEmail).to.eq(email)
            expect(unitTest.mountedComponent.showForgotPasswordDialog).to.be.false
            expect(unitTest.mountedComponent.showOkDialog).to.be.true
            done()
          })
        })
      })
    })
  })

  it('should pass the login data to the store when the user click login', (done) => {
    const email = 'e2e@test.de'
    const pw = '11223344'
    cy.get('[data-cy=email-input]').type(email)
    cy.get('[data-cy=password-input]').type(pw)
    cy.get('[data-cy=login-btn]').click().then(() => {
      unitTest.$next(() => {
        const state = unitTest.getState()
        expect(state.loginEmail).to.eq(email)
        expect(state.loginPw).to.eq(pw)
        done()
      })
    })
  })
})

describe('Test Router', () => {
  before(() => {
    cy.login()
  })

  it('opens landingpage despite logged in state', () => {
    cy.location().should((location) => {
      expect(location.href).to.eq(`${Cypress.config().baseUrl}/#/start`)
    })
  })

  it('opens location page after click on login button', () => {
    cy.get('[data-cy=login-button]').eq(0).click()
    cy.location().should((location) => {
      expect(location.href).to.eq(`${Cypress.config().baseUrl}/#/app/locations`)
    })
  })
})

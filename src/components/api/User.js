import moment from 'moment'
import FirebaseApp, { Firebase } from './Firebase'
import FacebookProvider from './AuthProvider/FacebookProvider'
import { googleCredential } from './AuthProvider/GoogleProvider'
import UserRepository from '../store/repositories/UserRepository'
import fakeUserApiFactory from '../../../cypress/support/FakeUserApi'

const userRepository = new UserRepository()

const userApi = {
  idTokenObserver: [],
  addIdTokenObserver(observer) {
    if (typeof observer.onNextIdToken !== 'function') {
      console.error('passed idTokenObserver does not implement the needed function onNextIdToken(idToken)')
      return
    }
    this.idTokenObserver.push(observer)
  },
  onNextIdToken(idToken) {
    this.idTokenObserver.forEach(observer => observer.onNextIdToken(idToken))
  },
  register(email, password) {
    return FirebaseApp.auth().createUserWithEmailAndPassword(
      email,
      password,
    )
  },
  login(email, password) {
    return FirebaseApp.auth().signInWithEmailAndPassword(
      email,
      password,
    )
  },
  // TODO: Do we still need this method?
  linkWithEmail(email, password) {
    return userRepository.getCurrentUser().then((user) => {
      const facebookCredentials = user.credentials.find(accessToken => accessToken.platform === 'facebook')
      const googleCredentials = user.credentials.find(accessToken => accessToken.platform === 'google')

      let reauthenticateCredential = null
      if (facebookCredentials) {
        reauthenticateCredential = Firebase.auth.FacebookAuthProvider.credential(facebookCredentials.accessToken)
      } else if (googleCredentials) {
        reauthenticateCredential = googleCredential(googleCredentials.id_token, googleCredentials.accessToken)
      }

      return this.getCurrentUser().reauthenticateAndRetrieveDataWithCredential(reauthenticateCredential).then(() => {
        const emailCredential = userApi.getEmailCredential(email, password)
        return userApi.linkWithCredential(emailCredential)
      })
    })
  },
  linkWithCredential(credential) {
    return userApi.getCurrentUser().linkAndRetrieveDataWithCredential(credential)
  },
  loginWithFacebook() {
    return userApi.loginWithProvider(FacebookProvider)
  },
  loginWithProvider(provider) {
    return FirebaseApp.auth().signInWithPopup(provider)
  },
  loginWithToken(token) {
    return FirebaseApp.auth().signInWithCustomToken(token)
  },
  logout() {
    return FirebaseApp.auth().signOut()
  },
  delete() {
    return FirebaseApp.auth().currentUser.delete()
  },
  observeLoginStatus(onChange) {
    FirebaseApp.auth().onAuthStateChanged((user) => {
      if (user && !this.isFreshFacebookRegistration(user)) {
        this.getCurrentIdToken().then(() => onChange(true, user))
      } else {
        onChange(false, null)
      }
    })
  },
  /**
   * We want to avoid new registrations via Facebook. This could lead to conflicts if multiple Facebook accounts
   * are connected to one Respondo account. This method checks if the account was created via Facebook within the
   * last 5 seconds.
   *
   * @param user
   * @returns {boolean}
   */
  isFreshFacebookRegistration(user) {
    const parsedUser = JSON.parse(JSON.stringify(user))
    return parsedUser.providerData.length === 1
        && parsedUser.providerData[0].providerId === 'facebook.com'
        && moment.unix(parsedUser.createdAt / 1000).isAfter(moment().add(-5, 'second'))
  },
  observeAdminStatus(onChange) {
    FirebaseApp.auth().onAuthStateChanged((user) => {
      if (user) {
        this.getIdTokenResult().then((idTokenResult) => {
          this.onNextIdToken(idTokenResult.token)
          onChange({
            isLoggedIn: true,
            idTokenResult,
          })
        })
      } else {
        onChange({
          isLoggedIn: false,
        })
      }
    })
  },
  getCurrentUser() {
    return FirebaseApp.auth().currentUser
  },
  getCurrentIdToken(forceRefresh = false) {
    return userApi.getCurrentUser().getIdToken(forceRefresh).then((token) => {
      this.onNextIdToken(token)
      return token
    })
  },
  resetCurrentIdToken() {
    this.onNextIdToken(null)
  },
  getIdTokenResult() {
    return Firebase.auth().currentUser.getIdTokenResult()
  },
  forgotPassword(emailAddress) {
    return FirebaseApp.auth().sendPasswordResetEmail(emailAddress)
  },
  changePassword(passwordInfos) {
    const credential = this.getEmailCredential(
      this.getCurrentUser().email,
      passwordInfos.oldPassword,
    )
    return this.getCurrentUser().reauthenticateAndRetrieveDataWithCredential(credential).then(() => FirebaseApp.auth().currentUser.updatePassword(passwordInfos.newPassword))
  },
  changeEmailAddress(userInfos) {
    const credential = this.getEmailCredential(
      this.getCurrentUser().email,
      userInfos.password,
    )

    return this.getCurrentUser().reauthenticateAndRetrieveDataWithCredential(credential).then(() => FirebaseApp.auth().currentUser.updateEmail(userInfos.newMailAddress))
  },
  getEmailCredential(email, password) {
    return Firebase.auth.EmailAuthProvider.credential(email, password)
  },
}

const fake = fakeUserApiFactory.create(userApi)
export default window.Cypress ? fake : userApi

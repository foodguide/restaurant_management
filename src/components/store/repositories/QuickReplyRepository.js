import BaseRepository from './BaseRepository'

class QuickReplyRepository extends BaseRepository {
  get managedCollection() {
    return 'quickReply'
  }

  get documentAttributes() {
    return ['userId', 'id', 'locationId', 'options', 'texts']
  }

  writeQuickReply(data) {
    const filtered = this.filterAllowedAttributes(data)
    return filtered.id ? this.updateOrCreate(filtered.id, filtered) : this.create(filtered)
  }
}

export default QuickReplyRepository

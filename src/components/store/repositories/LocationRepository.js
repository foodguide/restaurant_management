import BaseRepository from './BaseRepository'
import FirebaseApp from '../../api/Firebase'

class LocationRepository extends BaseRepository {
  get managedCollection() {
    return 'location'
  }

  get documentAttributes() {
    return ['name', 'userId', 'unseenOldReviewsTimestamp']
  }

  create(data) {
    const filtered = this.filterAllowedAttributes(data)
    filtered.userId = FirebaseApp.auth().currentUser.uid
    return this.commitWrite(this.collection.add(filtered))
  }

  updateRepresentations(locationId, representations) {
    const promises = []
    representations.forEach((representation) => {
      promises.push(this.collection
        .doc(locationId)
        .collection('representations')
        .doc(representation.id)
        .update(representation))
    })
    return Promise.all(promises)
  }

  addFacebookRepresentationToLocation(locationId, representation) { // TODO: change to generic addRepresentationToLocation()
    const data = {
      created: Date(),
      id: `fb_${locationId}`,
      type: 'api',
      platform: 'facebook',
      locationId,
      externalId: representation.externalId !== undefined ? representation.externalId : null,
      accessToken: representation.accessToken !== undefined ? representation.accessToken : null,
      credentialId: representation.credentialId !== undefined ? representation.credentialId : null,
      info: {// TODO
        name: representation.name,
        description: null,
        adress: null,
      },
    }
    return this.commitWrite(
      this.collection
        .doc(locationId)
        .collection('representations')
        .doc(`fb_${locationId}`)
        .set(data),
    ).then(() => data)
  }

  addInstagramRepresentationToLocation(locationId, representation) { // TODO: change to generic addRepresentationToLocation()
    const data = {
      created: Date(),
      id: `ig_${locationId}`,
      type: 'api',
      platform: 'instagram',
      externalId: representation.externalId !== undefined ? representation.externalId : null,
      accessToken: representation.accessToken !== undefined ? representation.accessToken : null,
      credentialId: representation.credentialId !== undefined ? representation.credentialId : null,
      info: {// TODO
        name: representation.name,
        description: null,
        adress: null,
      },
    }
    return this.commitWrite(
      this.collection
        .doc(locationId)
        .collection('representations')
        .doc(`ig_${locationId}`)
        .set(data),
    ).then(() => data)
  }

  deleteRepresentation(locationId, representation) {
    return this.collection
      .doc(locationId)
      .collection('representations')
      .doc(representation.id)
      .delete()
  }

  addRepresentationToLocation(locationId, data) {
    const representation = {
      created: Date(),
      id: `${data.platformShortcut}_${data.locationId}`,
      type: data.type,
      platform: data.platform,
      locationId: data.locationId,
      externalId: data.externalId !== undefined ? data.externalId : null,
      accessToken: data.accessToken !== undefined ? data.accessToken : null,
      credentialId: data.credentialId !== undefined ? data.credentialId : null,
      info: {},
    }
    if (data.info) {
      representation.info.name = data.info.name ? data.info.name : null
      representation.info.description = data.info.description ? data.info.description : null
      representation.info.address = data.info.address ? data.info.address : null
    }
    if (data.url) representation.url = data.url

    return this.commitWrite(
      this.collection
        .doc(locationId)
        .collection('representations')
        .doc(representation.id)
        .set(representation),
    ).then(() => representation)
  }

  updateRepresentation(locationId, representation, data) {
    return this.commitWrite(
      this.collection
        .doc(locationId)
        .collection('representations')
        .doc(representation.id)
        .update(data),
    )
  }

  writeUnseenOldReviewsTimestamp(locationData, timestamp) {
    const data = {
      ...locationData,
      unseenOldReviewsTimestamp: timestamp,
    }
    if (window.Cypress) return Promise.resolve(data)
    return this.updateOrCreate(data.id, data).then(() => data).catch((error) => {
      console.log(error)
      console.log(JSON.stringify(error))
      throw error
    })
  }

  getRepresentationByLocationId(id) {
    return this.commitRead(
      this.collection
        .doc(id)
        .collection('representations')
        .get(),
    )
  }
}

export { LocationRepository as default }

export const RepresentationStatus = {
  ACTIVE: 'active',
  ACCESSTOKEN_EXPIRED: 'accesstoken_expired',
  PERMISSION_DECLINED: 'permission_declined',
}

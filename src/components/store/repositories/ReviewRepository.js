import BaseRepository from './BaseRepository'

class ReviewRepository extends BaseRepository {
  get managedCollection() {
    return 'review'
  }

  get documentAttributes() {
    return [
      'id',
      'externalId',
      'locationId',
      'representationId',
      'created',
      'updated',
      'url',
      'reviewer',
      'response',
      'rating',
      'ratingType',
      'text',
      'userId',
      'platform',
      'post',
      'comments',
      'state',
      'externalURI',
      'state',
    ]
  }

  setReviewState(reviewId, state) {
    const review = {
      state,
    }
    return this.update(reviewId, review)
  }

  findAllForLocation(locationId) {
    return this.commitRead(this.collection.where('locationId', '==', locationId).get())
  }
}

export { ReviewRepository as default }

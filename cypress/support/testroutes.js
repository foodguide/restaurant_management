import Settings from '../../src/components/GeneralViews/Settings.vue'
import Reviews from '../../src/components/Dashboard/Views/Reviews/Review.vue'
import Imprint from '../../src/components/GeneralViews/Imprint.vue'
import Location from '../../src/components/Dashboard/Views/Location/Location.vue'
import Connector from '../../src/components/Dashboard/Views/Representation/Connector.vue'
import Login from '../../src/components/GeneralViews/Login.vue'
import Registration from '../../src/components/GeneralViews/Registration.vue'
import AuthLayout from '../../src/components/GeneralViews/AuthContainer.vue'
import Onboarding from '../../src/components/GeneralViews/Onboarding/Onboarding.vue'
import Landing from '../../src/components/GeneralViews/LandingPage/LandingPage.vue'
import Blogpost from '../../src/components/GeneralViews/Blogpost.vue'
import TermsOfService from '../../src/components/GeneralViews/TermsOfService.vue'
import LearnMore from '../../src/components/GeneralViews/LearnMore/LearnMore.vue'
import Statistics from '../../src/components/Dashboard/Views/Statistics/Statistics.vue'
import Payment from '../../src/components/GeneralViews/Payment.vue'
import { createRoutes } from '../../src/routes/routes'

export default createRoutes(Settings, Reviews, Imprint,
  Location, Connector, Login,
  Registration, AuthLayout, Onboarding, Landing, Blogpost, TermsOfService,
  LearnMore, Statistics, Payment)

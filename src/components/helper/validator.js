const validator = {}
validator.isNullOrEmpty = string => string === null || string.length === 0
validator.isValid = string => !validator.isNullOrEmpty(string)
validator.isValidVoucher = voucher => (
  validator.isValid(voucher.title)
        && validator.isValid(voucher.description)
        && validator.isValid(voucher.discount)
        && voucher.valid_from !== null
        && voucher.valid_until !== null
        && voucher.image !== null
)
export const Validator = validator

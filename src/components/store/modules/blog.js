import TranslationHelper from '../../helper/TranslationHelper'

const state = {
  entriesDe: [
    {
      path: 'src/components/GeneralViews/LearnMore/Blog/respondo_premium.md',
      title: 'Plattformen über Plattformen- Was unterscheidet sie untereinander?',
      previewText: 'Google, der Traffic-Held, TripAdvisor, der Reiseguide und Facebook der Social-Media-Starter. All diese Plattformen bieten Gästen die Möglichkeit Bewertungen zu eurem Lokal zu verfassen. Doch wie funktionieren die einzelnen Plattformen und wie unterscheiden sie sich?...',
      order: -4,
      previewImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/ar_16:9,c_fill,e_sharpen,c_thumb,e_colorize:15,q_auto:eco,w_600/v1575381919/respondo/rima-kruciene-h-Ilv2wMFz4-unsplash.jpg',
      imageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_thumb,e_colorize:39,q_auto:good,w_1080/v1575381919/respondo/rima-kruciene-h-Ilv2wMFz4-unsplash.jpg',
      headerImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_fill,g_auto,h_315,w_1000/v1575381919/respondo/rima-kruciene-h-Ilv2wMFz4-unsplash.jpg',
      markdown: '# Plattformen über Plattformen- Was unterscheidet sie untereinander?\n'
        + '\n'
        + '**Google**, der Traffic-Held, **TripAdvisor**, der Reiseguide und **Facebook** der Social-Media-Starter. All diese Plattformen bieten Gästen die Möglichkeit Bewertungen zu eurem Lokal zu verfassen. Doch wie funktionieren die einzelnen Plattformen und wie unterscheiden sie sich?\n'
        + '\n'
        + 'Zu klassischen Bewertungsplattformen zählen Yelp, TripAdvisor und Google-Bewertungen. Auf diesen Portalen kann euer Lokal eine Bewertung von 1-5 Sternen erhalten mit Kommentar, Bildern und Videos von eurem Lokal. Solltet ihr keinen Eintrag auf einer der Plattformen pflegen könnt ihr diesen kostenlos erstellen und eure Stammdaten einpflegen, wie z.B. Telefonnummer und Öffnungszeiten. Möglicherweise gibt es euer Lokal bereits und ihr müsst nur noch den Zugang beantragen.\n'
        + '\n'
        + 'Die Entscheidung liegt bei euch, ob ihr euren Gästen öffentlich auf die Bewertung antwortet oder privat. Yelp und TripAdvisor bieten ein internes Chatsystem an. Über Google funktioniert das private Antworten nur, wenn die Gäste ihre Email-Adresse hinterlegt haben.\n'
        + '\n'
        + '\n'
        + 'Über **Yelp** habt ihr auch die Möglichkeit eure treuesten Gäste mit einem Spezialangebot über die “Check In” Funktion zu belohnen. Das Funktioniert folgendermaßen: Wenn eure Gäste über einen Button auf Yelp "einchecken" teilen sie ihren Freunden mit, dass sie in eurem Geschäft sind. Als Belohnung erhalten sie von euch ein Spezialangebot in Form von Rabatten oder Gutscheinen. Natürlich ist es auch möglich Anzeigen zu schalten oder bei der Suche ganz oben zu erscheinen, genau wie bei Google und TripAdvisor.\n'
        + '\n'
        + '\n'
        + '**TripAdvisor** hat ursprünglich als Reiseführer begonnen. Zusätzlich kann man über TripAdvisor und auch Google sogenannte Performance-Werte aufrufen, um die Entwicklung eures Lokals auf den Plattformen nachzuverfolgen.\n'
        + '\n'
        + '\n'
        + '**Google** wiederum ist weltbekannt und bietet die die Möglichkeit den Kundenzulauf zu steigern. Mit Google-My-Business erreicht ihr eure Kunden über Google-Maps und Google-Suchmaschine. Die meisten Gäste verfassen ihre Bewertung über Google-Maps. Jeder Gast nutzt die Google-Suchmaschine, weil es sich um ein etabliertes Netzwerk handelt, um notwendige Informationen aufzurufen. Google ist ein Muss für jeden Unternehmer um entdeckt zu werden, da es sich hier um die erste Anlaufstelle handelt.\n'
        + '\n'
        + 'Natürlich darf man Social-Media nicht außer Acht lassen. Heutzutage surfen so gut wie alle Gäste tagtäglich im Social-Media. Auch da ist es wichtig aktiv zu sein, denn Social-Media ist das perfekte Medium, um sein Außenbild optimal zu präsentieren. Genau wie die anderen Portale handelt es sich außerdem um einen kostenlosen Account. Viele informieren sich auch im Social-Media über ein Lokal. Social-Media ermöglicht außerdem eine direkte Kommunikation zu euren Gästen. Ohne dass sie vielleicht sogar die Absicht haben gezielt nach eurem Lokal zu suchen. Ihr baut euch somit eine gewisse Reichweite und Vertrauen zu euren Gästen auf.\n'
        + '\n'
        + 'Auch über **Facebook** hat man die Option Bewertungen zu erhalten und zu antworten. Das funktioniert über das Daumen-Hoch-Daumen-Runter-System und wahlweise mit einem Kommentar. Außerdem beeinflusst man das Auftreten des Unternehmens mit verschiedenen Beiträgen wie Bildern und Spezialangeboten. So bleiben die Gäste interessiert und man gerät bei ihnen nicht in Vergessenheit. Daher haben die meisten Unternehmen auch mit als erstes immer einen Facebook-Account.\n'
        + '\n'
        + 'Aktuell ist **Instagram** auch ein beliebtes Portal, um die Kommunikation zu den Gästen spannend aufzubauen und aufrechtzuerhalten. Der Hauptkommunikationsweg erfolgt hier über die Kommentare unter euren eigenen Beiträgen. Auch hier ist es sehr empfehlenswert den Gästen zu antworten, um eine persönliche Ebene zu schaffen und sie an dein Lokal zu binden. Deswegen kann man auch Instagram über Respondo managen. Außerdem bietet Instagram ebenfalls Performance-Werte, durch die ihr eure Marketingmaßnahmen für euer Lokal einschätzen könnt.\n'
        + '\n'
        + 'Natürlich ist für Lokale auch ein Reservierungssystem wichtig. In Respondo vertreten ist **OpenTable**. Ein Account bei OpenTable ist kostenlos. Da es sich allerdings um eine Dienstleistung handelt, fallen pro Reservierung geringe Kosten an. Auch hier haben eure Gäste die Möglichkeit nach der Reservierung eine Bewertung von 1-5 Sternen mit einem Kommentar zu hinterlassen. Es handelt sich um eine direkte Aufforderung für Feedback. Die Gäste werden immer internetaffiner und steuern lieber alles per Klick. Wenn das Lokal gut besucht ist und alle Kapazitäten im Service gebraucht werden, ist es eine Vereinfachung für Inhaber und Gast online zu reservieren. Sobald eure Gäste die Reservierung wahrgenommen haben, können sie das Lokal bei OpenTable bewerten. Auf diese Weise erhaltet ihr keine Fake-Bewertungen, sondern konstruktives Feedback.\n'
        + '\n'
        + '**Zusammengefasst**\n'
        + '\n'
        + '**Yelp** ist für Gastronomen und Hoteliers gut geeignet als klassisches und einfaches Bewertungsportal, wobei **TripAdvisor** zusätzlich noch nützliche Performance-Werte bietet und vor allem für den Tourismus relevant ist. Wenn ein Lokal sehr nah an Sehenswürdigkeiten oder großen Bahnhöfen liegt, lohnt sich eine gute Darstellung bei Tripadvisor nochmal mehr.  **Google** ist weltweit bekannt, zählt zu der meist genutzten Plattform und ist ein Muss für Gastronomen und Hoteliers. Sobald eure Gäste Google-Maps nutzen, um den Weg zu finden, sehen sie automatisch die Bewertungen zu eurem Lokal. Google bietet die optimale Gelegenheit um den Kundenzulauf zu steigern. **Facebook** ist eine ideale Plattform um die Reichweite zu vergrößern. Durch Social-Media erhalten eure Gäste interessanten Input über euer Lokal. So werden die Kundenbeziehung gestärkt und Neue gewonnen. Das selbe gilt für Instagram. Außerdem ist **Instagram** die heute meistgenutzte Social-Media-Plattform. Auch Reservierungssysteme wie **OpenTable** haben sich in den letzten 10 Jahren entwickelt und an Bedeutung gewonnen. Sie bieten sowohl Inhabern als auch Gästen einen Vorteil bezüglich Zeit und Aufwand.'
        + '\n',
    },
    {
      path: 'src/components/GeneralViews/Start/Blog/das_instagram_abc_der_gastronomie.md',
      title: 'Warum sind Online-Bewertungen so wichtig für Gäste',
      previewText: 'Daumen hoch/runter, Sterne, Empfehlungen, Bewertungen – 77 % der Gäste vertrauen den Online-Bewertungen mehr als renommierten Kritikern. Deswegen sollte man diesen Aspekt nicht vergessen...',
      order: -3,
      previewImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/ar_16:9,c_fill,e_sharpen,c_thumb,e_colorize:15,q_auto:eco,w_600/v1572878651/respondo/online_bewertungen_wichtig.jpg',
      imageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_thumb,e_colorize:39,q_auto:good,w_1080/v1572878651/respondo/online_bewertungen_wichtig.jpg',
      headerImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_fill,g_center,h_315,w_1000/v1572878651/respondo/online_bewertungen_wichtig.jpg',
      markdown: '# Warum sind Online-Bewertungen so wichtig für Gäste\n'
        + '\n'
        + 'Daumen hoch/runter, Sterne, Empfehlungen, Bewertungen – 77 % der Gäste vertrauen den Online-Bewertungen mehr als renommierten Kritikern. Deswegen sollte man diesen Aspekt nicht vergessen. Denn Online-Bewertungen sind der wichtigste Beeinflussungsfaktor bei der Kaufentscheidung.\n'
        + '\n'
        + '**Nutzerverhalten**\n'
        + 'Bei der Suche nach dem richtigen Lokal informieren sich deine Gäste heute mit Hilfe von Online-Bewertungen. Entweder suche sie planlos in ihrer Nähe oder gezielt nach Empfehlungen. Wichtig sind ihnen die Atmosphäre und die Qualität. Im Schnitt verfasst jeder dritte Nutzer 5-12 Mal im Jahr auch eine Online-Bewertung. Gäste verfassen Online-Bewertungen, um anderen zu helfen den richtigen Service und das optimale Produkt zu finden.\n'
        + '\n'
        + '**Was du beachten musst**\n'
        + 'Nach einer aktuellen Umfrage gaben 80 % an, dass sie mindestens einmal eine Online-Bewertung verfasst haben. Bewertungen im Internet sind mittlerweile ein wichtiger Entscheidungsfaktor. Daher muss auch im Bewertungsmanagement Zeit investiert werden, um das Geschäft zu stabilisieren. Potenzielle Gäste beeinflussen sich gegenseitig. Um mehr gute Bewertungen zu erhalten, sollte man seine eigenen Gäste persönlich auffordern eine Online-Bewertung zu verfassen. Eine gute Omnipräsenz hilft um sein Außenbild auch online zu steuern und attraktiv zu präsentieren.\n'
        + '\n'
        + '**Schlechte Bewertungen**\n'
        + 'Schlechte Bewertungen sind nicht nur schlecht. Sie können für Authentizität sorgen. Wichtig ist als Lokal darauf einzugehen und ggf. die Situation zu erklären. Als Lokal ist es außerdem wichtig guten Service als übergeordnetes Ziel anzugeben und Feedback immer angenommen wird. So entwickelt man eine gewisse Sympathie und Nahbarkeit. Auch das hilft authentisch zu sein und weckt Interesse bei dem Leser der Online-Bewertungen. \n'
        + '\n'
        + '**Social Media**\n'
        + 'Auch soziale Netzwerke spielen eine Rolle bei Online-Bewertungen. Durch die sozialen Netzwerke können sich alle öffentlich austauschen. Außerdem bist du so präsent in der Online-Welt und kannst eine Beziehung zu deinen Gästen aufbauen. Die Empfehlungen steigen und auch über soziale Netzwerke hast du die Möglichkeit auf deine Bewertungen hinzuweisen und zum Bewerten zu animieren. Über Facebook sollte man darauf achten die Funktion für Bewertungen zu aktivieren. \n'
        + '\n'
        + '**Fazit**\n'
        + 'Online-Bewertung sind für Gäste wichtig um sich für ein tolles Erlebnis zu entscheiden. Sie sichern sich mit Online-Bewertungen ab, dass Atmosphäre und Qualität stimmt. Die Leser verlassen sich auf geteilte Kundenerfahrungen, um authentische Meinungen zu erhalten. Fast 90 % erhalten durch die Online-Bewertungen mehr Orientierung. Online-Bewertungen sind daher sehr wichtig für Gäste.',
    },
    {
      path: 'src/components/GeneralViews/Start/Blog/das_instagram_abc_der_gastronomie.md',
      title: 'Wie reagiere ich auf schlechte Bewertungen?',
      previewText: 'Die rasant an Bedeutung gewinnende Bewertungskultur ist Fluch und Segen zugleich. Heute ist jeder Kritiker, ob man es will oder nicht. Mit Google, Facebook, TripAdvisor und Co kommt die Flut an Bewertungen und Kommentaren auch zu den Gastronomen. Da kann auch mal eine schlechte Bewertung auftauchen. Diese zu ignorieren ist aber nicht der einzige Weg. Denn manchmal kann man diese durch effektiven Kundenkontakt...',
      order: -2,
      previewImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/ar_16:9,c_fill,e_sharpen,c_thumb,e_colorize:15,q_auto:eco,w_600/v1541167832/respondo/blog_wie_reagieren.jpg',
      imageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_thumb,e_colorize:39,q_auto:good,w_1080/v1541167832/respondo/blog_wie_reagieren.jpg',
      headerImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_fill,g_center,h_315,w_1000/v1548337660/respondo/blog_wie_reagieren.jpg',
      markdown: '# Wie reagiere ich auf schlechte Bewertungen?\n'
        + '\n'
        + 'Die rasant an Bedeutung gewinnende Bewertungskultur ist Fluch und Segen zugleich. Heute ist jeder Kritiker, ob man es will oder nicht. Mit [Google](https://www.google.de/maps?hl=de&tab=wl), [Facebook](https://www.facebook.com), [TripAdvisor](https://www.tripadvisor.de) und Co kommt die Flut an Bewertungen und Kommentaren auch zu den Gastronomen. Da kann auch mal eine schlechte Bewertung auftauchen. Diese zu ignorieren ist aber nicht der einzige Weg. Denn manchmal kann man diese durch effektiven Kundenkontakt in eine gute umwandeln. Wie ihr ein professionelles Beschwerdemanagement führen, und die schlechten Bewertungen zum Vorteil nutzen könnt, erfahrt ihr in in diesem Blogeintrag.\n'
        + '\n'
        + '**Warum sollte man auf schlechte Bewertungen antworten?**\n'
        + 'Man profitiert von nichts, wenn man schlechte Bewertungen ignoriert. Die Gäste werden ihre Meinung so nicht ändern. Antwortet man jedoch mit netten Worten, so fühlt sich der Bewerter gehört. Und das ist das Ziel der Bewerter! Sie wollen, dass sich ihr Feedback zu Herzen genommen wird. Wenn ihr so reagiert, überlegt sich der Kunde seine Meinung nochmal anders, besucht euer Lokal ein weiteres Mal und wandelt im besten Fall seine Bewertung zum Positiven um.\n'
        + '\n'
        + '**Der erste Schritt zum Erfolg**\n'
        + 'Sobald eine schlechte Bewertung eingeht, müsst ihr euch Zeit nehmen dem Problem im Lokal nachzugehen. Informiert euch intern in eurem Team wie es zu dieser schlechten Bewertung gekommen ist und wie sich die Situation laut dem Personal abgespielt hat. \n'
        + '\n'
        + '**Die Formulierung**\n'
        + 'Nach dem Recherchieren solltet ihr euch als erstes überlegen wie ihr die Antwort formulieren möchtet. Der Gast fühlt sich angesprochen sobald ihr in der Begrüßung seinen Namen erwähnt. Im Mittelteil solltet ihr sachlich bleiben, um den Gast nicht versehentlich zu reizen. Und vor allem solltet ihr dem Gast das Gefühl geben, dass euch seine Meinung wichtig ist. Habt ihr das im ersten Satz verdeutlicht, könnt ihr auf den Fall eingehen. Ist keine Rechtfertigung nötig, solltet ihr sie vermeiden. Am besten erwähnt ihr nochmal die Ziele des Restaurants. Das wichtigste Ziel, den Kunden immer glücklich zu machen und den bestmöglichen Service anzubieten, darf nicht fehlen. Zum Schluss sollte ein Call-to-Action eingebaut werden, um den Gast zu animieren euer Lokal nochmal aufzusuchen.\n'
        + ' \n'
        + '**Kundennähe steigert Image und Umsatz**\n'
        + 'Mit dem Gast herrscht somit eine direkte Kommunikation. Es stehen reale Menschen hinter den Bewertungen, sowie eurer Antwort. Dem Gast gefällt es, beachtet zu werden und fühlt sich dem Lokal verbundener. Das macht ein gutes Beschwerdemanagement aus. Eine Gast hat was anzumerken, woraufhin ihr verdeutlicht, dass die Anmerkung angenommen wird und wie wichtig Feedback ist. Der Gast wird somit besänftigt und im optimalen Fall gibt er dem Lokal eine weitere Chance. Für alle Gäste, die ein Lokal vor dem ersten Besuch anhand der Bewertungen prüfen, wird ein bestimmtes Bild übermittelt. Das Lokal weckt so bei den potenziellen Neukunden das Gefühl, dass ihr euch alle Probleme anhört und immer das Ziel verfolgt, dies umzusetzen und den bestmöglichen Service anzubieten. Die Gäste lassen sich somit in ihrer Wahl beeinflussen und empfinden eine gewisse Sympathie dem Lokal gegenüber. Somit entscheiden sich die Gäste das geprüfte Lokal zu besuchen und aus einer Beschwerde werden ganz schnell neue Kunden.',
    },
    {
      path: 'src/components/GeneralViews/Start/Blog/das_instagram_abc_der_gastronomie.md',
      title: 'Das Instagram ABC der Gastronomie',
      previewText: 'Kostenlos aber wirkungsvoll! Ein gepflegtes Instagram-Profil kommt vor allem bei der jüngeren Generation sehr gut an und ...',
      order: 3,
      // c_thumb,e_colorize:15,q_auto:eco,w_600 -> vorher
      previewImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/ar_16:9,c_fill,e_sharpen,c_thumb,e_colorize:15,q_auto:eco,w_600/v1548337657/respondo/aldo-delara-516065-unsplash.jpg',
      imageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_thumb,e_colorize:15,q_auto:good,w_1080/v1541166422/respondo/aldo-delara-516065-unsplash.jpg',
      headerImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_fill,g_auto,h_315,w_1000/v1548337660/respondo/aldo-delara-516065-unsplash.jpg',
      markdown: '# Das Instagram ABC der Gastronomie\n'
                + '\n'
                + 'Kostenlos aber wirkungsvoll! Ein gepflegtes Instagram-Profil kommt vor allem bei der jüngeren Generation sehr gut an. Wir wissen genau wovon wir reden. Denn Respondo - Bewertungsmanagement gehört zu [Foodguide](https://thefoodguide.de/), europas größten sozialen Netzwerk für Gastronomie. Mit über 1 Millionen Follower auf diversen Accounts haben wir viele Erfahrung sammeln können, die wir hier gerne mit euch teilen.\n'
                + '\n'
                + 'Instagram ist die perfekte Marketingplattform für Restaurants, Cafés und Bars, denn Menschen lieben es, ihr Essen, eine gesellige Runde oder den abendlichen Drink zu posten. Mit ein wenig Aufwand lockst du über Instagram neue Kunden in dein Lokal und machst dich auf dem sozialen Netzwerk sichtbar.\n'
                + '\n'
                + '**Baue ein Portfolio deines Restaurants auf**\n'
                + 'Lass dein Instagram-Profil deine Speisekarte sein! Leute besuchen deinen Kanal um raus zu finden, ob sich ein Besuch lohnt. Ob es etwas gibt, das ihnen schmecken könnte. Also biete einfach eine bunte Auswahl deiner Speisen an, die so gut aussehen, dass man vorbei kommen muss!\n'
                + '\n'
                + '**Stelle Neuheiten vor**\n'
                + '\n'
                + 'Du hast deine Karte erweitert oder bietest saisonale Speisen an? Erzähle deiner Instagram-Community davon! Und nicht vergessen: Immer fleißig auch andere Fotos kommentieren und liken, sodass potentielle Gäste überhaupt wissen, dass es deinen Kanal gibt!\n'
                + '\n'
                + '**Verwende eine richtige Kamera**\n'
                + '\n'
                + 'Na klar, Instagram ist für Handyfotos gemacht, nicht umsonst, kann man nur von diesem Fotos hochladen. Trotzdem heißt das nicht, dass keine professionellen Aufnahmen gepostet werden können. Fotografiere Deko, Interior oder toll angerichtete Speisen mit einer Spiegelreflexkamera und lade sie dann auf dein Handy. Die Qualität der Fotos ist einfach besser, was deinem Profil auf den ersten Blick einen hochwertigeren Look verpasst.\n'
                + '\n'
                + '**Setze dein Essen richtig in Szene**\n'
                + '\n'
                + 'foodporn-Profis raten Essen immer in natürlichem Licht zum Beispiel an einem Fenster zu fotografieren und auf keinen Fall einen Blitz zu verwenden. Beim Hintergrund ist wichtig, dass er dem Gericht nicht die Show stiehlt. Ruhige Flächen bieten sich an, wie ein tolles Holzbrett, eine Schieferplatte oder ein glatter weißer Tisch. Wichtig nur: Die Farbe sollte sich nicht mit den Zutaten beißen. Insidertipp: Ein sattes Auge ist kritischer ;)\n',
    },
    {
      path: 'src/components/GeneralViews/Start/Blog/das_restaurant_bewertungen_abc_wie_erhalte_ich_mehr_bewertungen.md',
      title: 'Das Restaurant Bewertungen ABC - wie erhalte ich mehr Bewertungen',
      previewText: 'Zweifellos macht es einen Unterschied, ob Sie mit Ihrem Unternehmen auf TripAdvisor vertreten sind oder nicht.',
      order: 6,
      previewImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/ar_16:9,c_fill,e_sharpen,c_thumb,e_colorize:15,q_auto:eco,w_600/v1541166422/respondo/blog_image_one.jpg',
      imageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_thumb,e_colorize:15,q_auto:good,w_1080/v1541166422/respondo/blog_image_one.jpg',
      headerImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_fill,g_auto,h_315,w_1000/v1548337660/respondo/blog_image_one.jpg',
      markdown: '# Das Restaurant Bewertungen ABC - wie erhalte ich mehr Bewertungen?\n'
                + '\n'
                + 'Zweifellos macht es einen Unterschied, ob Sie mit Ihrem Unternehmen auf TripAdvisor vertreten sind oder nicht. Mit über 570 Millionen Bewertungen übt das Feedback-Portal einen großen Einfluss auf den Entscheidungsprozess jedes Verbrauchers aus, der mit Freunden einen Urlaub oder ein Abendessen in einem Restaurant buchen möchte. Eine kürzlich von Comscore durchgeführte Studie hat ergeben, dass rund 60 % der Reisenden TripAdvisor konsultieren, bevor sie eine Buchung vornehmen.\n'
                + '\n'
                + 'Reicht Ihre Präsenz auf TripAdvisor aus, zukünftige Gäste von Ihrem Unternehmen zu überzeugen? Oder müssen Sie sich aktiv um Ihr Image kümmern und Ihre Online Reputation optimieren?\n'
                + '\n'
                + '**Wie funktioniert das TripAdvisor-Ranking?**\n'
                + 'Zuallererst ist es wichtig zu wissen, wie das TripAdvisor Ranking funktioniert. Es basiert auf drei Werten: Qualität, Quantität, Aktualität. Es ist unerlässlich, dass Sie viele Bewertungen (Quantität), viele positive Bewertungen (Qualität) und regelmäßig neue Bewertungen (Aktualität) haben. Die Qualität, die jüngsten Veröffentlichungen und die Anzahl der Rezensionen sind essentiell, um das Ranking zu verbessern oder einen der oberen Plätze zu halten.\n'
                + '\n'
                + 'TripAdvisor sagt dazu:\n'
                + '\n'
                + '“Der Popularitäts-Ranglisten-Algorithmus soll ein statistisches Maß des Vertrauens in Bezug auf aktuelle Erfahrungen mit einem Unternehmen bieten. Die Anzahl der von einem Unternehmen im Laufe der Zeit angesammelten Bewertungen ist direkt proportional zu der Menge an Informationen über die potenzielle Erfahrung, die die Verbraucher erwarten können. Erreichen Sie eine große Menge an Bewertungen, können wir die Position des Unternehmens im Ranking genauer vorhersagen.”\n'
                + '\n'
                + 'So wird ein kleines Hotel, das nicht viele Zimmer hat, im direkten Vergleich mit einer großen Kette – die mit einer größeren Anzahl von Gästen vermutlich wesentlich mehr Bewertungen sammeln kann – nicht benachteiligt. Mit einer Bewertungs-Management-Lösung haben Sie die Möglichkeit, TripAdvisor-Bewertungen durch automatisierte Prozesse zu sammeln und zu überwachen.\n'
                + '\n'
                + '**Wie verbessere ich mein Ranking?**\n'
                + 'Sammeln Sie regelmäßig positive Bewertungen, indem Sie Ihre Gäste zu TripAdvisor einladen. Customer Alliance ist Partner von TripAdvisor und bietet eine besonders einfache, automatisierte Lösung an.\n'
                + '\n'
                + 'Sie können alle Ihre Gäste umleiten und sie einladen, Ihr Unternehmen direkt auf TripAdvisor zu bewerten, auch wenn sie dort kein Konto haben. Grundsätzlich vereinfacht sich der gesamte Prozess, um eine Bewertung auf TripAdvisor zu hinterlassen: Die Daten der Gäste, die Sie im Backend von Customer Alliance gesammelt haben (Name, E-Mail, Stadt usw.), werden direkt in den TripAdvisor-Fragebogen kopiert.\n'
                + '\n'
                + 'Dies erhöht die Wahrscheinlichkeit, dass Gäste eine Bewertung abgeben: Ein einfacher Prozess führt immer zu besseren Ergebnissen! Die Gastdaten werden zwischen Customer Alliance und TripAdvisor nur geteilt, wenn der Gast seine Zustimmung erteilt.\n'
                + '\n'
                + '**Behalten Sie Ihre Online Reputation unter Kontrolle**\n'
                + 'Neben dem Sammeln von Bewertungen ist es wichtig zu überprüfen, was Gäste über Sie schreiben. Beschränken Sie sich also nicht darauf, Ihre Gäste zur Bewertung Ihres Unternehmens einzuladen, sondern denken Sie auch daran, regelmäßig Ihre Online Reputation zu analysieren.\n'
                + '\n'
                + 'Customer Alliance hilft Ihnen, den Inhalt Ihrer Bewertungen durch statistische und semantische Analysen zu überwachen. Auf diese Weise haben Sie immer einen detaillierten Überblick über das, was im Internet über Sie gesagt wird.\n'
                + '\n'
                + 'Aber das ist noch nicht alles. Wir geben Ihnen auch die Möglichkeit, Ihre Position und die Ihrer Mitbewerber im TripAdvisor City Ranking zu analysieren.\n'
                + '\n'
                + '**Kann ich nur positive Bewertungen auf TripAdvisor veröffentlichen?**\n'
                + 'TripAdvisor ist ein offenes Portal, was bedeutet, dass jeder über seine Erfahrungen schreiben kann – ob positiv oder negativ. Wir glauben, dass eine negative Bewertung, wenn sie richtig gemanagt werden, ein großer Gewinn für Ihre Marketingstrategie sein kann. TripAdvisor berichtet auch von einer Studie von Phocuswright, die besagt, dass 85 % der Nutzer denken, dass eine empathische Reaktion auf eine kritische Bewertung einen Mehrwert für das Hotel darstellt. Zudem entscheiden sich 65 % der Nutzer lieber für ein Unternehmen, dass auf Bewertungen – egal ob positive oder kritische – reagiert.\n'
                + '\n'
                + '**Worauf warten Sie noch?**\n'
                + 'Beginnen Sie jetzt, Ihre Online Reputation aktiv zu verwalten. Fordern Sie unten eine kostenlose Beratung unserer Lösung an und erklimmen Sie das TripAdvisor City Ranking!\n',
    },
    {
      path: 'src/components/GeneralViews/Start/Blog/die_macht_der_online_reputation_bewertungsplattformen_gewinnen_stetig_an_relevanz.md',
      title: 'Die Macht der Online Reputation - Bewertungsplattformen im Höhenflug',
      previewText: 'Unter Online Reputation versteht man den Ruf einer Person, eines Unternehmens oder einer Einrichtung im Internet. Genau so, wie man in der realen Welt...',
      order: 5,
      previewImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/ar_16:9,c_fill,e_sharpen,c_thumb,e_colorize:15,q_auto:eco,w_600/v1541167832/respondo/blog_image_three.jpg',
      imageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_thumb,e_colorize:39,q_auto:good,w_1080/v1541167832/respondo/blog_image_three.jpg',
      headerImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_fill,g_auto,h_315,w_1000/v1548337660/respondo/blog_image_three.jpg',
      markdown: '# Die Macht der Online Reputation - Bewertungsplattformen gewinnen stetig an Relevanz\n'
                + '\n'
                + 'Unter Online Reputation versteht man den Ruf einer Person, eines Unternehmens oder einer Einrichtung im Internet. Genau so, wie man in der realen Welt einen guten oder schlechten Ruf haben kann, läuft es auch im Digitalen ab. Oftmals nur leider in einer viel größeren Dimension, denn das Internet vergisst nichts, ist weltweit verfügbar und ermöglicht es, Meldungen, Fotos oder Videos in Windeseile zu verbreiten – Stichwort: Fake News. Wie ein Laubfeuer verteilen sich die Gerüchte und können – ob wahr oder falsch – nicht kontrolliert werden und oftmals ganze Existenzen zerstören. Zugleich bietet das Netz aber auch Menschen eine Stimme, die unter den gegebenen Umständen keinerlei Möglichkeit hätten, solch ein großes Publikum zu erreichen, und kreiert eine besondere Aufmerksamkeit – bestes Beispiel zur Zeit: #metoo.\n'
                + '\n'
                + '  \n'
                + '\n'
                + '**Überlassen Sie Ihre Online Reputation nicht dem Zufall**\n'
                + 'Diese Entwicklung und die mit ihr einhergehende Dynamik betrifft uns alle, ob wir wollen oder nicht. Denn auf das, was andere über uns schreiben, was Dritte im Internet über uns veröffentlichen – darauf haben wir keinen Einfluss.\n'
                + '\n'
                + '  \n'
                + '\n'
                + 'Was wir allerdings tun können, ist aktiv dagegen zu steuern: mit einem proaktiven Online Reputations Management gelingt es, gezielt authentische und den eigenen Vorstellungen entsprechende Informationen über die eigene Person, das Unternehmen oder die angebotene Dienstleistung zu verbreiten und so ein stimmiges Image aufzubauen.\n'
                + '\n'
                + '  \n'
                + '\n'
                + 'Für Unternehmen und Dienstleister liegt ein Schwerpunkt hierbei sicherlich im aktiven Managen der Kundenbewertungen. So wie früher Empfehlungen mündlich und im Freundes-und Familienkreis weitergegeben wurden, passiert dies nun online: ob Hotel oder Arzt, Kosmetikstudio oder Restaurant – vor Kundenbewertungen auf den großen Bewertungsportalen im Internet bleibt heute niemand mehr “verschont”.\n'
                + '\n'
                + '  \n'
                + '\n'
                + 'Grund genug, diese Bewertungen und die damit einhergehende Beurteilung ihres Unternehmens nicht dem Zufall zu überlassen. Denn rund 90 % aller Nutzer informieren sich via Rezensionen über ein Unternehmen, bevor sie dieses besuchen oder die angebotenen Dienste in Anspruch nehmen.\n'
                + '\n'
                + '  \n'
                + '\n'
                + 'Aufgrund der Vielzahl an unterschiedlichen Bewertungsplattformen – von Facebook bis Google oder branchenspezifischeren wie TripAdvisor oder Jameda – fällt es im Arbeitsalltag oft schwer, den Überblick zu behalten und ausreichend Ressourcen bereitzustellen, um angemessen auf das Feedback zu reagieren.\n'
                + '\n'
                + '  \n'
                + '\n'
                + 'Mit aktivem Bewertungsmanagement zu einer besseren Online Reputation\n'
                + '\n'
                + 'Zeit – und somit auch Geldersparnis bietet Ihnen hier der Einsatz einer professionellen Bewertungs-Management-Lösung, mit der Sie Ihre Kunden gezielt um Bewertungen bitten, diese sammeln, analysieren und auswerten und auf den für Sie relevanten Plattformen veröffentlichen können.\n'
                + '\n'
                + '  \n'
                + '\n'
                + 'Digitalisierung sei Dank verläuft ein Großteil dieser Prozesse automatisch und die moderne Technik übernimmt für Sie einen Großteil der Arbeit, die Sie ansonsten manuell erledigen müssten. So profitieren Sie nicht nur von mehr – und im Schnitt besseren – Bewertungen, Sie haben auch einen Einfluss darauf, wo diese veröffentlicht werden und können das Feedback Ihrer Kunden nutzen, um Ihr Angebot stetig zu verbessern, Schwachstellen zu identifizieren und zu beheben.\n'
                + '\n'
                + '  \n'
                + '\n'
                + 'Gleichzeitig wird es Ihnen auf lange Sicht gelingen, sich im Ranking der jeweiligen Bewertungsplattformen kontinuierlich weiter nach oben zu verbessern und die gute Position zu halten. Auch in den Google Suchergebnissen können Sie mit einem besseren Ergebnis rechnen. Somit sind Sie für potentielle Kunden nicht nur leichter zu finden, sondern wirken zugleich auch attraktiver und vertrauenswürdiger.\n'
                + '\n'
                + '  \n'
                + '\n'
                + '**Online Reputations Management: Mehr als “nur” Bewertungen sammeln**\n'
                + 'Mit dem aktiven Managen Ihrer Bewertungen haben Sie schon einen entscheidenden Beitrag zu einem gelungenen Online Reputations-Management geleistet. Doch gibt es weitere Faktoren, die eine wichtige Rolle für einen langfristigen Erfolg spielen, und die ebenfalls Teil des professionellen Online Reputations Managements sind.\n'
                + '\n'
                + '  \n'
                + '\n'
                + '**Googeln Sie sich selbst!**\n'
                + 'Googeln Sie sich (bzw. Ihr Unternehmen) doch einmal selbst und schauen Sie sich die Suchergebnisse genauer an. Öffnen Sie einen neuen Tab im Browser im Inkognito-Modus, denn nur so werden Ihnen die neutralen Suchergebnisse angezeigt, wie andere Nutzer sie ebenfalls sehen. Ihre Suchmaschine kennt Sie, Ihre Vorlieben und Gewohnheiten, und ohne Inkognito-Modus erhalten Sie ein auf Sie persönlich angepasstes Ergebnis, das nicht repräsentativ ist.\n'
                + '\n'
                + '  \n'
                + '\n'
                + '**Was sehen Sie? Und gefällt Ihnen, was Sie sehen?**\n'
                + 'Wie oben bereits erwähnt, haben Sie wenig Einfluss darauf, was Dritte über Sie posten. Was Sie hingegen steuern können, ist das, was Sie selbst veröffentlichen oder in Ihrem Auftrag von anderen, wie Bloggern oder Kooperationspartnern, veröffentlichen lassen.\n'
                + '\n'
                + 'Ihre eigene Homepage sollte in der Suche an oberster Stelle erscheinen. Ist dies nicht der Fall, nehmen Sie sich Zeit und optimieren die Webseite, indem Sie beispielsweise regelmäßig neuen Content veröffentlichen – denn die Suchmaschinen lieben frischen Content und danken es mit einem besseren Platz im Ranking.\n'
                + '\n'
                + 'Des Weiteren empfiehlt es sich, auf allen relevanten Portalen mit einem eigenen Profil vertreten zu sein. Auch so belegen Sie gleich mehrere Plätze in den Suchergebnissen und verdrängen gegebenenfalls unliebsame Einträge auf die hinteren Plätze. Gleichzeitig stärken Sie Ihre Reputation, denn viele Kunden und Gäste vertrauen den großen, bekannten Plattformen – und dieses Vertrauen überträgt sich direkt auf das jeweilige Profil Ihres Unternehmens.\n'
                + '\n'
                + '**Online Reputations Management in den sozialen Netzwerken**\n'
                + 'Auch hier gilt: sofern noch nicht geschehen, legen Sie auf den für Ihr Unternehmen relevanten Plattformen Accounts an. Ob Facebook, Instagram, Twitter oder Snapchat – Sie müssen längst nicht überall vertreten sein, zwei soziale Netzwerke sollten Sie jedoch bespielen, um Ihre Präsenz im Netz – und somit Ihre Auffindbarkeit und Ihre Online Reputation – zu stärken.\n'
                + '\n'
                + 'Veröffentlichen Sie regelmäßig neue Inhalte. Überlegen Sie sich hierbei genau, welches Bild Sie von sich und Ihrem Unternehmen in der Öffentlichkeit präsentieren möchten.\n'
                + 'Sie können die sozialen Medien nicht nur nutzen, um Angebote und Aktionen zu promoten, neue Kunden zu gewinnen und mit Stammkunden in Kontakt zu bleiben.\n'
                + 'Gleichzeitig fungieren Portale wie Facebook oder Instagram wie ein virtuelles Schaufenster, das einen Blick in Ihr Unternehmen gewährt. Veröffentlichen Sie also auch regelmäßig Content zu Ihren Alleinstellungsmerkmalen, die Sie von Ihren Wettbewerbern unterscheiden.\n'
                + '\n'
                + 'Oder lassen Sie doch einfach mal Ihre Mitarbeiter übernehmen – ein sogenanntes Instagram-Takeover. Leiten Sie ein Hotel, dann lassen Sie eine Woche lang Ihren Koch Fotos und kurze Videos (die natürlich gerne der für Social Media zuständige Mitarbeiter posten kann) aus dem Küchenalltag und spannende Insights, die der Gast sonst nicht zu Gesicht bekommt, aufnehmen. In der Folge-Woche ist dann vielleicht der Gärtner dran oder Ihr Concierge. So haben Sie nicht nur originelle Inhalte abseits der allgegenwärtigen Food- und Pool-Fotos, sondern werden für Ihre Gäste auch nahbarer und stellen eine persönliche Bindung her.\n'
                + '\n'
                + 'Animieren Sie Ihre Gäste, ebenfalls Content zu posten und Sie darauf zu verlinken. Kreieren Sie einen eigenen, individuellen Hashtag für Ihr Unternehmen. Und, besonders wichtig:\n'
                + '\n'
                + '**Die Privatsphäre-Einstellung**\n'
                + 'Ja, ja, es ist 2018 und eigentlich ist dieses Thema fast schon eine olle Kamelle. Doch leider kann man es nicht oft genug sagen: Überprüfen Sie regelmäßig Ihre Privatsphäre-Einstellungen und achten Sie darauf, dass die Welt nur das sieht, was Sie auch sehen soll und was das professionelle Image Ihrer Reputation unterstützt.\n'
                + '\n'
                + '**Korrekte Angaben**\n'
                + 'Achten Sie darauf, dass auf jeder Webseite, auf jedem Portal, die richtigen und aktuellen Informationen bereitgestellt sind. Dies betrifft beispielsweise Ihre Öffnungszeiten, aber auch den Standort Ihres Unternehmens bei Google Maps. Sie möchten doch schließlich nicht, dass Ihre Kunden vor verschlossener Türe oder der falschen Adresse stehen?\n'
                + '\n'
                + '**Setzen Sie auf Qualität**\n'
                + 'Wir leben in einer visuellen Welt. Photoshop und Filter legen einen perfektionistischen Schleier über die Bilderflut, die tagtäglich auf uns einprasselt. Das verändert den Blick und somit auch die Ansprüche und Erwartungen. Nehmen Sie sich Zeit und beurteilen Sie die Fotos Ihres Hotels, Ihres Restaurants mit den Augen Ihrer Gäste. Wirken Sie einladend, hell, freundlich und trotzdem authentisch? Oder gibt es hier Nachbesserungsbedarf? Die Investition in einen guten Fotografen wird sich lohnen, denn Sie können die Bilder nicht nur für Ihre eigene Webseite, sondern auch für Portale wie HolidayCheck, TripAdvisor, Facebook, Yelp und Co. nutzen und sich von den unscharfen Handyfotos Ihrer Mitbewerber abheben.\n',
    },
    {
      path: 'src/components/GeneralViews/Start/Blog/so_verbessere_ich_meinen_customer_service.md',
      title: 'So verbessere ich meinen Customer Service',
      previewText: 'Im Customer Service dreht sich alles um die Interaktion mit dem Kunden, um neue User zu gewinnen und eine langanhaltende Bindung zu erzielen. Nicht nur das Produkt selber, auch ...',
      order: 4,
      previewImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/ar_16:9,c_fill,e_sharpen,c_thumb,e_colorize:15,q_auto:eco,w_600/v1541167832/respondo/aleksander-borzenets-744778-unsplash.jpg',
      imageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_thumb,e_colorize:39,q_auto:good,w_1080/v1541167832/respondo/aleksander-borzenets-744778-unsplash.jpg',
      headerImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_fill,g_auto,h_315,w_1000/v1548337660/respondo/aleksander-borzenets-744778-unsplash.jpg',
      markdown: '# So verbessere ich meinen Customer Service\n'
                + '\n'
                + 'Im Customer Service dreht sich alles um die Interaktion mit dem Kunden, um neue User zu gewinnen und eine langanhaltende Bindung zu erzielen. Nicht nur das Produkt selber, auch der Service der mitgeliefert wird, bestimmt die Erfahrung des Kunden mit deinem Unternehmen - und ob er bei euch bleibt. Wie du deinen Customer Service auf ein neues Level hebst, erfährst du in unseren 4 Tipps. Deine Kunden werden es merken.\n'
                + '\n'
                + '**Persönlicher Support**\n'
                + '\n'
                + 'Dieser beginnt ganz einfach damit, den Namen eines Kunden in Telefongesprächen zu erinnern und ihn damit anzusprechen. Besonders gut funktioniert das, wenn du ihn gleich, nachdem du ihn gehört hast wiederholst. Diese Reproduktion wird im Gedächtnis nachhallen und du vergisst ihn weniger schnell. Der Kunde freut sich, wenn du dir die Zeit nimmst auf Socialmedia Kommentare und Nachrichten persönlich zu antworten. Personalisierte Karten zu Weihnachten kommen ebenfalls gut an. Wichtig ist, dem User das Gefühl zu geben, dass er als Individuum nicht nur wahrgenommen wird, sondern wichtig ist. Beim Verwalten und Beobachten von Online Bewertungen greift Respondo - Bewertungsmanagement gerne unter die Arme, damit der Support wirklich keine Lücken aufweist und jede Stimme gehört wird.\n'
                + '\n'
                + '**Erst zuhören, dann handeln**\n'
                + '\n'
                + 'Gut zu zu hören, ist der Schlüssel zu erstklassigem Customer Service. Der Kunde muss das Gefühl haben, verstanden zu werden. Um diesen Service zu gewährleisten, ist es gut, das Gesagte des Gegenübers grob zusammenzufassen und zu wiederholen um zu überprüfen, ob alles richtig aufgenommen wurde. Weitere Gegenfragen helfen, das Problem zu identifizieren und Lösungsvorschläge machen zu können.\n'
                + '\n'
                + '**Stelle die richtigen Mitarbeiter ein**\n'
                + '\n'
                + 'Egal, in welcher Branche dein Unternehmen tätig ist, die Mitarbeiter sind das A und O. Ob in der Gastronomie, im Callcenter oder Pflegedienst, in den meisten Gewerben, haben die Angestellten direkten Kontakt mit den Kunden und sind somit verantwortlich für die Erfahrung, die der Kunde mit deinem Unternehmen macht. Achte bei der Auswahl deiner Mitarbeiter auf Zuverlässigkeit, Ausstrahlung und ehrliches Interesse an deinem Unternehmen und dem damit verbundenen Job, den sie ausführen.\n'
                + '\n'
                + '**Seid rund um die Uhr erreichbar**\n'
                + '\n'
                + 'Wir in unserem kleinen Schichtplaner-Team wissen selber, dass durchgängiger Service sehr schwierig zu gewährleisten ist. Mitarbeiter brauchen Urlaubstage und werden manchmal krank. Trotzdem, versucht einfach, euer Möglichstes zu tun. Setzt euch ein Limit von höchstens 24 Stunden, in denen ihr jede Anfrage beantwortet habt. Gerade bei Online-Angeboten ist es wichtig, möglichst lange telefonisch erreichbar zu sein. Dafür kann es praktisch sein, Mitarbeiter in Schichten einzusetzen. Natürlich kann dieser Service nicht immer gegeben sein, richtet dafür ein Online Helpcenter ein, das einige Fragen direkt beantworten kann und verweist darauf am besten direkt in einer automatischen Abwesenheitsmail.\n',
    },
    {
      path: 'src/components/GeneralViews/Start/Blog/so_verbesserst_du_dein_restaurant_management_in_10_tagen.md',
      title: 'So verbesserst du dein Restaurant Management in 10 Tagen',
      previewText: 'Es ist ein harter Job, alle Aufgaben zu erfüllen, die im täglichen Leben eines Restaurants anfallen. Diese Tipps können dein Restaurant Management vereinfachen und legen die Aufmerksamkeit auf ...',
      order: 2,
      previewImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/ar_16:9,c_fill,e_sharpen,c_thumb,e_colorize:15,q_auto:eco,w_600/v1541167832/respondo/robert-bye-98949-unsplash.jpg',
      imageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_thumb,e_colorize:39,q_auto:good,w_1080/v1541167832/respondo/robert-bye-98949-unsplash.jpg',
      headerImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_fill,g_auto,h_315,w_1000/v1548337660/respondo/robert-bye-98949-unsplash.jpg',
      markdown: '# So verbesserst du dein Restaurant Management in 10 Tagen\n'
                + '\n'
                + 'Es ist ein harter Job, alle Aufgaben zu erfüllen, die im täglichen Leben eines Restaurants anfallen. Diese Tipps können dein Restaurant Management vereinfachen und legen die Aufmerksamkeit auf die wirklich wichtigen Dinge.\n'
                + '\n'
                + '**Lerne zu delegieren**\n'
                + '\n'
                + 'Du kannst nicht alles selber machen. Lerne kleinere Aufgaben abzugeben, damit du dich auf wichtigere Vorgänge konzentrieren kannst und nichts vernachlässigen musst. Suche dabei Mitarbeiter aus, bei denen du weißt, dass sie dich nicht im Stich lassen. Das bereitet sie nicht nur auf spätere Verantwortlichkeiten vor, sondern lässt sie auch deine Anerkennung spüren.\n'
                + '\n'
                + '**Rede mit den Gästen**\n'
                + ' \n'
                + 'Halte dich bei den Gästen auf und frage sie nicht nur, ob es geschmeckt hat, sondern auch, wie das sonstige Erlebnis war. Sehr aufschlussreich ist die Frage, was sie ihren Freunden über den Besuch erzählen würden. So fühlen sich die Gäste nicht nur umsorgt und ernst genommen, das Feedback ist außerdem sehr nützlich für die weitere Entwicklung des Restaurants. Falls du auf der Fläche selber nicht genug Feedback einholen kannst nutze die digitalen Kanäle wie Bewertungsplattformen für dich, in dem du das Feedback analysiert und geschickt darauf eingehst. Respondo hilft dir gern dabei den Überblick zu behalten und Zeit zu sparen.\n'
                + '\n'
                + '**Das Servicepersonal muss die Gerichte und Getränke kennen**\n'
                + '\n'
                + 'Diese Details machen den Besuch in einem sehr guten Restaurant aus. Die Kellner können nicht nur beschreiben, wie die Speisen schmecken, sondern auch welcher Wein dazu passt und warum. Auch bei Aushilfspersonal, das nur manchmal da ist und meist keine Ausbildung in der Gastronomie hat, sollte dieses Wissen Voraussetzung sein. Das ist nicht nur schön für den Gast, sondern notwendig, beispielsweise bei Allergien von Besuchern.\n'
                + '\n'
                + '**Nutze Social Media**\n'
                + '\n'
                + 'Es überlebenswichtig für Restaurants Werbung zu machen - nur so erfahren die Leute von der eigenen Existenz. Online gibt es mittlerweile viele günstige Wege auf sich aufmerksam zu machen. Profile auf Social Media sind kostenlos und sehr effektiv, denn tolle Food-Fotos werden mit Vorliebe geteilt und geliked. Vor allem die junge Zielgruppe erreicht man nur über eine starke, moderne Online-Präsenz. Viele Gastronomen zählen auch die Bewertungsplattformen als Social Media. Entscheide selber unter welchem Überbegriff die diese Kanäle behandeln willst und briefe entsprechend dein Personal.\n'
                + '\n'
                + '**Beobachte deine Kellner**\n'
                + '\n'
                + 'Das Personal ist ausschlaggebend für das Erlebnis des Gastes, deswegen ist es sehr wichtig, die Kellner genau zu beobachten. Ist immer ein Lächeln im Gesicht, wird sich vorgestellt und werden Empfehlungen ausgesprochen? Es ist gut, die Mitarbeiter nach solchen Beobachtungen zu loben, damit sich die Vorgänge einprägen. Kritik wird am besten nett verpackt in kleine Hinweise, damit der Abend mit der gleichen Motivation weitergehen kann.\n'
                + '\n'
                + '**Lächeln nicht vergessen**\n'
                + '\n'
                + 'Die Stimmung des Managers überträgt sich auf das Servicepersonal und damit auf die Gäste. Also ist es Pflicht, als Führungsperson mit gut gelaunten Beispiel voran zu gehen. Liebevolle Strenge ist der richtige Weg, damit Stimmung und Konzentration nicht flöten gehen.\n',
    },
    {
      path: 'src/components/GeneralViews/Start/Blog/wie_gewinne_ich_volle_kontrolle_ueber_meine_online_reputation.md',
      title: 'Wie gewinne ich volle Kontrolle über meine Online Reputation',
      previewText: 'Was genau ist Online Reputation? Damit gemeint ist der digitale Ruf von Personen,Marken und auch Gastronomien gemeint. Dabei darfst du nicht vergessen ...',
      order: 1,
      previewImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/ar_16:9,c_fill,e_sharpen,c_thumb,e_colorize:15,q_auto:eco,w_600/v1541167832/respondo/rawpixel-570911-unsplash.jpg',
      imageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_thumb,e_colorize:39,q_auto:good,w_1080/v1541167832/respondo/rawpixel-570911-unsplash.jpg',
      headerImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_fill,g_auto,h_315,w_1000/v1548337660/respondo/rawpixel-570911-unsplash.jpg',
      markdown: '# Wie gewinne ich volle Kontrolle über meine Online Reputation?\n'
                + '\n'
                + 'Was genau ist Online Reputation? Damit gemeint ist der digitale Ruf von Personen,Marken und auch Gastronomien gemeint. Dabei darfst du nicht vergessen, dass das Internet eine riesige Reichweite hat und vergisst nichts. Deshalb ist der Ruf im Internet schwieriger zu kontrollieren als der Ruf im realen Leben. Meldungen, Fotos oder Videos verbreiten sich in Sekunden – auch wenn sie nicht unbedingt der Wahrheit entsprechen.\n'
                + '\n'
                + 'Falsche Informationen können dir schaden, aber das Internet kann dir helfen, neue Kunden zu gewinnen, die sonst nie auf dich aufmerksam geworden wären.\n'
                + '\n'
                + 'Was andere über uns schreiben oder veröffentlichen, können wir nicht beeinflussen.\n'
                + '\n'
                + 'Aber wir können mit positiven Posts unsere Online Reputation verbessern. Baue über soziale Netzwerke gezielt auf, was du vermitteln möchtest. Zeig dich von deiner besten Seite! Wie du dich im Internet präsentierst, beeinflusst deinen Erfolg.\n'
                + '\n'
                + 'Du verbesserst dein Ranking auf Bewertungsplattformen, indem du die positiven Bewertungen sammelst und deine Gäste zu Bewertungsplattformen einlädst. Bitte sie, nach ihrem Besuch eine Bewertung über dich zu verfassen. Dies ist einer der einfachsten Wege, um möglichst viele gute Rezensionen zu sammeln. Außerdem ist es wichtig, die Kontrolle über deine Online Reputation zu behalten. Überprüfe regelmäßig, was deine Gäste über dein Restaurant schreiben und analysiere die Rezensionen.\n'
                + '\n'
                + 'Natürlich kann ein Gast über dein Restaurant auch eine eher negative Rezension schreiben. Aber wenn du richtig damit umgehst, kann es dir in deiner Marketingstrategie weiterhelfen. Für deine Nutzer ist eine empathische Antwort auf negative Kritik ein Zeichen, dass du nahbar und sympathisch bist. Das steigert den Mehrwert deines Restaurants!\n'
                + '\n'
                + 'Das Internet vergisst nie- also solltest du auch nicht vergessen, dass deine Antworten und Bewertungen im Internet eine wichtige Rolle für den Erfolg deines Restaurants spielen!\n'
                + '\n'
                + 'Überlasse deine Online Reputation nicht dem Zufall und nutze die Respondo.app.\n'
                + '\n'
                + 'Deine Vorteile:\n'
                + '\n'
                + '-   Du verpasst keine Bewertungen mehr\n'
                + '    \n'
                + '-   Du kannst direkt antworten\n'
                + '    \n'
                + '-   Du wirst bei auffälligen Bewertungen alarmiert\n'
                + '    \n'
                + '-   Du kannst Trends erkennen und schnell reagieren\n'
                + '    \n'
                + '-   Das spart dir viel Zeit\n'
                + '  \n'
                + 'Überlasse deine Online Reputation nicht dem Zufall, starte jetzt mit den deinem Zugang bei **Respondo.app**\n',
    },
    {
      path: 'src/components/GeneralViews/Start/Blog/wie_verbessere_ich_meine_platzierung_im_ranking_der_suchmaschinen_das_seo_einmaleins.md',
      title: 'Wie verbessere ich meine Platzierung im Ranking der Suchmaschinen - das SEO Einmaleins',
      previewText: 'SEO ist die Abkürzung für “Search Engine Optimization”, hierzulande auch bekannt als Suchmaschinenoptimierung ...',
      order: 7,
      previewImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/ar_16:9,c_fill,e_sharpen,c_thumb,e_colorize:15,q_auto:eco,w_600/v1541167832/respondo/brad-stallcup-783086-unsplash.jpg',
      imageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_thumb,e_colorize:39,q_auto:good,w_1080/v1541167832/respondo/brad-stallcup-783086-unsplash.jpg',
      headerImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_fill,g_auto,h_315,w_1000/v1548337660/respondo/brad-stallcup-783086-unsplash.jpg',
      markdown: '# Wie verbessere ich meine Platzierung im Ranking der Suchmaschinen - das SEO Einmaleins\n'
                + '\n'
                + 'Eine berechtigte Frage, in die wir direkt mit einer Definition einsteigen.\n'
                + '\n'
                + '**Was genau versteht man eigentlich unter SEO Traffic?**\n'
                + 'SEO ist die Abkürzung für “Search Engine Optimization”, hierzulande auch bekannt als Suchmaschinenoptimierung. Oftmals wird unter SEO Traffic jener Anteil der Webseitenbesucher verstanden, der über eine Suchmaschine auf die jeweilige Seite gelangt. Diese Bezeichnung verwässert das Ergebnis jedoch, schließlich ist der SEO Traffic eine relevante Kennzahl, wenn es um das Messen des Erfolges geht. Korrekter ist es, alle Besucher, die über eine Suchmaschine auf die Seite kommen, als “Suchmaschinen-Traffic” zu bezeichnen, und die Definition für SEO Traffic, als Teil dieser großen Gruppe, enger zu fassen. Denn SEO Traffic basiert eben auch auf erfolgreicher SEO-Arbeit, also dem gezielten Optimieren der Inhalte einer Webseite, um ein besseres Ranking in den Suchergebnissen und somit mehr Nutzer zu generieren.\n'
                + '\n'
                + '**Wie funktioniert SEO?**\n'
                + 'Nun gibt es zahlreiche Tipps und schlaue Anleitungen, wie man SEO am besten einsetzt, um den organischen Traffic zu erhöhen und somit mehr Nutzer auf die eigene Seite lockt. Von Broken-Link-Building ist da die Rede und von Studien, die behaupten, das 70 % des SEO Traffics durch Longtail-Keywörter generiert werden.\n'
                + '\n'
                + 'Diese Strategien haben durchaus ihre Daseinsberechtigung, keine Frage. Aber es ist nun mal so, dass der Algorithmus der Suchmaschinen, allen voran Google, recht undurchsichtig ist und sich sowieso ständig ändert. Und nicht umsonst gibt es die Berufsgruppe der SEO-Manager, die darauf spezialisiert sind, die Suchmaschinenoptimierung von Webseiten zu perfektionieren. So wie Sie, als Hotelier oder Gastronom, darauf spezialisiert sind, Ihren Gästen das bestmögliche Erlebnis in Ihrem Hause zu bieten.\n'
                + '\n'
                + 'Sparen Sie es sich also, sich in den undurchdringlichen Dschungel der Suchmaschinenoptimierung zu stürzen, Zeit und Nutzen stehen hierbei in keinem Verhältnis – man munkelt bereits, dass das Metier der SEO-Manager eine vom Aussterben bedrohte Rasse ist. Nutzen Sie stattdessen schlicht und einfach die Ressourcen, die Ihnen zur Verfügung stehen, und setzen Sie diese effizient ein.\n'
                + '\n'
                + '**SEO Traffic steigern mit Online Bewertungen**\n'
                + 'Die Bewertungen Ihrer zufriedenen Gäste, die Sie auf öffentlichen Portalen wie Facebook, TripAdvisor, Google oder HolidayCheck erhalten, sind Content, der Ihr Unternehmen für die Suchmaschinen interessant macht. Jede neuer Bewertung ist frischer Content – Suchmaschinen lieben frischen Content. Gleichzeitig liefern Bewertungen einen großen Mehrwert für andere Nutzer, und werden von den Suchmaschinen dementsprechend als besonders relevant eingestuft.\n'
                + '\n'
                + 'Sie gewinnen mit hochwertigem Content also nicht nur die Suchmaschinen für sich, sondern vor allem auch die Nutzer, denen Sie wertvolle und nützliche Informationen liefern.\n'
                + '\n'
                + 'Am meisten profitieren Sie von dieser Tatsache, wenn Sie sich nicht nur auf die Rezensionen der großen Bewertungsportale verlassen, sondern gezielt Ihre Gäste um Bewertungen bitten, diese sammeln und auf Ihrere eigenen Webseite einbinden. Dies gelingt mit Hilfe einer XML Integration. Die Einbindung Ihrer Gästebewertungen via XML hat einen positiven Effekt auf die Auffindbarkeit Ihrer Seite im Internet, denn Suchmaschinen bewerten Webseiten, deren Inhalte regelmäßig aktualisiert und ergänzt werden, besonders gut. Dadurch, dass die Bewertungen in Ihrer eigenen Webseite integriert werden, und Ihre Gäste immer neue Bewertungen schreiben, aktualisieren Sie, ohne selbst zeit oder Geld investieren zu müssen, ständig Ihren Webseiten Inhalt. Und Google wiederum belohnt wechselnde Inhalte mit einem besseren Ranking.\n'
                + '\n'
                + 'Für unsere Kunden stellen wir alle benötigten Daten zur Einbindung des XML Feeds – selbstverständlich in einem individuellen, von Ihnen gestalteten Design – zur Verfügung. Entscheiden Sie selbst, welche Bewertungen, Rankings und Statistiken Sie darstellen lassen möchten. Kontaktieren Sie uns einfach und nutzen Sie Ihre Bewertungen, um Ihren SEO Traffic zu steigern. Nachdem der XML-Feed eingebunden ist, geben Sie unserem Kundenservice bitte unbedingt kurz Bescheid, damit wir das Zertifikat auf “no-index” setzen können. Somit werden die Bewertungen von den Crawlern der Suchmaschinen nur noch auf Ihrer Webseite gefunden.\n'
                + '\n'
                + '**Übrigens…**\n'
                + 'Bei “noindex” handelt es sich um einen Befehl, der in den Meta-Informationen einer Webseite untergebracht wird. Dieser Befehl verhindert, dass die jeweilige Webseite in den Index der Suchmaschinen aufgenommen wird.\n'
                + '\n'
                + 'Nun könnte man annehmen, dass es doch eher von Vorteil ist, die eigenen Gästebewertungen auf so vielen verschiedenen Seiten wie möglich veröffentlicht zu haben. Jein. Natürlich hat Ihre Präsenz auf den öffentlichen Plattformen und das breite Streuen Ihrer Rezensionen einen positiven Effekt auf Ihre Online Reputation. Tauchen nun aber die selben Bewertungen auf verschiedenen Webseiten auf, ist das “Duplicate Content”, also doppelt vorhandener Inhalt. Und solchen gilt es zu vermeiden, denn es ist mehr als kontraproduktiv, wenn der gleiche Content auf mehreren unterschiedlichen Webseiten dargestellt wird. Denn die Suchmaschinen filtern Duplicate Content gerne heraus und berücksichtigen ihn nicht bei der Indexierung – oder, noch schlimmer, bewerten ihn, und somit Ihre Webseite, negativ.\n'
                + '\n'
                + 'Suchmaschinen wie Google gehen davon aus, dass Sie bewusst doppelten Content gestreut haben, um Ihr Ranking positiv zu beeinflussen und mehr Zugriffe zu erhalten und strafen dieses Vorgehen rigoros ab. Die Folge: Statt Ihr Ranking, wie erhofft, zu steigern, werden sie in den Suchergebnissen weit weniger berücksichtigt als zuvor. Ihre potentiellen Kunden können Sie über die organische Suche nur noch bedingt finden, Ihre Seitenzugriffe nehmen ab – und schließlich auch Ihr Umsatz.\n'
                + '\n'
                + '**Fazit**\n'
                + 'Es ist also unabdingbar und – aus Unternehmenssicht – lebenswichtig, den SEO-relevanten Content, also Ihre Gästebewertungen, gezielt zu veröffentlichen und den Suchmaschinen leicht zugänglich zu machen. So steigern Sie nicht nur den SEO Traffic Ihrer Webseite, sondern investieren in Ihren Umsatz und somit in Ihre eigene Zukunft.\n'
                + '\n'
                + 'Gerne helfen wir Ihnen dabei, die Bewertungen Ihrer Gäste gezielt zur Steigerung Ihres SEO Traffics einzusetzen. Vereinbaren Sie einen Termin mit einem unserer Mitarbeiter, nutzen Sie unsere Lösung kostenlos und unverbindlich für 30 Tage und überzeugen Sie sich selbst!\n',
    },
    {
      path: 'src/components/GeneralViews/Start/Blog/wie_verwandele_ich_online_bewertungen_in_umsatz.md',
      title: 'Wie verwandele ich Online Bewertungen in Umsatz',
      previewText: 'Online Bewertungen können dir helfen, neue Gäste zu gewinnen und somit deinen Umsatz zu erhöhen. Heutzutage checken ...',
      order: 8,
      previewImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/ar_16:9,c_fill,e_sharpen,c_thumb,e_colorize:15,q_auto:eco,w_600/v1541167832/respondo/blog_image_two.jpg',
      imageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_thumb,e_colorize:39,q_auto:good,w_1080/v1541167832/respondo/blog_image_two.jpg',
      headerImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_fill,g_auto,h_315,w_1000/v1548337660/respondo/blog_image_two.jpg',
      markdown: '# Wie verwandele ich Online Bewertungen in Umsatz?\n'
                + '\n'
                + 'Online Bewertungen können dir helfen, neue Gäste zu gewinnen und somit deinen Umsatz zu erhöhen. Heutzutage checken die meisten Menschen Restaurants auf Plattformen wie TripAdvisor, Facebook oder Holidaycheck. Dort können Gäste, die das favorisierte Restaurant schon besucht haben, schreiben, was sie gut oder auch schlecht fanden, Bilder hochladen und somit potentielle Gäste beeinflussen, ob sie dein Restaurant in Zukunft besuchen wollen oder nicht. Es ist also dein Ziel, möglichst viele, gute Bewerbungen zu erreichen. Wie kannst du das schaffen? Ganz einfach, indem du deine Gäste bittest, nach ihrem Besuch eine Bewertung über dich zu verfassen. Dies ist einer der einfachsten Wege, um möglichst viele gute Rezensionen zu sammeln. Außerdem ist es wichtig, die Kontrolle über deine Online Reputation zu behalten. Überprüfe regelmäßig, was deine Gäste über dein Restaurant schreiben und analysiere die Rezensionen.\n'
                + '\n'
                + 'Natürlich kann ein Gast über dein Restaurant auch eine eher negative Rezension schreiben. Aber wenn du richtig damit umgehst, kann es dir weiterhelfen. Die Nutzer von Bewertungsplattformen denken, dass eine empathische Antwort auf eine negative Kritik einen Mehrwert für dein Restaurant ist. Wenn du auf Rezensionen reagierst, egal ob negativ oder positiv, wirkt das nahbar und sympathisch auf deine Kunden und sie sind eher bereit, dein Restaurant zu besuchen. Eine der wichtigsten Eigenschaften, die einen guten Service ausmachen, ist das Reagieren bei Problemen und Kritik. Mit Verständnis und Lösungsvorschlägen gibst du dem Kunden das Gefühl, wichtig zu sein und dass es dir am Herzen liegt, das Problem so schnell wie möglich zu lösen.\n'
                + '\n'
                + 'Es ist also gar nicht so schwierig, aus Online Reputationen den Umsatz zu erhöhen. Google dich am besten regelmäßig selbst und checke deine Rezensionen. Bitte deine Gäste dir Rezensionen zu schreiben und reagiere empathisch auf Kritik. Das Internet hilft dir, auf dein Restaurant aufmerksam zu machen und so Kunden zu gewinnen, die möglicherweise sonst nie auf dein Restaurant aufmerksam geworden wären!\n',
    },
    {
      path: 'src/components/GeneralViews/Start/Blog/wie_ueberzeuge_ich_gaeste_mich_online_positiv_zu_bewerten.md',
      title: 'Wie überzeuge ich Gäste mich online positiv zu bewerten',
      previewText: 'Online Bewertungen gibt es heutzutage in jeder Branche. Vom Friseursalon bis hin zur Touristinformation können Kunden ihre (Un-) Zufriedenheit ...',
      order: 9,
      previewImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/ar_16:9,c_fill,e_sharpen,c_thumb,e_colorize:15,q_auto:eco,w_600/v1541167832/respondo/dan-gold-105699-unsplash.jpg',
      imageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_thumb,e_colorize:39,q_auto:good,w_1080/v1541167832/respondo/dan-gold-105699-unsplash.jpg',
      headerImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_fill,g_auto,h_315,w_1000/v1548337660/respondo/dan-gold-105699-unsplash.jpg',
      markdown: '# Wie überzeuge ich Gäste mich online positiv zu bewerten?\n'
                + '\n'
                + 'Online Bewertungen gibt es heutzutage in jeder Branche. Vom Friseursalon bis hin zur Touristinformation können Kunden ihre (Un-) Zufriedenheit ausdrücken und potentielle Kunden nachlesen, ob dieses Geschäft ihnen wirklich zusagt oder sie sich für ein anderes entscheiden. Danach können sie selbst bewerten und mit ihren eigenen Erfahrungen andere Gäste beeinflussen. Gerade in der Gastronomie sind Rezensionen im Internet besonders präsent und wichtig.\n'
                + '\n'
                + 'Für dich als Gastronom ist das nützlich, da du beobachten kannst, was auf Plattformen wie TripAdvisor oder Facebook über dein Restaurant geschrieben wird. Aus diesen Bewertungen kannst du lernen, dein Restaurant verbessern und so attraktiver für neue Kunden werden.\n'
                + '\n'
                + 'Natürlich kommen nicht alle Gäste auf die Idee, direkt nach ihrem Besuch dir eine Bewertung zu hinterlassen. Keine Zeit, vergessen oder sogar noch nie eine Bewertung vorher verfasst? Dem kannst du ganz einfach entgegenwirken, indem du deine Gäste bittest, dir eine Rezension zu hinterlassen. So steigst du im Ranking, da du mehr und hoffentlich positive Bewertungen bekommst. Außerdem kannst du so aus dem Feedback deiner Gäste wesentlich effizienter analysieren und dich verbessern.\n'
                + '\n'
                + 'Am einfachsten ist es, wenn du eines der folgenden Tricks verwendest, um deinen Gästen das Bewerten so einfach wie möglich zu machen. Wenn es zu kompliziert wird, haben die Kunden wenig Lust, noch eine ausführliche Bewertung abzugeben.\n'
                + '\n'
                + '-   **Individualisierte Fragebögen**\n'
                + 'Erstelle einen simplen Fragebogen, der den Gästen hilft, sich an der Bewertung entlang zu hangeln. Frage zum Beispiel, wie sie den Service fanden, die Wartezeiten etc. Gerade wenn du explizit etwas verbessern oder verändern möchtest, hilft dir diese Methode, um herauszufinden, was deine Gäste wirklich wollen.\n'
                + '\n'
                + '-   **Via E-Mail zum Bewerten einladen**\n'
                + 'Wenn Gäste ihre Reservierungen via E-Mail machen, kannst du es ausnutzen, ihre E-Mailadresse zu haben und sie direkt nach ihrem Besuch freundlich auffordern, dir eine Bewertung zu hinterlassen.\n'
                + '\n'
                + '-   **Bewertungseinladung per SMS**\n'
                + 'Du hast zwar nicht die E-Mail-Adresse, aber sie haben über ihre Mobilfunknummer reserviert? Dann schicke ihnen doch per SMS oder WhatsApp die Bitte, dich zu bewerten!\n'
                + '\n'
                + '-   **WLAN einrichten**\n'
                + 'Wenn du WLAN in deinem Restaurant hast, können deine Gäste direkt vor Ort bewerten und vergessen es nicht, auf dem Weg nach Hause! Dafür benötigen deine Gäste nur ihr Smartphone.\n'
                + '\n'
                + '-   **Bewerten vor Ort per Tablet**\n'
                + 'Haben deine Gäste kein Smartphone dabei? Dann könntest du ihnen ein Tablet anbieten, um ihre Bewertung noch während ihres Aufenthaltes zu schreiben. Du kannst somit auch ein persönliches Gespräch mit deinen Gästen führen, in dem mögliche Kritikpunkte angesprochen und geklärt werden können.\n'
                + '\n'
                + '- **Bewerten via QR Code**\n'
                + 'Wenn weder E-Mail, noch Smartphone oder WLAN verfügbar sind, kannst du deine Gäste mithilfe eines QR Codes zum Bewertungsformular führen! Du kannst den Code ganz einfach auf deine Karte drucken, einen kleinen Aufsteller auf den Tisch stellen oder auf die Rechnung drucken. Deine Gäste scannen den Code und landen direkt bei der Bewertung.\n'
                + '\n'
                + '**Fazit**\n'
                + 'Wie du siehst, gibt es zahlreiche Möglichkeiten, um deine Gäste die Möglichkeit zu bieten, dich zu bewerten. Es ist deine Entscheidung, welche Methode für dich und dein Restaurant am sinnvollsten ist und womit du die besten Bewertungen erzielen kannst.\n',
    },
    {
      path: 'src/components/GeneralViews/LearnMore/Blog/respondo_premium.md',
      title: 'Professionelles Bewertungsmanagement - Ohne Aufwand zu 5 Sternen',
      previewText: 'Suchmaschinen-Ranking, Feedback, 5 Sterne, Daumen hoch, Traffic - All diese Begriffe haben ein gemeinsames Ziel. Primäres Ziel ist natürlich immer die Umsatzsteigerung. Und das gelingt mit einem Traffic-Zulauf. Sekundäres Ziel ist das Image und die Onlinepräsenz...',
      order: -1,
      previewImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/ar_16:9,c_fill,e_sharpen,c_thumb,e_colorize:15,q_auto:eco,w_600/v1541167832/respondo/blogpost_respondo_premium.jpg',
      imageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_thumb,e_colorize:39,q_auto:good,w_1080/v1541167832/respondo/blogpost_respondo_premium.jpg',
      headerImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_fill,g_center,h_315,w_1000/v1548337660/respondo/blogpost_respondo_premium.jpg',
      markdown: ' # Professionelles Bewertungsmanagement - Ohne Aufwand zu 5 Sternen\n'
        + '  \n'
        + 'Suchmaschinen-Ranking, Feedback, 5 Sterne, Daumen hoch, Traffic - All diese Begriffe haben ein gemeinsames Ziel. Primäres Ziel ist natürlich immer die Umsatzsteigerung. Und das gelingt mit einem Traffic-Zulauf. Sekundäres Ziel ist das Image und die Onlinepräsenz. Neben verschiedene Social-Media-Kanälen ist professionelles Bewertungsmanagement ein wichtiges Thema um Vertrauen zu den Gästen aufzubauen und somit mehr Neukunden zu erreichen und Stammkunden zu halten. Wir bieten diesen Service mit Respondo Premium an. Unser Team übernimmt für dich die ausführliche Pflege der Onlinereputation und du sparst eine Menge Zeit, die du in andere Maßnahmen investieren kannst. Im folgenden erfährst du warum Respondo Premium für euch eine willkommene Option ist.\n'
        + '​\n'
        + '**Kundenmeinungen sind der Weg nach Oben im Suchmaschinen-Ranking**\n'
        + 'User generated Content hilft dir präsent zu sein und verbessert dein Ranking in Suchmaschinen. So wird deine Kernzielgruppe optimal erreicht und motiviert andere ebenfalls Feedback abzugeben. Nach jahrelanger Erfahrung im Bereich Social Media wissen wir genau wo wir andocken müssen um diese Schnittstellen zu nutzen.\n'
        + '​\n'
        + '**Bewertungen gewinnen immer mehr an Wert**\n'
        + 'Nach unseren Erfahrungen werden Bewertungen immer wichtiger, denn heutzutage erwartet die Mehrheit gehört zu werden und eine persönliche Antwort zu bekommen. Wir setzen uns mit schlechter Kritik auseinander und wirken dem entgegen. Gute Kritik würdigen wir mit einer netten Antwort. Auf beiden Wegen baut man eine persönliche Beziehung zu den Kunden auf und bindet sie an den Betrieb. \n'
        + '​\n'
        + '**Hinter jeder schlechten Bewertung, steckt ein neuer Kunde**\n'
        + 'Einige sind der Meinung, auf schlechte Bewertungen müssen sie nicht antworten. Dabei sind es genau die Bewertungen, die einer besonders gescheiten Antwort bedürfen. Eine schlechte Bewertung heißt, dass der Kunde enttäuscht war, weil seine Erwartungen nicht erfüllt wurden. Liest das ein weiterer Kunde der sich über den Betrieb informieren will, ist er sofort abgeneigt. Wir können geschickte Antworten formulieren, so dass jeder Kunde weiß, dass wir uns seine Meinung zu Herzen nehmen. So freut sich der Kunde, fühlt sich berücksichtigt und gibt dem Betrieb eine weitere Chance. Außerdem weckt man sympathie bei denen, die sich die Bewertungen und die geschickte formulierte Antwort sehen. Mit dem Antworten punktet man also nicht nur bei dem Bewertenden sondern auch bei allen weiteren Webbesuchern.\n'
        + '\n'
        + '**Andorra-Effekt und Greenspoon-Effekt**\n'
        + 'Im Grunde gibt es einen typischen Ablauf für eine Entscheidung. Der Betrieb stellt ein attraktives Angebot, woraufhin der Kunde sich informiert. Das passiert in den meisten Fällen online auf Bewertungsportalen (wie z.B. [Google](https://www.google.de/maps?hl=de&tab=wl), [Facebook](https://www.facebook.com/) und [TripAdvisor](https://www.tripadvisor.de/)). Die Kunden möchten sich auf diese Weise ein klares Bild machen. \n'
        + '​\n'
        + 'Wir streben zwei Phänomene an, um das bestmöglichste auszuschöpfen. Das erste ist der Andorra-Effekt. Hier passen sich die Kunden den Bewertungen an und es entsteht der bekannte Herdentrieb. \n'
        + '​\n'
        + 'Als zweites möchten wir den Greenspoon-Effekt auslösen. Das geschieht wie folgt: Man erhält eine Bewertung und wir bestätigen diese in einer Antwort. Der Kunde freut sich gehört, sowie bestätigt zu werden. Ein weiterer Webbesucher, der die Kommunikation sieht wird somit positiv beeinflusst in sein Entscheidungs-und Kaufverhalten. \n'
        + '​\n'
        + 'Um das erfolgreich umzusetzen ist professionelles Bewertungsmanagement ein wichtiger Bestandteil, um die eigene Reputation im Internet auf ein solides Fundament zu stellen.\n'
        + '\n'
        + '**Professionelles Bewertungsmanagement mit Respondo Premium**\n'
        + 'Um Bewertungen optimal zu managen müssen mind. drei Themenbereiche realisiert und mit Leben erfüllt werden.\n'
        + '1. Wahrzunehmendes Wunschbild aus Kundensicht definieren \n'
        + '2. Bewertungskanäle schaffen\n'
        + '3. Eine Task-Force einrichten\n'
        + '\n'
        + 'Das fällt mit Respondo Premium in unseren Aufgabenbereich und du sparst dir den Aufwand. Natürlich gibt es noch andere wichtige Maßnahmen außerhalb der Bewertungen die Zeit und Energie beanspruchen. Wir von Respondo haben Erfahrung im Umgang mit professionellen Bewertungsmanagement und widmen uns den Aufgaben täglich und ausführlich. Überlasse das managen deiner Bewertungen uns. So besitzt auch dein Lokal ein stabiles Fundament und Vertrauen basierend auf der Kommunikation via Reputation. Der Aufwand für diesen Bereich sinkt für dich auf null und du hast mehr Zeit, die du in andere Maßnahmen investieren kannst. Wir fokussieren uns auf dein Bewertungsmanagement und bearbeiten alles vollständig und täglich.\n'
        + '\n'
        + '**So können wir als Online-Reputationsagentur helfen**\n'
        + 'Vier gute Gründe, warum du auf uns als Online-Reputationsagentur setzen solltest.\n'
        + '1. Wir definieren und setzen gewisse Standards, um kontinuierlich und systematisch Kundenbewertungen zu generieren.\n'
        + '2. Wir installieren die einzelnen Maßnahmen an den Schnittstellen zwischen Unternehmen und Kunden so, dass diese Wirkung zeigen.\n'
        + '3. Sollte eine unangemessene oder kompromittierende Bewertung oder ein Erlebnisbericht eingehen, wissen wir, wie reagiert werden muss.\n'
        + '4. Wir nehmen dir den kompletten Aufwand ab und du sparst viel Zeit.\n'
        + '\n'
        + 'Wir sorgen für mehr Neukundenzulauf und Beziehungspflege der Stammkunden auf der Seite der professionellen Pflege der Bewertungen. Du hast keinen Aufwand mehr und investiert mehr Zeit in weitere wichtige Maßnahmen. \n'
        + '​\n'
        + 'Überlasse deine Online Reputation nicht dem Zufall! Du willst mehr über Respondo Premium erfahren? [Kontaktiere uns!](mailto:francesca@thefoodguide.de)\n'
        + '​\n',
    },
    {
      path: 'src/components/GeneralViews/LearnMore/Blog/respondo_premium.md',
      title: 'Social Media Trends 2020',
      previewText: 'Wir posten ein Bild auf Instagram, geben einen Daumen hoch/runter auf Facebook und veröffentlichen ein neues Video auf TikTok. Jeder nutzt Social-Media im Alltag und deswegen ist es heutzutage elementar für den Bereich Marketing und PR ...',
      order: -5,
      previewImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/ar_16:9,c_fill,e_sharpen,c_thumb,e_colorize:15,q_auto:eco,w_600/v1578325542/respondo/tyler-nix-FbP3btqc4UI-unsplash.jpg',
      imageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_thumb,e_colorize:39,q_auto:good,w_1080/v1578325542/respondo/tyler-nix-FbP3btqc4UI-unsplash.jpg',
      headerImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_fill,g_center,h_315,w_1000/v1578325542/respondo/tyler-nix-FbP3btqc4UI-unsplash.jpg',
      markdown: '# Social Media Trends 2020 \n'
        + '\n'
        + 'Wir posten ein Bild auf Instagram, geben einen Daumen hoch/runter auf Facebook und veröffentlichen ein neues Video auf TikTok. Jeder nutzt Social-Media im Alltag und deswegen ist es heutzutage elementar für den Bereich Marketing und PR. Im Durchschnitt nutzen wir Social-Media mehr als 3 Stunden pro Tag. Somit handelt es sich um ein optimales Medium um präsent zu sein oder die eigenen Produkte/Dienstleistungen zu präsentieren. Der Trend hat sich stets entwickelt und wurde in den letzten Jahren immer innovativer. Um als Gastronom und Hotelier die Beliebtheit des Lokals zu steigern, ist es förderlich sich mit dem Trend zu entwickeln. Denn auch die Kundschaft und deren Interesse entwickelt sich und das Ziel ist es die Kundschaft zu halten und im besten Falle mehr Kunden zu gewinnen. Das tut man, indem das Interesse weiterhin weckt und zu den innovativen Kommunikationsmethoden greift. Wohin entwickelt sich der Trend also im Jahr 2020?\n'
        + '\n'
        + '**Rückblick 2019**\n'
        + 'Bevor wir uns mit dem nächsten Jahr beschäftigen, betrachten wir das vergangene Jahr 2019. In diesem Jahr waren Stories bei WhatsApp, Instagram und Facebook sehr beliebt. Laut den offiziellen Nutzerzahlen von Instagram, sind 700 Millionen Menschen auf der Welt auf diesem Social-Media-Kanal aktiv und 250 Millionen Menschen weltweit nutzen begeistert die Instagram-Stories. Auch “live content” ist eine Art die gerne genutzt wurde, um die Beliebtheit zu steigern. Genauso beliebt unter den Kunden sind Nischenplattformen wie zum Beispiel TikTok. Hierbei handelt es sich um einen immer noch wachsenden Trend. Dazu erfahrt ihr im Folgenden mehr.\n'
        + '\n'
        + '**Das neue Jahr 2020**\n'
        + 'Für das Jahr 2020 werden ephemere Inhalte weiterhin an Popularität gewinnen und ein nützliches Mittel sein, um das Interesse bei Kunden zu wecken und gute Kundenbeziehungen aufzubauen. Ephemere Inhalte sind kurzlebige Momente die veröffentlicht werden, wie zum Beispiel Stories. Die direkte Kommunikation wird weiterhin wichtig und gefragt sein. Außerdem wird der Bereich Social-Commerce ein nützliches Medium sein, um seine Conversions zu verbessern, denn so ist das Produkt direkt dort käuflich zu erwerben, wo sich der Konsument aufhält. Instagram macht es mit Shoppable Ads bereits vor, indem man über markierte Produkte im Beitrag direkt zum Webshop weitergeleitet wird. Gewinnspiele, Verlosungen oder Events sind ebenfalls Methoden um seine Leistungen präsent anzubieten. Die Foodguide Instagram-Accounts bieten diese Leistungen bereits mit Erfolg europaweit an. So hat man die Möglichkeit eine Interaktion mit den Kunden aufzubauen und steuert wieder das Interesse an. Außerdem wird so die Kommunikation innerhalb der Gäste geweckt. Durch Gewinnspiele zum Beispiel werden sie animiert die entsprechenden Beiträge oder Mitteilungen auf verschiedene Art und Weise zu teilen.\n'
        + '\n'
        + 'Außerdem werden Nischenplattformen in Zukunft immer beliebter. Viele Menschen nutzen Sozialplattform wie die Foodguide App, Tik Tok und Untappd. Über die Foodguide App können Nutzer ihre Erfahrungen in Restaurants mit einem Foto dokumentieren und online stellen. Somit können alle anderen Nutzer das Bild nach dem Tinder-Prinzip, nach links oder nach rechts swipen. Untappd ist eine App mit der man überall auf der Welt Biere, Brauereien und Veranstaltungsorte finden kann. Bei TikTok handelt es sich um eine schnell viral gehende Plattform, die als globale Video-Community bezeichnet wird. Man kann kurze und unterhaltsame Videos entdecken oder selber aufnehmen mit ausgewählter Musik. Momentan zählt TikTok zu einer der beliebtesten Apps der Welt. Mit mehr als 33 Millionen Installationen im Jahre 2019 ist die App momentan beliebter als Facebook, Instagram und YouTube. Sobald ein Gastronom in der App TikTok Werbung platziert, können das also eine Menge Menschen sehen.\n'
        + '\n'
        + 'Die Nutzerzahlen für solche Plattformen steigen immer noch an. Mit der wachsenden Reichweite ist durchaus absehbar, dass diese Plattformen aus dem Nischen-Konzept hinauswachsen und als große Plattform angesehen werden.\n'
        + '\n'
        + '**Los geht’s**\n'
        + 'Wer sich um ephemere Inhalte, Social-Commerce und Werbeplatzierung in Nischenplattformen bemüht, ist gut ausgestattet. Allerdings ist der Alltag eines Gastronomen/ Hoteliers bereits zeitintensiv, sowie auch für seine Mitarbeiter. Daher sollte man sich auf das Mindeste der Trends fokussieren. Optimal für Gastronomen ist die Werbeplatzierung in Stories. Das ist auch über die Foodguide Instagram-Accounts möglich. Denn die heutigen Kunden sind immer noch höchst aktiv über Instagram und lassen sich gerne inspirieren. Außerdem gibt man die Arbeit für das Erstellen der Stories ab und hat nur den Zeitaufwand in der Absprache, was nur einen Bruchteil des Aufwandes entspricht. Um auch die Nischenplatformen abzudecken, passt die Foodguide-App optimal zu Gastronomen, da eine Gruppe von Menschen angesprochen wird die sich speziell mit Restaurants beschäftigt und bestimmte Lokale entdecken wollen. Auch hier ist kaum Zeitaufwand vorhanden, da das Teilen an sich die Kunden übernehmen. 2020 wird der Bereich Social-Media mit Sicherheit intensiver ausgebaut und noch mehr Bestandteil im alltäglichen sein bei allen Konsumenten.\n'
        + '\n'
        + 'Somit wünscht das Respondo-Team ein frohes neues Jahr und viel Erfolg!',
    },
  ],
  entriesEn: [
    {
      path: 'src/components/GeneralViews/Start/Blog/wie_verwandele_ich_online_bewertungen_in_umsatz.md',
      title: 'How to transform Reviews into Sale',
      previewText: 'Today\'s communication exists in the world wide web. Actually online-reviews for restaurants or hotels are very important and ...',
      order: 0,
      previewImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/ar_16:9,c_fill,e_sharpen,c_thumb,e_colorize:15,q_auto:eco,w_600/v1541167832/respondo/blog_image_two.jpg',
      imageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_thumb,e_colorize:39,q_auto:good,w_1080/v1541167832/respondo/blog_image_two.jpg',
      headerImageUrl: 'https://res.cloudinary.com/foodiscovr/image/upload/c_fill,g_auto,h_315,w_1000/v1548337660/respondo/blog_image_two.jpg',
      markdown: '# How to transform Reviews into Sales\n'
        + '\n'
        + 'Today\'s communication exists in the world wide web. Actually online-reviews for restaurants or hotels are very important and numerous for example at [Facebook](https://www.facebook.com), [Google](https://www.google.de/maps?hl=de&tab=wl) or [Yelp](https://www.yelp.com). With reviews you can raise your traffic and sales. There customer can comment for their mostly liked or mostly not liked locations and upload pictures to influence all those who see that. People choose a location based on online-reviews. So the goal is to get good reviews as much as possible. How can you reach this? Easy going. You have to ask your customer for online feedback after the visit. In this way you can get many reviews easy and fast. Moreover its very important to have a good overview on your reviews and regularly check out what your customers comment.\n'
        + '\n'
        + 'It’s possible that you can get negative ratings from customers. But if you take the right action to handle this it can turn to a positive effect. The most of the users from rating platforms think that an empathic answer on the negative comment has more added value. For customers it\'s approachable and sympathetic to answer on reviews no matter if it\'s positive or negative. So they are motivated to visit your location. One of the most important properties for good service is to handle problems and criticisms with appreciation and solutions. The customer should feel important and know that solving their problems is very ambitious for you.\n'
        + '\n'
        + 'Ay you can see, it’s not too difficult to transform online reviews into more sales. Check your location on Google regularly. Ask your customers for giving you online ratings and be empathic on negative comments. With help from the Internet your location can call attention and get more customers.\n'
        + '\n',
    },
  ],
}

const getters = {
  getSortedBlogs(state) {
    let entries = []
    switch (TranslationHelper.currentLocale()) {
      case 'de': entries = state.entriesDe
        break
      case 'en': entries = state.entriesEn
        break
      default:
        entries = state.entriesEn
    }
    return entries.sort((a, b) => a.order - b.order)
  },
}
const mutations = {}
const actions = {}

export default {
  state,
  getters,
  mutations,
  actions,
}

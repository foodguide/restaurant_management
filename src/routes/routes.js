import SplashScreen from '../components/UIComponents/SplashScreen.vue'
import DashboardLayout from '../components/Dashboard/Layout/DashboardLayout.vue'
// GeneralViews
import NotFound from '../components/GeneralViews/NotFoundPage.vue'

// Main pages
const Settings = () => import('../components/GeneralViews/Settings.vue')
const Reviews = () => import('../components/Dashboard/Views/Reviews/Review.vue')
const Imprint = () => import('../components/GeneralViews/Imprint.vue')
const Location = () => import('../components/Dashboard/Views/Location/Location.vue')
const Connector = () => import('../components/Dashboard/Views/Representation/Connector.vue')
const Login = () => import('../components/GeneralViews/Login.vue')
const Registration = () => import('../components/GeneralViews/Registration.vue')
const AuthLayout = () => import('../components/GeneralViews/AuthContainer.vue')
const Onboarding = () => import('../components/GeneralViews/Onboarding/Onboarding.vue')
const Blogpost = () => import('../components/GeneralViews/Blogpost.vue')
const TermsOfService = () => import('../components/GeneralViews/TermsOfService.vue')
const Landing = () => import('../components/GeneralViews/LandingPage/LandingPage.vue')
const LearnMore = () => import('../components/GeneralViews/LearnMore/LearnMore.vue')
const Statistics = () => import('../components/Dashboard/Views/Statistics/Statistics.vue')
const Payment = () => import('../components/GeneralViews/Payment.vue')
const QuickReply = () => import('../components/GeneralViews/QuickReply/QuickReply.vue')
const NotificationSubscriptions = () => import('../components/GeneralViews/NotificationSubscriptions.vue')
const ApplicationError = () => import('../components/GeneralViews/ApplicationError.vue')
const Welcome = () => import('../components/GeneralViews/Welcome.vue')

export const createRoutes = (Settings, Reviews, Imprint,
  Location, Connector, Login,
  Registration, AuthLayout, Onboarding, Landing, Blogpost, TermsOfService,
  LearnMore, Statistics, Payment, QuickReply, NotificationSubscriptions, ApplicationError, Welcome) => [
  // GENERAL paths
  {
    path: '/',
    redirect: '/start',
    component: SplashScreen,
  },
  {
    path: '/blog/:blogOrderIndex',
    component: Blogpost,
    meta: {
      isApplication: false,
    },
  },
  {
    path: '/start',
    component: Landing,
    meta: {
      isApplication: false,
    },
  },
  {
    path: '/more',
    component: LearnMore,
    meta: {
      isApplication: false,
    },
  },
  {
    path: '/unsubscribe/:id',
    component: NotificationSubscriptions,
    meta: {
      isApplication: false,
    },
  },
  {
    path: '/verified/:id',
    component: NotificationSubscriptions,
    meta: {
      isApplication: false,
    },
  },
  {
    path: '/imprint',
    component: Imprint,
    meta: {
      isApplication: false,
    },
  },
  {
    path: '/terms-of-service',
    component: TermsOfService,
    meta: {
      isApplication: false,
    },
  },
  // APP paths
  {
    path: '/auth',
    component: AuthLayout,
    redirect: '/auth/login',
    meta: {
      isApplication: true,
    },
    children: [
      {
        path: 'login',
        component: Login,
      },
      {
        path: 'register',
        component: Registration,
      },
    ],
  },
  {
    path: '/payment',
    redirect: '/test',
    meta: {
      isApplication: true,
    },
  },
  {
    path: '/test',
    redirect: '/test/respondo-starter',
    meta: {
      isApplication: true,
    },
  },
  {
    path: '/test/:planLabel',
    component: Payment,
    meta: {
      isApplication: true,
    },
  },
  {
    path: '/error',
    component: ApplicationError,
    meta: {
      isApplication: true,
    },
  },
  {
    path: '/registration-done',
    component: Welcome,
    meta: {
      isApplication: true,
    },
  },
  {
    path: '/app',
    component: DashboardLayout,
    redirect: '/app/locations',
    meta: {
      isApplication: true,
    },
    children: [
      {
        path: 'reviews/:id',
        component: Reviews,
      },
      {
        path: 'statistics/:id',
        component: Statistics,
      },
      {
        path: 'statistics',
        component: Statistics,
      },
      {
        path: 'settings',
        component: Settings,
      },
      {
        path: 'imprint',
        component: Imprint,
      },
      {
        path: 'terms-of-service',
        component: TermsOfService,
      },
      {
        path: 'connector/:id',
        component: Connector,
      },
      {
        path: 'connector',
        component: Connector,
      },
      {
        path: 'locations',
        component: Location,
      },
      {
        path: 'quick',
        component: QuickReply,
      },
    ],
  },
  { path: '*', component: NotFound },

]

export default createRoutes(Settings, Reviews, Imprint,
  Location, Connector, Login,
  Registration, AuthLayout, Onboarding, Landing, Blogpost, TermsOfService,
  LearnMore, Statistics, Payment, QuickReply, NotificationSubscriptions, ApplicationError, Welcome)

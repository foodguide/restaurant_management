import { mapGetters } from 'vuex'

export default {
  computed: {
    ...mapGetters([
      'getAllGoogleSearchResults',
    ]),
    searchResults() {
      const results = this.getAllGoogleSearchResults[this.location.name]
      return results || null
    },
  },
  methods: {
    startSearch(query) {
      const encodedQuery = encodeURIComponent(query)
      return this.$store.dispatch('requestGoogleSearchResults', encodedQuery)
    },
    getSearchResultsByPlatform(platform) {
      return this.getSearchResultsByPlatformName(platform.name)
    },
    getSearchResultsByPlatformName(name) {
      const results = this.searchResults
      if (!results) return null
      return results.filter(resultItem => this.isRelated(resultItem, name))
    },
    isRelated(resultItem, platformName) {
      return resultItem.displayLink.includes(platformName)
    },
  },
}

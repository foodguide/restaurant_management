import BaseRepository from './BaseRepository'
import FirebaseApp from '../../api/Firebase'

class AccountRepository extends BaseRepository {
  unlinkProvider(providerId) {
    return FirebaseApp.auth().currentUser.unlink(providerId)
  }

  unlinkProviders() {
    if (!FirebaseApp.auth().currentUser) return
    const providers = FirebaseApp.auth().currentUser.providerData
    providers.forEach((provider) => {
      if (provider.providerId === 'google.com') {
        this.unlinkProvider(provider.providerId)
      } else if (provider.providerId === 'facebook.com') {
        // Delete FB provider only if 'password' provider is present
        if (providers.findIndex(provider => provider.providerId === 'password') >= 0) {
          this.unlinkProvider(provider.providerId)
        }
      }
    })
  }
}

export { AccountRepository as default }

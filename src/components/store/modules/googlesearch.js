import Api from 'src/components/api/Api'
import Vue from 'vue'

const getters = {
  getAllGoogleSearchResults: state => state,
}
const mutations = {
  storeGoogleSearchResult(state, payload) {
    const { key } = payload
    Vue.set(state, key, payload.result)
  },
}

const actions = {
  requestGoogleSearchResults(context, payload) {
    if (context.state[payload]) {
      return Promise.resolve(context.state[payload])
    }
    return Api.google.search.search(decodeURIComponent(payload)).then((response) => {
      const { items } = response.data
      context.commit('storeGoogleSearchResult', {
        key: payload,
        result: items,
      })
      return items
    })
  },
}
export default {
  state: {},
  getters,
  mutations,
  actions,
}

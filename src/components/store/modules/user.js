import { event } from 'vue-analytics'
import moment from 'moment'
import uuidv1 from 'uuid/v1'
import Api from '../../api/Api'
import UserRepository, { AccessTokenStatus } from '../repositories/UserRepository'
import AccountRepository from '../repositories/AccountRepository'
import { HookId } from './hooks'
import { EventName, EventType } from './event'
import { RepresentationStatus } from '../repositories/LocationRepository'
import { capitalize } from '../../helper/StringHelper'
import TranslationHelper from '../../helper/TranslationHelper'

const storeSubscriptions = [
  { name: 'basic', type: 'email' },
  { name: 'summary', type: 'email' },
  { name: 'reminder', type: 'email' },
  { name: 'newsletter', type: 'email' },
]

const storeState = {
  currentUser: {
    isLoggedIn: false,
    isUnknown: true,
    nick: null,
    locations: [],
    id: null,
    credentials: [],
    subscriptions: [],
    providerData: [],
    uid: null,
    canModifyAccounts: false,
    locale: null,
    /**
     * We need this timestamp to store when check for unseen old reviews has been done the last time.
     */
    unseenOldReviewsTimestamp: null,
  },

  availableSubscriptions: storeSubscriptions,

  defaultSubscriptions: [
    storeSubscriptions.find(subscription => subscription.name === 'basic'),
  ],
}

const getCredentialsByPlatform = (state, platform) => {
  if (!state.currentUser.credentials) return null
  return state.currentUser.credentials.filter(tokenObject => tokenObject.platform === platform)
}

const fireLogoutResets = (context) => {
  context.commit('instagramStateReset')
  context.commit('locationStateReset')
  context.commit('reviewStateReset')
  context.commit('paymentStateReset')
  context.commit('facebookStateReset')
  context.dispatch('stopListeningForAllRepresentations')
  context.commit('clearTemporaryLocation')
  context.dispatch('resetFirebaseToken')
}

const getters = {
  notificationSubscriptions: state => state.availableSubscriptions.map((availableSubscription) => {
    const hasSubscribed = !!state.currentUser.subscriptions
      .find(subscribed => subscribed.name === availableSubscription.name && subscribed.type === availableSubscription.type)
    return { ...availableSubscription, hasSubscribed }
  }),
  availableSubscriptions: state => state.availableSubscriptions,
  isUnknown: state => state.currentUser.isUnknown,
  isLoggedIn: state => state.currentUser.isLoggedIn,
  nick: state => state.currentUser.nick,
  getFacebookCredentials: state => getCredentialsByPlatform(state, 'facebook'),
  getCurrentUserId: state => state.currentUser.id,
  getCredentials: state => state.currentUser.credentials,
  canModifyAccounts: state => state.currentUser.canModifyAccounts,
  hasOnlyFacebookProvider: state => state.currentUser.providerData
        && state.currentUser.providerData.length === 1
        && state.currentUser.providerData.findIndex(provider => provider.providerId === 'facebook.com') === 0,
  providers: state => state.currentUser.providerData,
  getCurrentUnhealthyGoogleCredential: (state) => {
    if (state.currentUser && state.currentUser.credentials) {
      const filtered = state.currentUser.credentials.filter(credential => !!credential.status
        && credential.status.startsWith('accesstoken_')
        && credential.platform === 'google')
      if (filtered.length > 0) return filtered[0]
    }
    return null
  },
}

const mutations = {
  logout(state) {
    // eslint-disable-next-line no-param-reassign
    state.currentUser = {
      isLoggedIn: false,
      isUnknown: false,
      nick: null,
      locations: [],
      id: null,
      credentials: [],
      subscriptions: [],
      uid: null,
      locale: null,
      /**
       * We need this timestamp to store when check for unseen old reviews has been done the last time.
       */
      unseenOldReviewsTimestamp: null,
    }
  },
  login(state, payload) {
    if (!payload) return
    // eslint-disable-next-line no-param-reassign
    if (payload.email) if (payload && payload.email) state.currentUser.nick = payload.email
    // eslint-disable-next-line no-param-reassign
    if (payload.id) state.currentUser.id = payload.id
    const eventLabel = payload.event ? payload.event : 'login'
    event('authentication', eventLabel)
  },
  onEmailChange(state, email) {
    // eslint-disable-next-line no-param-reassign
    if (email) state.currentUser.nick = email
  },
  setLoginStatus(state, payload) {
    // eslint-disable-next-line no-param-reassign
    state.currentUser.isUnknown = false
    // eslint-disable-next-line no-param-reassign
    state.currentUser.isLoggedIn = payload
  },
  onUserFetch(state, payload) {
    if (payload.user) {
      if (payload.user.credentials) {
        if (payload.merge) {
          // eslint-disable-next-line no-param-reassign
          state.currentUser.credentials = [...state.currentUser.credentials, ...payload.user.credentials]
        } else {
          // eslint-disable-next-line no-param-reassign
          state.currentUser.credentials = payload.user.credentials
        }
      }
      // eslint-disable-next-line no-param-reassign
      if (payload.user.id) state.currentUser.id = payload.user.id
      // eslint-disable-next-line no-param-reassign
      if (payload.user.unseenOldReviewsTimestamp) state.currentUser.unseenOldReviewsTimestamp = payload.user.unseenOldReviewsTimestamp
      // eslint-disable-next-line no-param-reassign
      if (payload.user.subscriptions) state.currentUser.subscriptions = payload.user.subscriptions
      // eslint-disable-next-line no-param-reassign
      if (payload.user.canModifyAccounts) state.currentUser.canModifyAccounts = payload.user.canModifyAccounts
    }
  },
  onUserProviderData(state, payload) {
    // eslint-disable-next-line no-param-reassign
    state.currentUser.providerData = payload
  },
  setNotificationSubscriptions(state, subscriptions) {
    if (!state.currentUser) return
    // eslint-disable-next-line no-param-reassign
    state.currentUser.subscriptions = subscriptions
  },
}

const userRepository = new UserRepository()
const accountRepository = new AccountRepository()

const getWrongUserMessage = platform => `Wrong ${capitalize(platform)} user.`

const actions = {
  register(context, payload) {
    return Api.user.register(payload.email, payload.password).then(() => {
      setTimeout(() => {
        // Delay because user might not be created yet
        context.dispatch('writeEvent', { name: EventName.Register, type: EventType.Action })
      }, 500)
      return userRepository.onAfterAuthenticated().then((user) => {
        context.commit('onUserFetch', { user })
        context.commit('login', {
          ...payload,
          event: 'registration',
        })
        context.commit('setEmptySubscriptions')
        context.commit('setLoginStatus', true)
        context.dispatch('subscribeToDefaultMessages')
      })
    })
  },
  login(context, payload) {
    return Api.user.login(payload.email, payload.password).then(() => {
      context.dispatch('writeEvent', { name: EventName.Login, type: EventType.Action, data: { mail: payload.email } })
      return userRepository.onAfterAuthenticated().then((user) => {
        context.commit('onUserFetch', { user })
        context.commit('login', payload)
        context.commit('setLoginStatus', true)
        if (!user.subscriptions) {
          context.dispatch('subscribeToDefaultMessages')
        }
        return user
      })
    })
  },
  loginFacebook(context) {
    return Api.user.loginWithFacebook().then((result) => {
      context.dispatch('writeEvent', {
        name: EventName.Login,
        type: EventType.Action,
        data: { provider: 'facebook' },
      })
      const credential = [{
        accessToken: result.credential.accessToken,
        platform: 'facebook',
      }]

      const payload = {
        id: result.user.uid,
        email: result.user.email ? result.user.email : null,
      }
      const isRegistration = result.additionalUserInfo.isNewUser
      if (isRegistration) {
        context.dispatch('showError', {
          message: 'error.noFacebook',
          isUserFriendly: true,
        })
        context.dispatch('deleteUser')
      } else {
        return userRepository.onAfterAuthenticated(credential).then((user) => {
          context.commit('onUserFetch', { user })
          context.commit('setLoginStatus', true)
          context.commit('login', payload)
        })
      }
    })
  },
  storeFacebookCredentials(context, payload) {
    return userRepository.addAccessToken(context.state.currentUser, payload.newCredential.accessToken, 'facebook', payload.oldCredential.id).then((responseData) => {
      context.commit('onUserFetch', { user: responseData, merge: true })
      return responseData
    }).catch((error) => {
      console.log(error)
    })
  },
  storeUserLocale(context, userId) {
    return userRepository.updateLocale(userId, TranslationHelper.currentLocale())
  },
  loginWithToken(context, token) {
    return Api.user.loginWithToken(token).then((result) => {
      context.dispatch('writeEvent', { name: EventName.Login, type: EventType.Action, data: { provider: 'token' } })
      const payload = {
        id: result.user.uid,
        email: result.user.email ? result.user.email : null,
      }
      if (result.additionalUserInfo.isNewUser) payload.event = 'registration'
      return userRepository.onAfterAuthenticated().then((user) => {
        context.commit('onUserFetch', { user })
        context.commit('setLoginStatus', true)
        context.commit('login', payload)
        return user
      })
    })
  },
  linkWithEmail(context, payload) {
    return Api.user.linkWithEmail(payload.email, payload.password).then((response) => {
      context.commit('onUserProviderData', response.user.providerData)
      context.commit('onEmailChange', payload.email)
    })
  },
  fetchCurrentUser(context) {
    return userRepository.getCurrentUser().then((user) => {
      if (!user) return null
      context.commit('onUserFetch', { user })
      return user
    })
  },
  requestGoogleCredential() {
    return Api.google.auth.signIn().then((authResponse) => {
      const auth = authResponse
      if (auth.access_token) {
        auth.accessToken = authResponse.access_token
        delete auth.access_token
      }
      auth.platform = 'google'
      auth.id = uuidv1()
      return auth
    })
  },
  requestFacebookCredential() {
    return Api.facebook.signIn()
  },
  createCredential(context, credential) {
    return userRepository.createOrUpdateCredential(context.state.currentUser, credential).then((responseData) => {
      context.commit('onUserFetch', { user: responseData, merge: false })
    })
  },
  logout(context) {
    return Api.user.logout().then(() => {
      context.commit('logout')
      fireLogoutResets(context)
      context.dispatch('onHookEvent', { hookId: HookId.LoginStatus, payload: { isLoggedIn: false } })
      context.commit('setHeadline', { text: 'Respondo' })
    })
  },
  checkLoginStatus(context) {
    const isLoggedInHandler = (isLoggedIn, user) => {
      // eslint-disable-next-line no-param-reassign
      if (user) user = JSON.parse(JSON.stringify(user))
      if (isLoggedIn) {
        return userRepository.getCurrentUser().then((data) => {
          context.commit('setLoginStatus', true)
          context.commit('login', data)
          context.commit('onUserFetch', { user: data })
          context.commit('onUserProviderData', user.providerData)
          context.dispatch('storeUserLocale', user.uid)
          context.dispatch('onHookEvent', { hookId: HookId.LoginStatus, payload: { isLoggedIn } })
          return { user: data }
        })
      }
      context.commit('setLoginStatus', false)
      fireLogoutResets(context)
      context.dispatch('onHookEvent', { hookId: HookId.LoginStatus, payload: { isLoggedIn } })
      return Promise.resolve()
    }
    return Api.user.observeLoginStatus(isLoggedInHandler)
  },
  forgotPassword(context, payload) {
    return Api.user.forgotPassword(payload.email)
  },
  changePassword(context, payload) {
    return Api.user.changePassword(payload)
  },
  changeEmailAddress(context, payload) {
    return Api.user.changeEmailAddress(payload)
  },
  subscribeToDefaultMessages(context) {
    return context.dispatch('subscribeToMessages', context.state.defaultSubscriptions)
  },
  subscribeToMessages(context, messages) {
    if (messages.length === 0) return Promise.resolve()
    // eslint-disable-next-line no-param-reassign
    if (messages[0].hasSubscribed) messages = messages.map(message => ({ name: message.name, type: message.type }))
    const beforeUpdate = context.state.currentUser.subscriptions
    const concattedSubscriptions = context.state.currentUser.subscriptions.concat(messages)
    context.commit('setNotificationSubscriptions', concattedSubscriptions)
    return Api.backend.user.subscribe(messages).then(({ subscriptions }) => {
      context.commit('setNotificationSubscriptions', subscriptions)
      return subscriptions
    }).catch((error) => {
      context.commit('setNotificationSubscriptions', beforeUpdate)
      throw error
    })
  },
  unsubscribeFromMessage(context, { name, type }) {
    const beforeUpdate = context.state.currentUser.subscriptions
    const cleaned = context.state.currentUser.subscriptions
      .filter(subscription => subscription.type === type)
      .filter(subscription => subscription.name !== name)
    context.commit('setNotificationSubscriptions', cleaned)
    return Api.backend.user.unsubscribe({ name, type }).then(({ subscriptions }) => {
      context.commit('setNotificationSubscriptions', subscriptions)
      return subscriptions
    }).catch((error) => {
      context.commit('setNotificationSubscriptions', beforeUpdate)
      throw error
    })
  },
  forceNewFirebaseToken() {
    return Api.user.getCurrentIdToken(true)
  },
  resetFirebaseToken() {
    return Api.user.resetCurrentIdToken()
  },
  unlinkProvider(context, payload) {
    return accountRepository.unlinkProvider(payload.providerId)
  },
  unlinkProviders() {
    /*
        As we want to allow multiple account for a provider, the existing provider links should be deleted.
        Access data for each provider are store in the Firestore database user.
         */
    return accountRepository.unlinkProviders()
  },
  deleteUser() {
    return Api.user.delete()
  },
  updateCredential(context, { platform, credentialId, status = AccessTokenStatus.ACTIVE }) {
    if (!platform) return Promise.reject(new Error('Cannot update access token without having information about the platform'))
    const { currentUser } = storeState
    return userRepository.addAccessToken(currentUser, null, platform, credentialId, status)
  },
  reauthenticateWithGoogle(context) {
    return context.dispatch('requestGoogleCredential').then(newCredential => context.dispatch('requestGoogleMyBusinessAccounts', { credential: newCredential }).then((accounts) => {
      const personalAccount = (accounts.length === 1) ? accounts[0] : accounts.find(account => account.type === 'PERSONAL')
      const unhealthyAccount = context.getters.getCurrentUnhealthyGoogleCredential.account
      if (unhealthyAccount.name !== personalAccount.name) {
        return Promise.reject(new Error(getWrongUserMessage('google')))
      }

      const credential = {
        ...context.getters.getCurrentUnhealthyGoogleCredential,
        ...newCredential,
        id: context.getters.getCurrentUnhealthyGoogleCredential.id,
        status: AccessTokenStatus.ACTIVE,
      }

      // Make sure to sync reviews (especially Google reviews) after reload, therefore we set an old date
      const yesterday = moment().add(-1, 'day').toDate().getTime()
      context.commit('setLastApiReviewSync', yesterday)

      return context.dispatch('createCredential', credential).then(() => context.dispatch('showError', {
        title: this.$t('error.reauthenticate.title', { platform: 'Google' }),
        message: this.$t('error.reauthenticate.message', { platform: 'Google' }),
        isUserFriendly: true,
        onConfirmCallback: () => {
          window.location.reload()
        },
      }))
    })).catch((error) => {
      if (error.message === getWrongUserMessage('google')) {
        context.dispatch('showError', {
          message: this.$t('error.reauthenticate.googleError'),
          isUserFriendly: true,
        })
      }
    })
  },
  reauthenticateWithFacebook(context) {
    return Api.facebook.signIn(true).then(credential => context.dispatch('requestLongLivingAccessToken', { credential }).then(longlivingCredential => context.dispatch('requestFacebookAccount', { credential: longlivingCredential }).then((result) => {
      const account = result.data
      const unhealthyAccount = context.rootGetters.getCurrentUnhealthyFacebookCredential.account

      if (unhealthyAccount.id !== account.id) {
        return Promise.reject(new Error(getWrongUserMessage('facebook')))
      }

      const payload = {
        oldCredential: context.rootGetters.getCurrentUnhealthyFacebookCredential,
        newCredential: longlivingCredential,
      }
      return context.dispatch('storeFacebookCredentials', payload).then(() => {
        const unhealthyFBRepresentations = context.rootGetters.getUnhealthyRepresentations.filter(rep => rep.platform === 'facebook')
        const data = {
          status: RepresentationStatus.ACTIVE,
        }
        Promise.all(unhealthyFBRepresentations.map(representation => context.dispatch('updateRepresentation', {
          locationId: representation.locationId,
          representation,
          data,
        }))).then(() => {
          context.dispatch('showError', {
            title: this.$t('error.reauthenticate.title', { platform: 'Google' }),
            message: this.$t('error.reauthenticate.message', { platform: 'Google' }),
            isUserFriendly: true,
            onConfirmCallback: () => {
              window.location.reload()
            },
          })
        })
      })
    }))).catch((error) => {
      if (error.message === getWrongUserMessage('facebook')) {
        context.dispatch('showError', {
          message: this.$t('error.reauthenticate.facebookError'),
          isUserFriendly: true,
        })
      }
    })
  },
}

if (process.env.NODE_ENV !== 'production') {
  actions.setTestLogin = (context, payload) => {
    context.commit('login', payload)
    context.commit('setLoginStatus', true)
  }
}

export default {
  getters,
  actions,
  mutations,
  state: storeState,
}

import Vue from 'vue'
import Api from '../../api/Api'
import WidgetRepository from '../repositories/WidgetRepository'

const storeState = {
  positions: [
    {
      id: '0',
      description: 'settings.widget.position.zero',
    },
    {
      id: '1',
      description: 'settings.widget.position.one',
    },
    {
      id: '2',
      description: 'settings.widget.position.two',
    },
    {
      id: '3',
      description: 'settings.widget.position.three',
    },
    {
      id: '4',
      description: 'settings.widget.position.four',
    },
    {
      id: '5',
      description: 'settings.widget.position.five',
    },
    {
      id: '6',
      description: 'settings.widget.position.six',
    },
    {
      id: '7',
      description: 'settings.widget.position.seven',
    },
  ],
  widgets: [],
}

const getters = {
  getWidgetPositionById: state => id => state.positions.find(position => position.id === id),
  getWidgets: state => state.widgets,
}

const widgetRepository = new WidgetRepository()

const actions = {
  fetchWidgets(context) {
    return widgetRepository.findAllForCurrentUser().then((data) => {
      context.commit('fetchWidgets', data)
    })
  },
  addWidget(context, payload) {
    if (payload.id) {
      return widgetRepository.update(payload.id, payload).then(() => {
        context.commit('updateWidget', payload)
        return payload
      })
    }
    return widgetRepository.create(payload).then((data) => {
      payload.id = data.id
      payload.userId = data.userId
      context.commit('addWidget', payload)
      return payload
    })
  },
  deleteWidget(context, payload) {
    return widgetRepository.delete(payload.id).then(() => {
      context.commit('deleteWidget', payload)
    })
  },
  fetchWidgetSnippet(context, payload) {
    return Api.fileLoader.getFile('/static/widget/widget_snippet.html')
      .then(snippet => snippet.split('{{widgetId}}').join(payload))
  },
}

const mutations = {
  fetchWidgets(state, payload) {
    storeState.widgets = payload
  },
  addWidget(state, payload) {
    storeState.widgets.push(payload)
  },
  updateWidget(state, payload) {
    const widgetIndex = storeState.widgets.findIndex(widget => widget.id === payload.id)
    Vue.set(storeState.widgets, widgetIndex, payload)
  },
  deleteWidget(state, payload) {
    const index = storeState.widgets.findIndex(widget => widget.id === payload.id)
    storeState.widgets.splice(index, 1)
  },
}


export default {
  actions,
  mutations,
  state: storeState,
  getters,
}

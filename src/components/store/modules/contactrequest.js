import ContactRequestRepository from 'components/store/repositories/ContactRequestRepository'
import moment from 'moment'

const contactRequestRepository = new ContactRequestRepository()

const state = {
  contactRequests: [],
}

const compareByCreated = (a, b) => {
  const format = 'DD.MM.YYYY, HH:mm:ss'
  const momentA = moment(a.timestamp, format)
  const momentB = moment(b.timestamp, format)
  return momentA.diff(momentB) * -1
}

const getters = {
  contactRequests(state) {
    const requests = [...[], ...state.contactRequests]
    return requests.sort(compareByCreated)
  },
  lastThreeContactRequests(state) {
    const requests = [...[], ...state.contactRequests]
    return requests.sort(compareByCreated).splice(0, 3)
  },
}

const mutations = {
  fetchContactRequests(state, contactRequests) {
    state.contactRequests = contactRequests
  },
}

const actions = {
  writeContactRequest(context, payload) {
    return contactRequestRepository.writeContactRequest(payload).then(data => data)
  },
  writeBusinessContactRequest(context, payload) {
    return contactRequestRepository.writeBusinessContactRequest(payload).then(data => data)
  },
  fetchContactRequests(context) {
    return contactRequestRepository.findAllForAdmin().then((contactRequests) => {
      context.commit('fetchContactRequests', contactRequests)
      return contactRequests
    })
  },
}

export default {
  state,
  getters,
  mutations,
  actions,
}

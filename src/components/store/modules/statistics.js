import StatisticsRepository from '../repositories/StatisticsRepository'
import LocationRepository from '../repositories/LocationRepository'
import StatisticsStorageHelper from '../../helper/StatisticsStorageHelper'
import StatisticsHelper from '../../helper/StatisticsHelper'
import { Slack } from '../../api/Slack'
import moment from 'moment'

const state = {
  statistics: [],
  isFetchingStatistics: false,
}

const statisticsRepository = new StatisticsRepository()
const locationRepository = new LocationRepository()

const getters = {
  getStatisticsForCurrentUser() {
    return state.statistics
  },
  getStatisticsPerRepresentation(state, getters) {
    return StatisticsHelper.getStatisticsPerRepresentation(getters.getStatisticsForCurrentUser)
  },
  isFetchingStatistics() {
    return state.isFetchingStatistics
  },
}

const mutations = {
  fetchStatistics(state, { statistics }) {
    state.statistics = statistics
  },
  setIsFetchingStatistics(state, payload) {
    state.isFetchingStatistics = payload
  },
}

const actions = {
  fetchStatistics(context) {
    const userId = statisticsRepository.getCurrentUserUid()
    if (!userId) return Promise.resolve
    context.commit('setIsFetchingStatistics', true)
    // if (process.env.NODE_ENV !== 'production') {
    //   return new Promise((resolve) => {
    //     $.getJSON('static/statistics.json', (statistics) => {
    //       context.commit('fetchStatistics', { userId, statistics })
    //       resolve()
    //     })
    //   })
    // }
    const key = `respondo_statistics_fetchdate_${userId}`
    if (StatisticsStorageHelper.hasKey(key)) StatisticsStorageHelper.clearOldData(userId)

    const date = StatisticsStorageHelper.getFetchDate(userId)
    const beginOfThisYear = moment().startOf('year').toDate()
    const promise = date && StatisticsStorageHelper.hasCachedStatistics(userId)
      ? statisticsRepository.findAllForCurrentUserAfterCacheDate(date)
      : statisticsRepository.findAllForCurrentUserAfterCacheDate(beginOfThisYear)
    return promise.then((statistics) => {
      context.commit('setIsFetchingStatistics', false)
      // Temporary remove all Instagram statistics data
      const clearedData = statistics.filter(statsData => !statsData.representationId.startsWith('ig_'))
      StatisticsStorageHelper.storeFetchDate(new Date(), userId)
      StatisticsStorageHelper.storeStatistics(clearedData, userId).then((data) => {
        context.commit('fetchStatistics', { statistics: data })
        Slack.logText(`counting all statistics for current user ${userId}: ${statistics.length}`)
      })
    }).catch(() => {
      context.commit('setIsFetchingStatistics', false)
    })
  },
  deleteStatisticsForRepresentation(context, { id }) {
    return statisticsRepository.deleteByRepresentationId(id).then(() => {
      StatisticsStorageHelper.clear(locationRepository.getCurrentUserUid())
      return context.dispatch('fetchStatistics')
    })
  },
  deleteStatisticsForLocation(context, location) {
    if (!location.representations || location.representations.length === 0) return Promise.resolve()
    return Promise.all(
      location.representations.map(({ id }) => statisticsRepository.deleteByRepresentationId(id)),
    ).then(() => {
      StatisticsStorageHelper.clear(locationRepository.getCurrentUserUid())
      return context.dispatch('fetchStatistics')
    })
  },
  clearCache() {
    return StatisticsStorageHelper.clear(locationRepository.getCurrentUserUid())
  },
}

if (process.env.NODE_ENV !== 'production') {
  actions.setTestStatistics = (context, payload) => {
    context.commit('fetchStatistics', { statistics: payload, userId: 'lI1wliD35aec7BfcYHe5AB1FoD93' })
  }
}

export default {
  state,
  getters,
  actions,
  mutations,
}

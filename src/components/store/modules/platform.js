const state = {
  platforms: [
    {
      name: 'facebook',
      displayName: 'Facebook',
      shortcut: 'fb',
      icon: '',
      id: 1,
      type: 'api',
      info: 'platforms.facebook.info',
    },
    {
      name: 'instagram',
      displayName: 'Instagram',
      shortcut: 'ig',
      icon: '',
      id: 2,
      type: 'api',
      info: 'platforms.instagram.info',
    },
    {
      name: 'google',
      displayName: 'Google',
      shortcut: 'go',
      icon: '',
      id: 3,
      type: 'api',
      info: 'platforms.google.info',
    },
    {
      name: 'yelp',
      displayName: 'Yelp',
      shortcut: 'yp',
      icon: '',
      id: 4,
      type: 'crawler',
      baseUrl: 'https://www.yelp.de/biz/',
      regex: '^https:\\/\\/www.yelp.[a-zA-Z]{2,3}\\/biz\\/.*',
      info: '',
      steps: ['settings.connections.directReply.yelp.one',
        'settings.connections.directReply.yelp.two',
        'settings.connections.directReply.yelp.three',
        'settings.connections.directReply.yelp.four',
        'settings.connections.directReply.yelp.five',
        'settings.connections.directReply.yelp.six'],
      images: ['https://res.cloudinary.com/foodiscovr/image/upload/v1574162175/respondo/yelp_1.png',
        'https://res.cloudinary.com/foodiscovr/image/upload/v1574162175/respondo/yelp_2.png',
        'https://res.cloudinary.com/foodiscovr/image/upload/v1574162175/respondo/yelp_3.png'],
    },
    {
      name: 'tripadvisor',
      displayName: 'Tripadvisor',
      shortcut: 'ta',
      icon: '',
      id: 5,
      type: 'crawler',
      baseUrl: 'https://www.tripadvisor.de/Restaurant_Review-g',
      regex: '^https:\\/\\/www.tripadvisor.[a-zA-Z]{2,3}\\/(Restaurant|Attraction)_Review-g[a-zA-Z0-9]{1,}-d[a-zA-Z0-9]{1,}-.*',
      info: '',
      steps: ['settings.connections.directReply.tripadvisor.one',
        'settings.connections.directReply.tripadvisor.two',
        'settings.connections.directReply.tripadvisor.three',
        'settings.connections.directReply.tripadvisor.four',
        'settings.connections.directReply.tripadvisor.five',
        'settings.connections.directReply.tripadvisor.six',
        'settings.connections.directReply.tripadvisor.seven'],
      images: ['https://res.cloudinary.com/foodiscovr/image/upload/v1574238402/respondo/tripadvisor_1.png',
        'https://res.cloudinary.com/foodiscovr/image/upload/v1574238402/respondo/tripadvisor_2.png',
        'https://res.cloudinary.com/foodiscovr/image/upload/v1574238402/respondo/tripadvisor_3.png'],
    },
    {
      name: 'opentable',
      displayName: 'OpenTable',
      shortcut: 'ot',
      icon: '',
      id: 6,
      type: 'crawler',
      baseUrl: 'https://www.opentable.de',
      regex: '^https:\\/\\/www.opentable.[a-zA-Z]{2,3}(\\/r\\/){0,1}(?!.*-reservations-|.*?page=|.*?lang=|.*\\/n\\/|.*\\/s\\/|.*\\/start\\/|.*\\/landmark\\/).*',
      info: 'platforms.opentable.info',
      steps: ['settings.connections.directReply.opentable.one',
        'settings.connections.directReply.opentable.two',
        'settings.connections.directReply.opentable.three',
        'settings.connections.directReply.opentable.four',
        'settings.connections.directReply.opentable.five'],
      images: ['https://res.cloudinary.com/foodiscovr/image/upload/v1574162175/respondo/opentable_1.png',
        'https://res.cloudinary.com/foodiscovr/image/upload/v1574162175/respondo/opentable_2.png',
        'https://res.cloudinary.com/foodiscovr/image/upload/v1574162175/respondo/opentable_3.png'],
    },
  ],
}

const getters = {
  getAllPlatforms() {
    return state.platforms
  },
}

const mutations = {}

const actions = {}

export default {
  state,
  getters,
  actions,
  mutations,
}

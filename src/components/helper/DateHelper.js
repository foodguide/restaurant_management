import moment from 'moment'

export default class DateHelper {
  static getDateFromInstagramTimestamp(timestamp) {
    return new Date(parseInt(timestamp, 10) * 1000)
  }

  static getDateFromTimestamp(timestamp) {
    if (!timestamp) return null
    if (typeof timestamp === 'string') return new Date(timestamp)
    if (timestamp instanceof Date) return timestamp
    if (typeof timestamp === 'number') return new Date(parseInt(timestamp, 10))
    return timestamp.toDate()
  }

  static getMomentFromTimestamp(timestamp) {
    const date = DateHelper.getDateFromTimestamp(timestamp)
    return moment(date)
  }

  static getFormattedDateForTimestamp(timestamp, platform = null) {
    const momentObject = moment(DateHelper.getDateFromTimestamp(timestamp))
    const dateTimeFormat = 'lll'
    const dateFormat = 'll'

    switch (platform) {
      case 'facebook': return momentObject.format(dateTimeFormat)
      case 'instagram': return momentObject.format(dateTimeFormat)
      case 'google': return momentObject.format(dateTimeFormat)
      case 'opentable': return momentObject.format(dateFormat)
      case 'yelp': return momentObject.format(dateFormat)
      case 'tripadvisor': return momentObject.format(dateFormat)

      default:
        return momentObject.format(dateTimeFormat)
    }
  }

  static getFormattedDateForReview(review) {
    return DateHelper.getFormattedDateForTimestamp(review.created, review.platform)
  }

  static getFutureMoment() {
    return moment().add(1, 'year')
  }
}

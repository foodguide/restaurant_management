import { facebook } from '../helper/Scopes'

const VueFB = {}
const options = {
  appId: process.env.FACEBOOK_APP_ID,
  autoLogAppEvents: true,
  xfbml: true,
  version: 'v4.0',
}
VueFB.install = function install(Vue) {
  (function (d, s, id) {
    let js


    const fjs = d.getElementsByTagName(s)[0]
    if (d.getElementById(id)) {
      return
    }
    js = d.createElement(s)
    js.id = id
    js.src = '//connect.facebook.net/en_US/sdk.js'
    fjs.parentNode.insertBefore(js, fjs)
    console.log('setting fb sdk')
  }(document, 'script', 'facebook-jssdk'))

  window.fbAsyncInit = function onSDKInit() {
    FB.init(options)
    FB.AppEvents.logPageView()
    Vue.FB = FB
    window.dispatchEvent(new Event('fb-sdk-ready'))
  }
  Vue.FB = undefined
}

export const facebookApi = {
  send: (url, options = null, method = null) => {
    if (!method) method = 'GET'
    if (!options) options = {}
    return new Promise((resolve, reject) => {
      FB.api(url, method, options, (response) => {
        if (!response || response.error) {
          if (response.error) {
            const error = new Error(response.error.message)
            error.code = response.error.code
            error.error_subcode = response.error.error_subcode
            error.platform = 'facebook'
            reject(error)
          } else {
            reject(new Error('Facebook response is empty'))
          }
        } else {
          resolve(response)
        }
      })
    })
  },
  signIn(reauthenticate = false) {
    const parameters = { scope: facebook.asString() }
    if (reauthenticate) parameters.auth_type = 'reauthenticate'
    return new Promise((resolve, reject) => {
      FB.login((response) => {
        if (response) {
          resolve(response.authResponse)
        } else {
          reject(new Error('Facebook response is empty'))
        }
      }, parameters)
    })
  },
  sendWithPagination(url, options = {}, method = 'GET', after = null, aggregatedData = []) {
    if (after) options.after = after
    return this.send(url, options).then((response) => {
      const hasData = !!response && response.data.length > 0
      const hasNext = !!response && !!response.paging && response.paging.next !== undefined

      if (hasData) aggregatedData.push(...response.data)

      if (!hasNext) return aggregatedData
      const nextUrl = new URL(response.paging.next)
      const nextAfter = nextUrl.searchParams.get('after')
      if (hasData && hasNext && nextAfter !== undefined) return this.sendWithPagination(url, options, method, nextAfter, aggregatedData)
      return aggregatedData
    })
  },
  getPages(credential) {
    const { accessToken } = credential
    return this.sendWithPagination('me/accounts', { access_token: accessToken })
  },
  getLocations(accessToken, pageId) {
    const url = `/${pageId}/locations`
    const options = {
      access_token: accessToken,
      fields: 'name_with_location_descriptor, access_token',
    }
    return this.send(url, options)
  },
  getInstagramPages(credential) {
    const { accessToken } = credential
    const url = '/me/accounts'
    const options = {
      access_token: accessToken,
      fields: 'instagram_business_account, access_token',
    }
    return this.send(url, options).then(pages => pages.data.filter(page => page.instagram_business_account && page.instagram_business_account.id))
  },
  getInstagramPageName(credential, pageId) {
    const { accessToken } = credential
    const url = `/${pageId}`
    const options = {
      access_token: accessToken,
      fields: 'username',
    }
    return this.send(url, options)
  },
  getPermissions(credential) {
    return this.send('/me/permissions', {
      access_token: credential.accessToken,
    }).then(response => response.data)
  },
  sendInstagramReply(access_token, review, message) {
    return this.send(`/${review.externalId}/replies?message=${message}`, { access_token }, 'POST')
  },
  sendFacebookReply(access_token, review, message) {
    return this.send(`/${review.externalId}/comments`, { access_token, message }, 'POST')
  },
}

export default VueFB

// The full list of error codes can be found here:
// https://developers.facebook.com/docs/graph-api/using-graph-api/error-handling
export const FacebookErrorCodes = {
  API_SESSION_EXPIRED: 102,
  TOO_MANY_REQUESTS: 4,
  API_UNKNOWN: 1,
  API_SERVICE_DOWN: 2,
  TOO_MANY_USER_REQUESTS: 17,
  NO_PERMISSION: 10,
  ACCESSTOKEN_EXPIRED: 190,
  APP_LIMIT_REACHED: 341,
  RULES_VIOLATION: 368,
  DOUBLED_POST: 506,
  POST_LINK: 1609005,
  CUSTOM_ERROR: 'custom_facebook_error',
  MISSING_PERMISSIONS: 'missing_permissions',
}

export const FacebookErrorSubcodes = {
  APP_NOT_INSTALLED: 458,
  USER_CHECKPOINT: 459,
  PASSWORD_CHANGED: 460,
  EXPIRED: 463,
  NOT_VERIFIED_USER: 464,
  UNVALID_ACCESSTOKEN: 467,
  UNVALID_SESSION: 492,
}

export const FacebookPermissionStatus = {
  GRANTED: 'granted',
  DECLINED: 'declined',
}

export default {
  getLabel(subscription) {
    return `settings.notifications.${subscription.name}`
  },
}

import Api from 'src/components/api/Api'

const initialState = {}

const state = initialState

const getters = {
}

const mutations = {
  instagramStateReset(state) {
    state = initialState
  },
}

const actions = {
  requestInstagramPages(context, payload) {
    return Api.facebook.getInstagramPages(payload.credential)
  },
  requestInstagramPageName(context, payload) {
    return Api.facebook.getInstagramPageName(payload.credential, payload.id)
  },
  requestInstagramReviews(context, location) {
    return Api.backend.sync.apiForLocation(location.id, ['instagram'])
  },
}

export default {
  state,
  getters,
  actions,
  mutations,
}

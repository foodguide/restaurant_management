import { user as testUser } from '../fixtures/testdata'

const fakeOnChanges = []

export default {
  create: realUserApi => ({
    ...realUserApi,
    register() {
      fakeOnChanges.forEach(onChange => onChange(true, testUser))
      return Promise.resolve(testUser)
    },
    login() {
      fakeOnChanges.forEach(onChange => onChange(true, testUser))
      return Promise.resolve(testUser)
    },
    linkWithEmail() {
      return Promise.resolve(testUser)
    },
    linkWithCredential() {
      return Promise.resolve(testUser)
    },
    loginWithFacebook() {
      fakeOnChanges.forEach(onChange => onChange(true, testUser))
      return Promise.resolve(testUser)
    },
    loginWithProvider() {
      fakeOnChanges.forEach(onChange => onChange(true, testUser))
      return Promise.resolve(testUser)
    },
    loginWithToken() {
      fakeOnChanges.forEach(onChange => onChange(true, testUser))
      return Promise.resolve(testUser)
    },
    logout() {
      fakeOnChanges.forEach(onChange => onChange(false, testUser))
      return Promise.resolve()
    },
    delete() {
      return Promise.resolve()
    },
    observeLoginStatus(onChange) {
      fakeOnChanges.push(onChange)
    },
    getCurrentUser() {
      return Promise.resolve(testUser)
    },
    getCurrentIdToken() {
      return Promise.resolve(testUser.id)
    },
    getIdTokenResult() {
      return Promise.resolve(testUser.id)
    },
    forgotPassword() {
      return Promise.resolve()
    },
    changePassword() {
      return Promise.resolve()
    },
    changeEmailAddress() {
      return Promise.resolve()
    },
    getEmailCredential() {
      return Promise.resolve()
    },
  }),
}

import BaseRepository from './BaseRepository'

export default class StatisticsRepository extends BaseRepository {
  get managedCollection() {
    return 'representationStatistics'
  }

  deleteByRepresentationId(representationId) {
    return this.deleteWhere('representationId', '==', representationId)
  }

  findAllForCurrentUserAfterCacheDate(date) {
    return this.commitRead(
      this.collection
        .where('userId', '==', this.getCurrentUserUid()).where('date', '>', date)
        .get(),
    )
  }
}

describe('Test Statistics Page with normal login', () => {
  beforeEach(() => {
    cy.visit('/')
    indexedDB.deleteDatabase('firebaseLocalStorageDb')
    cy.login()
    cy.get('[data-cy=login-button]').eq(0).click()
    cy.get('[data-cy=navbar-button]').click()
    cy.get('[data-cy=navbar-item]').eq(2).click()
  })

  it('shows correct header information', () => {
    cy.get('[data-cy=trend-card]').contains('---')
    cy.get('[data-cy=average-card]').contains('2.36')
    cy.get('[data-cy=total-card]').contains('290')
    cy.get('[data-cy=open-card]').contains('290/290')
  })

  it('all charts are shown', () => {
    cy.get('[data-cy=average-chart]').should('be.visible')
    cy.get('[data-cy=growth-chart]').should('be.visible')
    cy.get('[data-cy=platform-distribution-chart]').should('be.visible')
    cy.get('[data-cy=rating-distribution-chart]').should('be.visible')
  })

  it('shows toolbar and filter options', () => {
    cy.get('[data-cy=toolbar]').should('be.visible')

    cy.get('[data-cy=location-menu]').should('be.visible')
    cy.get('[data-cy=location-menu]').click()
    cy.get('[data-cy=location-item]').should('have.length', 2)

    cy.get('[data-cy=platform-menu-single]').should('not.exist')
    cy.get('[data-cy=platform-menu]').should('be.visible')

    cy.get('[data-cy=time-menu]').should('be.visible')
    cy.get('[data-cy=time-menu-range]').should('not.exist')
    cy.get('[data-cy=time-menu]').click()
    cy.get('[data-cy=time-menu-item]').should('have.length', 4)
    cy.get('[data-cy=time-menu-item]').eq(1).click()
    cy.get('[data-cy=time-menu-range]').should('be.visible')
    cy.get('[data-cy=time-menu]').should('not.exist')
    cy.get('[data-cy=time-menu-range]').click()
    cy.get('[data-cy=time-menu-item]').should('have.length', 4)
    cy.get('[data-cy=time-menu-item]').eq(0).click()
  })
})

describe('Test Statistics Page with normal login Mobile', () => {
  beforeEach(() => {
    cy.viewport('iphone-6')
    cy.visit('/')
    indexedDB.deleteDatabase('firebaseLocalStorageDb')
    cy.login()
    cy.get('[data-cy=login-button]').eq(0).click()
    cy.get('[data-cy=navbar-button]').click()
    cy.get('[data-cy=navbar-item]').eq(2).click()
  })

  it('shows correct header information', () => {
    cy.get('[data-cy=trend-card]').contains('---')
    cy.get('[data-cy=average-card]').contains('2.36')
    cy.get('[data-cy=total-card]').contains('290')
    cy.get('[data-cy=open-card]').contains('290/290')
  })

  it('all charts are shown', () => {
    cy.get('[data-cy=average-chart]').should('be.visible')
    cy.get('[data-cy=growth-chart]').should('be.visible')
    cy.get('[data-cy=platform-distribution-chart]').should('be.visible')
    cy.get('[data-cy=rating-distribution-chart]').should('be.visible')
  })
})

describe('Test Statistics Page without locations', () => {
  beforeEach(() => {
    cy.viewport('iphone-6')
    cy.visit('/')
    indexedDB.deleteDatabase('firebaseLocalStorageDb')
    cy.login()
    cy.setLocations([])
    cy.setReviews([])
    cy.get('[data-cy=login-button]').eq(0).click()
    cy.get('[data-cy=navbar-button]').click()
    cy.get('[data-cy=navbar-item]').eq(2).click()
  })

  it('shows empty hint', () => {
    cy.get('[data-cy=statistics-empty]').should('be.visible')
  })
})

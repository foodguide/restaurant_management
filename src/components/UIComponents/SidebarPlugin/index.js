import Sidebar from './SideBar.vue'

const SidebarStore = {
  showSidebar: false,
  sidebarLinks: [
    {
      name: 'menu.locations',
      icon: 'ti-location-pin',
      path: '/app/locations',
    },
    {
      name: 'menu.quickReplies',
      icon: 'ti-comment-alt',
      path: '/app/quick',
    },
    {
      name: 'menu.statistics',
      icon: 'ti-stats-up',
      path: '/app/statistics',
    },
    {
      name: 'menu.settings',
      icon: 'ti-settings',
      path: '/app/settings',
    },
    {
      name: 'terms.imprint',
      icon: 'ti-stamp',
      path: '/app/imprint',
    },
    {
      name: 'terms.tos',
      icon: 'ti-write',
      path: '/app/terms-of-service',
    },
  ],
  sidebarActions: [
    {
      name: 'terms.logout',
      icon: 'ti-control-eject',
      dispatchAction: 'logout',
    },
  ],
  displaySidebar(value) {
    this.showSidebar = value
  },
}

const SidebarPlugin = {
  install(Vue) {
    Vue.mixin({
      data() {
        return {
          sidebarStore: SidebarStore,
        }
      },
    })

    Object.defineProperty(Vue.prototype, '$sidebar', {
      get() {
        return this.$root !== undefined
          ? this.$root.sidebarStore
          : null
      },
    })
    Vue.component('side-bar', Sidebar)
  },
}

export default SidebarPlugin

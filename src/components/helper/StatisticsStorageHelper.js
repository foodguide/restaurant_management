import firebase from 'firebase/app'
import 'firebase/firestore'
import DateHelper from './DateHelper'
import CachingDBHelper from './CachingDBHelper'

const cachingDBHelper = new CachingDBHelper()

const { Timestamp } = firebase.firestore

export default class StatisticsStorageHelper {
  static fetchDateKey(userId) {
    return `respondo_statistics_fetchdatekey_${userId}`
  }

  static storeFetchDate(date, userId) {
    const key = StatisticsStorageHelper.fetchDateKey(userId)
    const value = date.toISOString()
    localStorage.setItem(key, value)
  }

  static async getStatistics(userId) {
    const dataCollection = await cachingDBHelper.db.representationStatistics.where('userId').equals(userId)
    const data = await dataCollection.toArray()
    if (!data || data.length === 0) return null
    return data.map((statistic) => {
      const date = new Timestamp(statistic.date.seconds, statistic.date.nanoseconds)
      return { ...statistic, date }
    })
  }

  static getFetchDate(userId) {
    const isoString = localStorage.getItem(StatisticsStorageHelper.fetchDateKey(userId))
    if (!isoString) return null
    return new Date(isoString)
  }

  static async storeStatistics(statistics, userId) {
    const alreadyCached = await StatisticsStorageHelper.getStatistics(userId)
    const data = alreadyCached
      ? StatisticsStorageHelper.concatStatistics({ alreadyCached, fetched: statistics })
      : statistics
    try {
      await cachingDBHelper.db.representationStatistics.bulkAdd(statistics)
    } catch (e) {
      if (!e.message.startsWith('representationStatistics.bulkAdd()')) throw e
    }
    return data
  }

  static doesNewOneExistsInCache(newOne, alreadyCached) {
    return alreadyCached.indexOf((cachedOne) => {
      const isSameRepresentation = cachedOne.representationId === newOne.representationId
      const cachedMoment = DateHelper.getMomentFromTimestamp(cachedOne.created)
      const newMoment = DateHelper.getMomentFromTimestamp(newOne.created)
      return isSameRepresentation && cachedMoment.isSame(newMoment, 'day')
    }) > 0
  }

  static concatStatistics({ alreadyCached, fetched }) {
    if (!alreadyCached && !fetched) return []
    if (!alreadyCached && fetched) return fetched
    if (alreadyCached && !fetched) return alreadyCached
    return [
      ...alreadyCached,
      ...fetched.filter(newOne => !StatisticsStorageHelper.doesNewOneExistsInCache(newOne, alreadyCached)),
    ]
  }

  static async clear(userId) {
    localStorage.removeItem(StatisticsStorageHelper.fetchDateKey(userId))
    const statisticIds = (await StatisticsStorageHelper.getStatistics(userId)).map(stat => stat.id)
    return cachingDBHelper.db.representationStatistics.bulkDelete(statisticIds)
  }

  static clearOldData(userId) {
    localStorage.removeItem(`respondo_statistics_fetchdate_${userId}`)
    const statisticIds = StatisticsStorageHelper.getStatistics(userId).map(stat => stat.id)
    return cachingDBHelper.db.representationStatistics.bulkDelete(statisticIds)
  }

  static hasKey(key) {
    return !!localStorage.getItem(key)
  }

  static hasCachedStatistics(userId) {
    return this.hasKey(StatisticsStorageHelper.fetchDateKey(userId))
  }
}

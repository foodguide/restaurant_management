import FloatingButton from 'src/components/UIComponents/Buttons/FloatingButton'
import PlatformIcon from 'src/components/UIComponents/Icons/PlatformIcon'

describe('FloatingButton', () => {
  const testPath = '/icon'
  const routes = [{ path: testPath, component: PlatformIcon }]

  let wrapper
  let button
  let buttonWrapper
  before((done) => {
    cy.unitTest(FloatingButton, routes, null).then((unitTest) => {
      wrapper = unitTest.wrapper
      button = unitTest.mountedComponent
      buttonWrapper = unitTest.componentWrapper
      done()
    })
  })

  it('has set up correctly', () => {
    expect(wrapper.vm.$route.path).to.eq('/')
    cy.get('button')
  })

  it('links to the correct component', (done) => {
    buttonWrapper.setProps({ path: testPath })
    cy.get('a')
      .should('have.attr', 'href')
      .and('include', testPath)
    cy.get('a')
      .click().then(() => {
        expect(wrapper.vm.$route.path).to.eq(testPath)
        done()
      })
  })
})

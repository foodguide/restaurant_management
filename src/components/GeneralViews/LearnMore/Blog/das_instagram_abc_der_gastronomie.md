﻿# Das Instagram ABC der Gastronomie

Kostenlos aber wirkungsvoll! Ein gepflegtes Instagram-Profil kommt vor allem bei der jüngeren Generation sehr gut an. Wir wissen genau wovon wir reden. Denn Respondo - Bewertungsmanagement gehört zu [Foodguide](https://thefoodguide.de/), europas größten sozialen Netzwerk für Gastronomie. Mit über 1 Millionen Follower auf diversen Accounts haben wir viele Erfahrung sammeln können, die wir hier gerne mit euch teilen.

Instagram ist die perfekte Marketingplattform für Restaurants, Cafés und Bars, denn Menschen lieben es, ihr Essen, eine gesellige Runde oder den abendlichen Drink zu posten. Mit ein wenig Aufwand lockst du über Instagram neue Kunden in dein Lokal und machst dich auf dem sozialen Netzwerk sichtbar.

-   **Baue ein Portfolio deines Restaurants auf**  
  
Lass dein Instagram-Profil deine Speisekarte sein! Leute besuchen deinen Kanal um raus zu finden, ob sich ein Besuch lohnt. Ob es etwas gibt, das ihnen schmecken könnte. Also biete einfach eine bunte Auswahl deiner Speisen an, die so gut aussehen, dass man vorbei kommen muss!
  

-   **Stelle Neuheiten vor**


Du hast deine Karte erweitert oder bietest saisonale Speisen an? Erzähle deiner Instagram-Community davon! Und nicht vergessen: Immer fleißig auch andere Fotos kommentieren und liken, sodass potentielle Gäste überhaupt wissen, dass es deinen Kanal gibt!


-   **Verwende eine richtige Kamera**
    
Na klar, Instagram ist für Handyfotos gemacht, nicht umsonst, kann man nur von diesem Fotos hochladen. Trotzdem heißt das nicht, dass keine professionellen Aufnahmen gepostet werden können. Fotografiere Deko, Interior oder toll angerichtete Speisen mit einer Spiegelreflexkamera und lade sie dann auf dein Handy. Die Qualität der Fotos ist einfach besser, was deinem Profil auf den ersten Blick einen hochwertigeren Look verpasst.


-   **Setze dein Essen richtig in Szene**

#foodporn-Profis raten Essen immer in natürlichem Licht zum Beispiel an einem Fenster zu fotografieren und auf keinen Fall einen Blitz zu verwenden. Beim Hintergrund ist wichtig, dass er dem Gericht nicht die Show stiehlt. Ruhige Flächen bieten sich an, wie ein tolles Holzbrett, eine Schieferplatte oder ein glatter weißer Tisch. Wichtig nur: Die Farbe sollte sich nicht mit den Zutaten beißen. Insidertipp: Ein sattes Auge ist kritischer ;)

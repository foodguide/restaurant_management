import EventRepository from '../repositories/EventRepository'

const eventRepository = new EventRepository()

const actions = {
  writeEvent(context, payload) {
    return eventRepository.addEvent(payload)
  },
}

export default {
  actions,
}

export const EventName = {
  Login: 'login',
  Register: 'register',
  Location: 'LocationsPage',
  Review: 'ReviewsPage',
  Statistics: 'StatisticsPage',
  Settings: 'SettingsPage',
  LocationConnector: 'LocationConnectorPage',
  QuickReply: 'QuickReplyPage',
  Payment: 'PaymentPage',
  CreateLocationStart: 'CreateLocationStart',
  CreateLocationFinish: 'CreateLocationFinish',
  DeleteLocationStart: 'DeleteLocationStart',
  DeleteLocationFinish: 'DeleteLocationFinish',
  DeleteRepresentationStart: 'DeleteRepresentationStart',
  DeleteRepresentationFinish: 'DeleteRepresentationFinish',
  CreateRepresentationStart: 'CreateRepresentationStart',
  CreateRepresentationFinish: 'CreateRepresentationFinish',
  SendReplyStart: 'SendReplyStart',
  SendReplyFinish: 'SendReplyFinish',
  CheckoutStart: 'CheckoutStart',
  CheckoutFinish: 'CheckoutFinish',
  CheckoutTestPlanStart: 'CheckoutTestPlanStart',
  CheckoutTestPlanFinish: 'CheckoutTestPlanFinish',
  CreateWidget: 'CreateWidget',
  DeleteWidget: 'DeleteWidget',
  Error: 'Error',
}

export const EventType = {
  Visit: 'visit',
  Action: 'action',
  Error: 'error',
}

import { capitalize } from 'src/components/helper/StringHelper'

describe('StringHelper.js', () => {
  it('should capitalize a string', () => {
    expect(capitalize('test')).to.eq('Test')
    expect(capitalize('i will test this with a string that has multiple words.')).to.eq('I will test this with a string that has multiple words.')
  })
})

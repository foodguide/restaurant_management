import CrawlerRepository from '../repositories/CrawlerRepository'
import Api from '../../api/Api'
import { HookId } from './hooks'

const state = {}
const getters = {}
const mutations = {}

const crawlerRepository = new CrawlerRepository()

const activeCrawlers = []

const getRepresentationListFromLocations = (locations) => {
  const representations = []
  locations.forEach((location) => {
    if (location.representations) {
      location.representations.forEach((representation) => {
        representations.push(representation)
      })
    }
  })
  return representations
}

const getLocationForRepresentationId = (locations, representationId) => {
  const locationId = representationId.split('_')[1]
  return locations.find(location => location.id === locationId)
}

const startListeningForRepresentation = (context, representation) => crawlerRepository.addCrawlerTaskListener(representation.id, (data) => {
  if (!data) return
  if (data.state === 'RUNNING' || data.state === 'WAITING') {
    const index = activeCrawlers.indexOf(data.id)
    if (index < 0) {
      activeCrawlers.push(data.id)
      sendProgressTask(context, representation.id)
    }
  } else if (data.state === 'DONE') {
    const index = activeCrawlers.indexOf(data.id)
    if (index > -1) {
      activeCrawlers.splice(index, 1)
      context.dispatch('fetchReviews', true)

      removeProgressTask(context, representation.id)

      const crawlerSuccessful = !data.error
      if (crawlerSuccessful) {
        sendSuccessNotification(context, data.id)
      } else {
        showSpecificError(context, data.id, data.error)
      }
    }
  } else if (data.state === 'FAILED') {
    const index = activeCrawlers.indexOf(data.id)
    if (index > -1) {
      activeCrawlers.splice(index, 1)
      removeProgressTask(context, representation.id)
      showGeneralError(context, data.id)
    }
  }
})

const stopListeningForRepresentation = (context, representation) => {
  crawlerRepository.removeCrawlerTaskListener(representation.id)
  const index = activeCrawlers.indexOf(representation.id)
  if (index > -1) {
    activeCrawlers.splice(index, 1)
    removeProgressTask(context, representation.id)
  }
}

const getDataFromRepresentationId = (context, representationId) => {
  const locations = context.getters.getLocationsForCurrentUser
  const representations = getRepresentationListFromLocations(locations)
  const representation = representations.find(representation => representation.id === representationId)
  if (!representation) return null
  const platform = context.getters.getAllPlatforms.find(platform => platform.name === representation.platform)
  const location = getLocationForRepresentationId(locations, representationId)
  if (!location) return null

  return {
    location,
    platform,
  }
}

const sendSuccessNotification = (context, representationId) => {
  const data = getDataFromRepresentationId(context, representationId)
  if (!data) return

  context.dispatch('onHookEvent', {
    hookId: HookId.CrawlerSuccess,
    payload: {
      location: data.location,
      platform: data.platform,
    },
  })
}

const sendProgressTask = (context, representationId) => {
  const data = getDataFromRepresentationId(context, representationId)
  if (!data) return

  context.dispatch('onHookEvent', {
    hookId: HookId.CrawlerProgress,
    payload: {
      representationId,
      location: data.location,
      platform: data.platform,
    },
  })
}

const removeProgressTask = (context, representationId) => {
  context.commit('removeProgressTask', {
    id: representationId,
  })
}

const showGeneralError = (context, representationId) => {
  const data = getDataFromRepresentationId(context, representationId)
  if (!data) return

  context.dispatch('onHookEvent', {
    hookId: HookId.CrawlerError,
    payload: {
      location: data.location,
      platform: data.platform,
    },
  })
}

const showSpecificError = (context, representationId, error) => {
  const data = getDataFromRepresentationId(context, representationId)
  if (!data) return

  context.dispatch('onHookEvent', {
    hookId: HookId.CrawlerError,
    payload: {
      location: data.location,
      platform: data.platform,
      errorCode: error.code,
    },
  })
}

const actions = {
  requestCrawlerReviews(context, payload) {
    return Api.backend.sync.crawlerForRepresentation(payload.representationId, payload.locationId)
  },
  startListeningForCrawlerTasks(context) {
    const representations = getRepresentationListFromLocations(context.getters.getLocationsForCurrentUser)
    representations.forEach((representation) => {
      startListeningForRepresentation(context, representation)
    })
  },
  startListeningForRepresentation(context, payload) {
    return startListeningForRepresentation(context, payload)
  },
  stopListeningForRepresentations(context, payload) {
    if (!payload) return
    payload.forEach((representation) => {
      stopListeningForRepresentation(context, representation)
    })
  },
  stopListeningForAllRepresentations() {
    crawlerRepository.removeAllCrawlerTaskListener()
  },
}

export default {
  state,
  getters,
  mutations,
  actions,
}

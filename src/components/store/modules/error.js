import getUserFriendlyMessage from 'src/components/helper/ErrorHelper'

const initialState = {
  active: false,
  message: '',
  title: null,
  onConfirmCallback: null,
}

const state = { ...initialState }

const getters = {
  getCurrentError() {
    return {
      active: state.active,
      message: state.message,
      title: state.title,
    }
  },
}

const mutations = {
  updateError(state, payload) {
    state.active = payload.active
    state.message = payload.message
    state.title = payload.title ? payload.title : null
    state.onConfirmCallback = payload.onConfirmCallback ? payload.onConfirmCallback : null
  },
}

const actions = {
  showError(context, payload) {
    const message = getUserFriendlyMessage(payload)
    if (process.env.NODE_ENV === 'development') {
      console.log(payload)
    }
    context.commit('updateError', {
      active: true,
      message,
      title: payload.title,
      onConfirmCallback: payload.onConfirmCallback,
    })
  },
  showErrorMessage(context, payload) {
    context.commit('updateError', {
      active: true,
      message: payload,
    })
  },
  errorConfirmed(context) {
    const callback = context.state.onConfirmCallback
    context.commit('updateError', {
      active: false,
      message: '',
    })
    if (typeof callback === 'function') callback()
  },
}

export default {
  state,
  getters,
  actions,
  mutations,
}

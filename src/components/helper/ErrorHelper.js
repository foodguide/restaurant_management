import { getUserFriendlyCrawlerErrorMessage } from '../api/Crawler'

const fallbackErrorMessage = 'error.fallback'

const getUserFriendlyMessageForRequestError = (error) => {
  const url = error.request.responseURL
  if (url === process.env.CRAWLER_BASE_URL) {
    return getUserFriendlyCrawlerErrorMessage(error)
  } return null
}

const getUserFriendlyMessageForErrorCode = (code) => {
  switch (code) {
    case 'auth/credential-already-in-use':
      return 'error.emailInUse'
    case 'auth/wrong-password':
      return 'error.passwordMailInvalid'
    case 'auth/popup-blocked':
      return 'error.popup'
    default:
      return null
  }
}

const getUserFriendlyMessage = (error) => {
  if (!error) return fallbackErrorMessage
  if (error.isUserFriendly) return error.message
  let message = null
  if (error.request) {
    message = getUserFriendlyMessageForRequestError(error)
  } else if (!message && error.code) {
    message = getUserFriendlyMessageForErrorCode(error.code)
    if (message) return message
  }
  return message || fallbackErrorMessage
}

export default getUserFriendlyMessage

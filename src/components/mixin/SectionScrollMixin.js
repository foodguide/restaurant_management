export default {
  methods: {
    getContainerSelector() {
      // this needs to be overwritten by component
    },
    getSectionsSelector() {
      // this needs to be overwritten by component
    },
    getNavigationButtonsSelector() {
      // this needs to be overwritten by component
    },
    getRelatedButtonSelector(sectionSelector) {
      // this needs to be overwritten by component
    },
    getActiveClass() {
      return 'active' // can be overwritten
    },
    getActiveNavigationButtonSelector() {
      return `${this.getNavigationButtonsSelector()}.${this.getActiveClass()}`
    },
    makeActive(selector) {
      const activeClass = this.getActiveClass()
      // don't do anything if active is already correct
      if ($(selector).hasClass(activeClass)) {
        return
      }
      // remove the active class from the button that was active before
      $(this.getActiveNavigationButtonSelector()).removeClass(activeClass)
      // add the active class to the new button
      $(selector).addClass(activeClass)
    },
    getIdSelectors(selector) {
      return this.getIds(selector)
        .map(id => `#${id}`) // add hash for later id selection
    },
    // / returns the ids from the elements which are found with the given css selector
    getIds(selector) {
      return $(selector)
        .map(function () {
          return this.id
        }) // use the jquery map (not normal map function) to extract the ids
        .get() // jquery - unwrap (every object from jquery has a lot of additional properties and functions. with get() the object is cleaned and a plain old old js object.
        .filter(id => !!id) // filter null & undefined (not every element must have an id)
    },
    getSectionIdSelectors() {
      const idSelectors = this.getIdSelectors(this.getSectionsSelector())
      if (!idSelectors || idSelectors.length === 0) {
        console.error('No id\'s found for the sections. SectionScrollMixin can only work with sections that has an id.')
      }
      return idSelectors
    },
    onScroll() {
      const sectionSelector = this.findSectionWhichIsMostInScreen()
      this.makeActive(this.getRelatedButtonSelector(sectionSelector))
    },
    listenForActiveMenuButton() {
      $(this.getContainerSelector()).on('scroll resize', () => {
        this.onScroll()
      })
      // initial onScroll
      this.onScroll()
    },
    getOffsetCenterFromSection(sectionSelector) {
      const sectionSelectors = this.getSectionIdSelectors()
      const index = sectionSelectors.findIndex(selector => selector === sectionSelector)
      if (index < 0) return
      let previousContentHeight = 0
      let counter = 0
      while (counter < index) {
        previousContentHeight += $(sectionSelectors[counter]).height()
        counter++
      }
      return previousContentHeight + ($(sectionSelector).height() * 0.2)
    },
    getOffsetCenterFromContentContainer() {
      return $(this.getContainerSelector()).scrollTop()
    },
    getSectionOffsets() {
      const containerOffset = this.getOffsetCenterFromContentContainer()
      return this.getSectionIdSelectors()
        .map(selector => ({
          selector,
          center: Math.abs(this.getOffsetCenterFromSection(selector) - containerOffset),
        }))
    },
    findSectionWhichIsMostInScreen() {
      return this.getSectionOffsets()
        .sort((left, right) => left.center - right.center)[0].selector
    },
  },
  created() {
    if (!this.getContainerSelector()) {
      console.error('the SectionScrollMixin only works in a component that overwrites getContainerSelector()')
    }
    if (!this.getNavigationButtonsSelector()) {
      console.error('the SectionScrollMixin only works in a component that overwrites getNavigationButtonsSelector()')
    }
    if (!this.getSectionsSelector()) {
      console.error('the SectionScrollMixin only works in a component that overwrites getSectionsSelector()')
      return
    }
    if (!this.getRelatedButtonSelector(this.getSectionsSelector()[0])) {
      console.error('the SectionScrollMixin only works in a component that overwrites getRelatedButtonSelector()')
    }
  },
  mounted() {
    this.listenForActiveMenuButton()
  },
}

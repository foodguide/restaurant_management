import Vue from 'vue'

const state = {
  tasks: [],
}

const getters = {
  showProgress() {
    return state.tasks.length > 0
  },
}

const mutations = {
  addProgressTask(context, payload) {
    const index = state.tasks.findIndex(task => !!task.id && task.id === payload.id)
    if (index >= 0) {
      Vue.set(state.tasks, index, payload)
    } else {
      Vue.set(state.tasks, state.tasks.length, payload)
    }
  },
  removeProgressTask(context, payload) {
    const index = state.tasks.findIndex(task => !!task.id && task.id === payload.id)
    if (index >= 0) {
      state.tasks.splice(index, 1)
    }
  },
}

const actions = {
  showSpinnerTasks() {
    state.tasks.forEach((task) => {
      Vue.notify({
        title: task.title,
        text: task.text,
        type: 'info',
        duration: 3000,
      })
    })
  },
}

export default {
  state,
  getters,
  mutations,
  actions,
}

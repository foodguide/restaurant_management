import BaseRepository from './BaseRepository'

export default class WidgetRepository extends BaseRepository {
  get managedCollection() {
    return 'widget'
  }

  get documentAttributes() {
    return ['color', 'position', 'locationId', 'userId']
  }
}

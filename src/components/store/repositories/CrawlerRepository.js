import { database } from '../../api/Firebase'

export default class CrawlerRepository {
  constructor() {
    this.activeCrawlerTaskListener = {}
  }

  listenOnAllCrawlerTasks(onCallback) {
    const createListener = path => new Promise((resolve) => {
      database.ref(path).on('value', (snapshot) => {
        const values = []
        const val = snapshot.val()
        if (!val) return
        Reflect.ownKeys(val).forEach((key) => {
          values.push(val[key])
        })
        onCallback(values)
        resolve()
      })
    })
    return Promise.all([
      createListener('crawlerQueue/tasks'),
      createListener('yelpCrawlerQueue/tasks'),
    ])
  }

  async addCrawlerTaskListener(taskId, onCallback) {
    if (!this.activeCrawlerTaskListener[taskId]) {
      const path = CrawlerRepository.getRef(taskId)
      const ref = database.ref(path)
      ref.on('value', (snapshot) => {
        this.activeCrawlerTaskListener[taskId].onCallback(snapshot.val())
      })
      this.activeCrawlerTaskListener[taskId] = {
        ref,
        taskId,
        path,
        onCallback,
      }
    }
    return Promise.resolve()
  }

  removeCrawlerTaskListener(taskId) {
    if (!this.activeCrawlerTaskListener[taskId]) return
    this.activeCrawlerTaskListener[taskId].ref.off()
    delete this.activeCrawlerTaskListener[taskId]
  }

  static getRef(taskId) {
    return taskId.startsWith('yp_') ? `yelpCrawlerQueue/tasks/${taskId}` : `crawlerQueue/tasks/${taskId}`
  }

  removeAllCrawlerTaskListener() {
    Reflect.ownKeys(this.activeCrawlerTaskListener).forEach((taskId) => {
      this.removeCrawlerTaskListener(taskId)
    })
  }
}

describe('Test Review Page', () => {
  beforeEach(() => {
    indexedDB.deleteDatabase('firebaseLocalStorageDb')
    cy.login()
    cy.get('[data-cy=login-button]').eq(0).click()
    cy.get('[data-cy=location-card]').eq(0).click()
    cy.contains('Nein').click()
  })

  it('links to statistics page', () => {
    cy.get('[data-cy=statistics-button]').click()
    cy.url().should('include', '/statistics/e2GC6atRuvKpN58YazZF')
    cy.go('back')
  })

  it('links to external facebook page', () => {
    cy.get('[data-cy=all-button]').click()
    cy.get('#review-box').scrollTo(0, 300)
    cy.get('[data-cy=response-link]').should('be.visible')
    cy.go('back')
  })
})

import ExternalDependencyHelper from 'src/components/helper/ExternalDependencyHelper'
import externalDependencies from '../../../build/external-dependencies'

export const resolveChargebee = () => {
  const url = externalDependencies.find(dependency => dependency.module === 'chargebeeJs').entry
  return ExternalDependencyHelper.resolveGlobalVariable('Chargebee', url, document)
}

export default class ChargebeeApi {
  constructor(backend) {
    this._backend = backend
    resolveChargebee().then((Chargebee) => {
      this._chargebee = Chargebee.init({
        site: process.env.CHARGEBEE_SITE,
        enableGATracking: true,
      })
    })
    // this.chargebee.setPortalSession(() => this.backend.payment.portalSession())
  }

  get chargebee() {
    return this._chargebee
  }

  get backend() {
    return this._backend
  }

  set backend(newValue) {
    this._backend = newValue
  }

  checkoutNewPlan(planId, onStepChange = null) {
    return new Promise((resolve, reject) => {
      this.chargebee.openCheckout({
        hostedPage: () => this.backend.payment.newSubscriptionUrl(planId),
        loaded() {
          // console.log('opened checkout')
        },
        close() {
          reject('closed by user')
        },
        success(hostedPageId) {
          resolve(hostedPageId)
        },
        step(value) {
          if (typeof onStepChange === 'function') onStepChange(value)
        },
      })
    })
  }

  openPortal() {
    this.chargebee.createChargebeePortal().open({
      loaded: () => {
        console.log('showing portal')
      },
      close: () => {
        console.log('closing portal')
      },
      paymentSourceAdd: (status) => {
        console.log('payment source add')
        // console.log(status)
      },
      paymentSourceUpdate: (status) => {
        console.log('payment source update')
        // console.log(status)
      },
      paymentSourceRemove: (status) => {
        console.log('payment source remove')
        // console.log(status)
      },
    })
  }
}

#!/usr/bin/env bash
source ~/.nvm/nvm.sh

BRANCH=$(git symbolic-ref --short -q HEAD)
if [ $BRANCH != "master" ] ; then
    echo "Building from ${BRANCH} is not allowed. Checkout the master branch to build."
    exit
fi

if [ -z "$(git status --porcelain)" ]; then
  echo "git status is clear. continue..."
else
  echo "git status is not clear. aborting..."
  exit
fi

function defaultDeploy() {
    echo "This script will do the following:
    1. Start the cypress test runner and check if every test passes
    2. Run npm install to verify that all needed dependencies are installed in this branch
    3. Build the project for production
    4. Tag the master branch
    5. Commit and push all uncommited changes to the bitbucket repository
    6. Deploy the built dist folder to the default hosting target firebase (make sure you installed firebase with 'npm i firebase-tools -g')
    do you wish to continue?(yes/no)"
    read answr
    if [ "$answr" == "yes" ] ; then
        echo "Ok."
    else
        echo "aborting..."
        exit
    fi

    echo "Starting tests..."
    TESTRESULT=$(./node_modules/cypress/bin/cypress run --browser chrome)
    if [ -z "$(echo $TESTRESULT | grep 'All specs passed')" ]; then
        echo ${TESTRESULT}
        echo ""
        echo "Tests haven't passed. Aborting..."
        echo "Please run the tests on their own to get detailed information. Use:"
        echo "./node_modules/cypress/bin/cypress run --browser chrome"
        exit
    else
        echo "All tests have passed"
    fi

    echo "Latest git tag is:"
    git describe --abbrev=0 --tags
    echo "Please insert the tag for this new build."
    read tagText
    if [ -z "$tagText" ] ; then
	    echo "aborting..."
        exit
    fi

    npm install
    npm run build
    git add .
    git commit -a -m "built for production. $tagText"
    git tag "$tagText" -a -m "$tagText"
    git push origin "$tagText"
    firebase deploy --only hosting:app-respondo
    git push
}

function adminDeploy() {
    echo "This script will do the following:
    1. Run npm install to verify that all needed dependencies are installed in this branch
    2. Build the project for production
    3. Tag the master branch
    4. Commit and push all uncommited changes to the bitbucket repository
    5. Deploy the built dist folder to the hosting target 'Admin' firebase (make sure you installed firebase with 'npm i firebase-tools -g')
    do you wish to continue?(yes/no)"
    read answr
    if [ "$answr" == "yes" ] ; then
        echo "Ok."
    else
        echo "aborting..."
        exit
    fi

    echo "Latest git tag is:"
    git describe --abbrev=0 --tags
    echo "Please insert the tag for this new build."
    read tagText
    if [ -z "$tagText" ] ; then
	    echo "aborting..."
        exit
    fi

    npm install
    npm run build
    git add .
    git commit -a -m "built for production. $tagText"
    git tag "$tagText" -a -m "$tagText"
    git push origin "$tagText"
    firebase deploy --only hosting:admin-respondo
    git push
}

if [ "$1" == "admin" ]; then
    echo "Deploying admin sites...";
    adminDeploy;
else
    echo "Deploying default app sites...";
    defaultDeploy;
fi

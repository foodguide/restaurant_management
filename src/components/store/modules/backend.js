import Api from 'components/api/Api'

const actions = {
  isBackendAvailable(context) {
    Api.backend.status.check().then(() => {
      context.commit('setApplicationError', false)
    }).catch(() => {
      context.commit('setApplicationError', true)
    })
  },
  isAuthorizationValid(context) {
    const currentUserId = context.getters.getCurrentUserId
    Api.backend.status.authorization(currentUserId).then(() => {
      // All good with the authorization, do nothing
    }).catch(() => {
      context.dispatch('forceNewFirebaseToken')
    })
  },
}

export default {
  actions,
}

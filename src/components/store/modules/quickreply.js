import QuickReplyRepository from 'components/store/repositories/QuickReplyRepository'
import Vue from 'vue'

const state = {
  replies: [],
}

const getters = {
  getQuickReplies: state => state.replies,
}

const mutations = {
  fetchQuickReplies(state, payload) {
    state.replies = payload
  },
  addQuickReply(state, payload) {
    const index = state.replies.findIndex(reply => !!payload.id && reply.id === payload.id)
    if (index >= 0) {
      Vue.set(state.replies, index, payload)
    } else {
      Vue.set(state.replies, state.replies.length, payload)
    }
  },
}

const quickReplyRepository = new QuickReplyRepository()

const actions = {
  fetchQuickReplies(context) {
    return quickReplyRepository.findAllForCurrentUser().then((data) => {
      context.commit('fetchQuickReplies', data)
      return data
    })
  },
  writeQuickReplies(context, payload) {
    const promises = []
    payload.forEach((reply) => {
      promises.push(quickReplyRepository.writeQuickReply(reply).then(() => {
        context.commit('addQuickReply', reply)
      }))
    })
    return Promise.all(promises)
  },
}

export default {
  state,
  getters,
  actions,
  mutations,
}

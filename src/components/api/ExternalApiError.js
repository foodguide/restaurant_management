export default class ExternalApiError extends Error {
  constructor(userId, message = null, code = null) {
    super()
    this.message = message
    this.userId = userId
    this.name = 'ExternalApiError'
    this.stack = new Error().stack
  }
}

export const capitalize = ([first, ...rest]) => first.toUpperCase() + rest.join('')

// credits: https://stackoverflow.com/questions/7616461/generate-a-hash-from-string-in-javascript
export const hashCode = (text) => {
  let hash = 0; let i; let
    chr
  if (text.length === 0) return hash
  for (i = 0; i < text.length; i++) {
    chr = text.charCodeAt(i)
    hash = ((hash << 5) - hash) + chr
    hash |= 0 // Convert to 32bit integer
  }
  return hash
}

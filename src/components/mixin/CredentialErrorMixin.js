import { mapGetters } from 'vuex'
import { RepresentationStatus } from '../store/repositories/LocationRepository'
import { FacebookErrorCodes, FacebookErrorSubcodes } from '../api/Facebook'
import { AccessTokenStatus } from '../store/repositories/UserRepository'
import { HookListener } from '../store/modules/hooks'
import { capitalize } from '../helper/StringHelper'

export default {
  computed: {
    ...mapGetters({
      representations: 'getAllRepresentationForCurrentUser',
      unhealthyRepresentations: 'getUnhealthyRepresentations',
      facebookCredentials: 'getFacebookCredentials',
      unhealthyFacebookCredential: 'getCurrentUnhealthyFacebookCredential',
      unhealthyGoogleCredential: 'getCurrentUnhealthyGoogleCredential',
    }),
    facebookRepresentations() {
      return this.representations.filter(representation => representation.platform === 'facebook')
    },
    instagramRepresentations() {
      return this.representations.filter(representation => representation.platform === 'instagram')
    },
    unhealthyFacebookRepresentations() {
      const representations = [...this.facebookRepresentations, ...this.instagramRepresentations]
      return representations.filter(rep => !!this.unhealthyRepresentations.find(unhealthyRep => unhealthyRep.id === rep.id))
    },
    hasUnhealthyFacebookRepresentations() {
      return this.unhealthyFacebookRepresentations.length > 0
    },
    hasUnhealthyGoogleCredential() {
      return !!this.unhealthyGoogleCredential
    },
    errorGroup() {
      return 'reauthenticate-access'
    },
    facebookAccount() {
      if (!this.unhealthyFacebookCredential) return null
      return this.unhealthyFacebookCredential.account
    },
    googleAccount() {
      if (!this.unhealthyGoogleCredential) return null
      return this.unhealthyGoogleCredential.account
    },
  },
  data() {
    return {
      isShowingAuthenticationWarning: false,
    }
  },
  methods: {
    onError(error) {
      const status = this.getStatus(error)
      if (!status) return
      this.setCredentialStatus(status, error.platform).then(() => {
        this.showError(error)
      })
    },
    getStatus(error) {
      if (error.platform === 'facebook') {
        if (error.code === FacebookErrorCodes.ACCESSTOKEN_EXPIRED) {
          return RepresentationStatus.ACCESSTOKEN_EXPIRED
        }
        if (error.code === FacebookErrorCodes.CUSTOM_ERROR && error.newStatus) {
          return error.newStatus
        }
        if (error.code === FacebookErrorCodes.MISSING_PERMISSIONS && error.newStatus) {
          return error.newStatus
        }
        return null
      }
      if (error.platform === 'google') {
        return AccessTokenStatus.ACCESSTOKEN_REQUIRED
      }
      return null
    },
    getAccount(error) {
      switch (error.platform) {
        case 'facebook':
          return this.facebookAccount
        case 'google':
          return this.googleAccount
        default:
          return null
      }
    },
    showError(error) {
      if (this.isShowingAuthenticationWarning) {
        return
      }
      this.isShowingAuthenticationWarning = true
      const account = this.getAccount(error)
      this.$notify({
        group: this.errorGroup,
        title: capitalize(error.platform),
        text: this.getDescription(error),
        duration: -1,
        closeOnClick: false,
        type: 'warn',
        width: 400,
        max: 1,
        data: {
          error,
          account,
        },
      })
    },
    hideError() {
      this.$notify({
        group: this.errorGroup,
        clean: true,
      })
      this.isShowingAuthenticationWarning = false
    },
    setCredentialStatus(status, platform) {
      if (platform === 'facebook') return this.setFacebookCredentialsStatus(status)
      if (platform === 'google') {
        return this.$store.dispatch('updateCredential', {
          status,
          platform: 'google',
          credentialId: this.unhealthyGoogleCredential.id,
        })
      }
      return null
    },
    setFacebookCredentialsStatus(status) {
      const data = { status }
      return Promise.all(this.facebookRepresentations.map(representation => this.$store.dispatch('updateRepresentation', {
        locationId: representation.locationId,
        representation,
        data,
      }))).then(() => this.$store.dispatch('updateCredential', {
        status: AccessTokenStatus.ACCESSTOKEN_EXPIRED,
        platform: 'facebook',
        credentialId: this.unhealthyFacebookCredential.id,
      }))
    },
    getDescription(error) {
      if (error.platform === 'facebook') return this.getFacebookErrorDescription(error)
      if (error.platform === 'google') return 'error.reauthenticate.google'
      return ''
    },
    getFacebookErrorDescription(error) {
      if (error.code === FacebookErrorCodes.ACCESSTOKEN_EXPIRED) {
        if (error.error_subcode === FacebookErrorSubcodes.PASSWORD_CHANGED) {
          return 'error.reauthenticate.facebook.passwordChange'
        } if (error.error_subcode === FacebookErrorSubcodes.USER_CHECKPOINT) {
          return 'error.reauthenticate.facebook.invalidConnection'
        } if (error.error_subcode === FacebookErrorSubcodes.NOT_VERIFIED_USER) {
          return 'error.reauthenticate.facebook.unverified'
        } if (error.error_subcode === FacebookErrorSubcodes.APP_NOT_INSTALLED) {
          return 'error.reauthenticate.facebook.notInstalled'
        }
      } else if (error.code === FacebookErrorCodes.CUSTOM_ERROR) {
        if (error.userFriendlyMessage) return error.userFriendlyMessage
      }
      return 'error.reauthenticate.facebook.other'
    },
    reauthenticateWithFacebook() {
      return this.$store.dispatch('reauthenticateWithFacebook').then(() => {
        this.hideError()
      })
    },
    reauthenticateWithGoogle() {
      return this.$store.dispatch('reauthenticateWithGoogle').then(() => {
        this.hideError()
      })
    },
    reauthenticate({ platform }) {
      if (platform === 'facebook') return this.reauthenticateWithFacebook()
      if (platform === 'google') return this.reauthenticateWithGoogle()
      return null
    },
    registerFetchListener() {
      const listener = new HookListener({
        hookId: HookListener.Identifier.RepresentationFetch,
        handler: () => {
          this.$nextTick(() => {
            if (this.hasUnhealthyFacebookRepresentations) {
              const error = {
                code: FacebookErrorCodes.ACCESSTOKEN_EXPIRED,
                platform: 'facebook',
              }
              this.onError(error)
            } else if (this.hasUnhealthyGoogleCredential) {
              this.onError({ platform: 'google' })
            }
          })
        },
      })
      this.$store.commit('addHookListener', listener)
    },
    registerLogoutListener() {
      const listener = new HookListener({
        hookId: HookListener.Identifier.LoginStatus,
        handler: ({ payload }) => {
          if (!payload.isLoggedIn) this.hideError()
        },
      })
      this.$store.commit('addHookListener', listener)
    },
  },
  mounted() {
    this.registerFetchListener()
    this.registerLogoutListener()
  },
}

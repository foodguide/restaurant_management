describe('Test Location Page', () => {
  before(() => {
    cy.login()
    cy.get('[data-cy=login-button]').eq(0).click()
  })

  after(() => {
    cy.logout()
  })

  it('links to review page', () => {
    cy.get('[data-cy=location-card]').eq(0).click()
    cy.url().should('include', '/reviews/e2GC6atRuvKpN58YazZF')
  })

  it('links to representation page', () => {
    cy.get('[data-cy=location-action]').eq(0).click({ force: true })
    cy.url().should('include', '/connector/e2GC6atRuvKpN58YazZF')
  })

  it('opens sidebar and quick replies', () => {
    cy.get('[data-cy=navbar-button]').click()
    cy.get('[data-cy=navbar-item]').eq(1).click()
    cy.url().should('include', '/app/quick')
  })

  it('opens sidebar and statistics', () => {
    cy.get('[data-cy=navbar-button]').click()
    cy.get('[data-cy=navbar-item]').eq(2).click()
    cy.url().should('include', '/app/statistics')
  })

  it('opens sidebar and settings', () => {
    cy.get('[data-cy=navbar-button]').click()
    cy.get('[data-cy=navbar-item]').eq(3).click()
    cy.url().should('include', '/app/settings')
  })

  it('opens sidebar and imprint', () => {
    cy.get('[data-cy=navbar-button]').click()
    cy.get('[data-cy=navbar-item]').eq(4).click()
    cy.url().should('include', '/app/imprint')
  })
})

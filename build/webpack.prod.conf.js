var path = require('path')
var utils = require('./utils')
var webpack = require('webpack')
var config = require('../config')
var merge = require('webpack-merge')
var baseWebpackConfig = require('./webpack.base.conf')
var CopyWebpackPlugin = require('copy-webpack-plugin')
var HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
var OptimizeCSSPlugin = require('optimize-css-assets-webpack-plugin')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
var SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin')
var WebpackPwaManifest = require('webpack-pwa-manifest')
let WebappWebpackPlugin = require('webapp-webpack-plugin')
// let WebpackClearConsole = require("webpack-clear-console").WebpackClearConsole

var env = process.env.NODE_ENV === 'testing'
    ? require('../config/test.env')
    : config.build.env

var webpackConfig = merge(baseWebpackConfig, {
    mode: 'production',
    cache: false,
    module: {
        rules: utils.styleLoaders({
            sourceMap: config.build.productionSourceMap,
            extract: true
        })
    },
    // devtool: config.build.productionSourceMap ? '#source-map' : false,
    output: {
        path: config.build.assetsRoot,
        filename: utils.assetsPath('js/[name].[chunkhash].js'),
        chunkFilename: utils.assetsPath('js/[id].[chunkhash].js')
    },
    optimization: {
        minimize: true,
        runtimeChunk: 'single',
        splitChunks: {
            chunks: 'async',
            minSize: 30000,
            maxSize: 0,
            minChunks: 1,
            maxAsyncRequests: 5,
            maxInitialRequests: 3,
            automaticNameDelimiter: '~',
            name: true,
            cacheGroups: {
                vendors: {
                    test: /[\\/]node_modules[\\/]/,
                    name: 'vendors',
                    enforce: true,
                    chunks: 'all'
                },
                default: {
                    minChunks: 2,
                    priority: -20,
                    reuseExistingChunk: true
                }
            }
        },
        noEmitOnErrors: true,
        moduleIds: 'hashed'
    },
    plugins: [
        // new CleanWebpackPlugin(['dist']),
        // http://vuejs.github.io/vue-loader/en/workflow/production.html
        new webpack.DefinePlugin({
            'process.env': env
        }),
        new VueLoaderPlugin(),
        // extract css into its own file
        new MiniCssExtractPlugin({
            filename: utils.assetsPath('css/[name].[contenthash].css')
        }),

        // Compress extracted CSS. We are using this plugin so that possible
        // duplicated CSS from different components can be deduped.
        new OptimizeCSSPlugin({
            cssProcessorOptions: {
                safe: true
            }
        }),
        new WebappWebpackPlugin({
            logo: './src/assets/respondo_logo_only_square.svg',
            cache: true,
            inject: true
        }),
        // keep module.id stable when vender modules does not change
        new webpack.HashedModuleIdsPlugin(),
        // copy custom static assets
        new CopyWebpackPlugin([
            {
                from: path.resolve(__dirname, '../static'),
                to: config.build.assetsSubDirectory,
                ignore: ['.*']
            }
        ]),
        // new WebpackClearConsole()
    ]
})

if(config.build.productionGzip) {
    var CompressionWebpackPlugin = require('compression-webpack-plugin')
    webpackConfig.plugins.push(
        new CompressionWebpackPlugin()
    )
}

if(config.build.bundleAnalyzerReport) {
    var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
    webpackConfig.plugins.push(new BundleAnalyzerPlugin())
}

var appWebpackConfig = merge(webpackConfig, {
    plugins: [
        // generate dist index.html with correct asset hash for caching.
        // you can customize output by editing /index.html
        // see https://github.com/ampedandwired/html-webpack-plugin
        new HtmlWebpackPlugin({
            filename: process.env.NODE_ENV === 'testing'
                ? 'index.html'
                : config.build.index,
            template: 'index.html',
            inject: true,
            minify: {
                removeComments: true,
                collapseWhitespace: true,
                removeAttributeQuotes: true,
                minifyJS: true,
                minifyCSS: true
                // more options:
                // https://github.com/kangax/html-minifier#options-quick-reference
            },
            // necessary to consistently work with multiple chunks via CommonsChunkPlugin
            chunksSortMode: 'dependency'
        }),
        // service worker caching
        new SWPrecacheWebpackPlugin({
            cacheId: 'respondo',
            filename: 'service-worker.js',
            staticFileGlobs: ['dist/**/*.{js,css}', '/'],
            minify: true,
            stripPrefix: 'dist/',
            dontCacheBustUrlsMatching: /\.\w{6}\./
        }),
        new WebpackPwaManifest({
            name: 'Respondo ReviewManager',
            short_name: 'Respondo',
            description: 'All review platforms in one tool!',
            background_color: '#ffffff',
            crossorigin: null//can be null, use-credentials or anonymous
        }),
    ]
})

var adminWebpackConfig = merge(webpackConfig, {
    output: {
        path: config.build.assetsRoot + '_admin'
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'admin.html',
            inject: true,
            minify: {
                removeComments: true,
                collapseWhitespace: true,
                removeAttributeQuotes: true,
                minifyJS: true,
                minifyCSS: true
            },
            chunksSortMode: 'dependency'
        }),
    ]
})

adminWebpackConfig.entry = {
    admin: './src/admin.js'
}

module.exports = [
    appWebpackConfig, adminWebpackConfig
]

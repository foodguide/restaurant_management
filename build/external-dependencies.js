module.exports = [
    {
        module: 'gapi',
        entry: "https://apis.google.com/js/api.js",
        global: 'gapi'
    },
    {
        module: 'chargebeeJs',
        entry: "https://js.chargebee.com/v2/chargebee.js",
        global: 'Chargebee'
    }
]

/**
 * This guard is used for the admin area
 *
 * @param router
 * @param store
 */
export const addAdminGuard = (router, store) => {
  router.beforeEach((to, from, next) => {
    if (to.fullPath === '/login') next()
    else if (store.getters.isLoggedIn) next()
    else next({ path: '/login', replace: true })
  })
}

import axios from 'axios'

export default {
  getFile(path) {
    return axios.get(path)
      .then(result => result.data)
      .catch(error => Promise.reject(error))
  },
}

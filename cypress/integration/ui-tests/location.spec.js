import { locations, user, reviews } from '../../fixtures/testdata'

describe('Test Location Page with locations', () => {
  before(() => {
    cy.viewport(1024, 768)
    cy.customLogin(user, locations, reviews)
    cy.get('[data-cy=login-button]').eq(0).click()
  })

  it('hides empty state', () => {
    cy.get('[data-cy=empty-state]').should('not.exist')
  })

  it('has location cards and reviews', () => {
    cy.get('[data-cy=location-card]').eq(0).should('be.visible')
    cy.get('[data-cy=location-card-title]').eq(0).contains(locations[0].name)
    cy.get('[data-cy=reviews-text]').eq(0).contains('Gesamt: 2')
    cy.get('[data-cy=reviews-text]').eq(0).contains('Ungelesen: 1')
    cy.get('[data-cy=last-review-text]').eq(0).contains('Ein Traum von Pommen-Schranke!')
    cy.get('[data-cy=review-average-text]').eq(0).contains('5.0')

    cy.get('[data-cy=location-card]').eq(1).should('be.visible')
    cy.get('[data-cy=location-card-title]').eq(1).contains(locations[1].name)
    cy.get('[data-cy=location-action]').eq(1).contains('Klicke hier um Plattform hinzuzufügen')
    cy.get('[data-cy=reviews-text]').eq(1).contains('Gesamt: 0')
    cy.get('[data-cy=reviews-text]').eq(1).contains('Ungelesen: 0')
    cy.get('[data-cy=last-review-text]').eq(1).contains('Leider sind noch keine Bewertungen vorhanden')
    cy.get('[data-cy=review-average-text]').eq(1).contains('-')
  })
})

describe('Test Location Page without locations', () => {
  before(() => {
    cy.customLogin(user, [])
    cy.get('[data-cy=login-button]').eq(0).click()
  })

  it('shows empty state', () => {
    cy.get('[data-cy=empty-state]').should('be.visible')
  })

  it('has no location cards', () => {
    cy.get('[data-cy=location-card]').should('not.exist')
  })
})

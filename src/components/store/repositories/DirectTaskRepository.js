import BaseRepository from './BaseRepository'

class DirectTaskRepository extends BaseRepository {
  get managedCollection() {
    return 'directTask'
  }

  get documentAttributes() {
    return ['reviewId', 'locationName', 'userName', 'state', 'created', 'replied']
  }

  update(id, data) {
    const filtered = this.filterAllowedAttributes(data)
    return this.commitWrite(this.collection.doc(id).update(filtered))
  }

  async addDirectTaskListener(userId, onCallback) {
    this.collection.where('userId', '==', userId)
      .onSnapshot((querySnapshot) => {
        querySnapshot.docChanges().forEach((change) => {
          onCallback(change.doc.data())
        })
      })
  }
}

export { DirectTaskRepository as default }

﻿# Wie überzeuge ich Gäste mich online positiv zu bewerten?

Online Bewertungen gibt es heutzutage in jeder Branche. Vom Friseursalon bis hin zur Touristinformation können Kunden ihre (Un-) Zufriedenheit ausdrücken und potentielle Kunden nachlesen, ob dieses Geschäft ihnen wirklich zusagt oder sie sich für ein anderes entscheiden. Danach können sie selbst bewerten und mit ihren eigenen Erfahrungen andere Gäste beeinflussen. Gerade in der Gastronomie sind Rezensionen im Internet besonders präsent und wichtig.

Für dich als Gastronom ist das nützlich, da du beobachten kannst, was auf Plattformen wie TripAdvisor oder Facebook über dein Restaurant geschrieben wird. Aus diesen Bewertungen kannst du lernen, dein Restaurant verbessern und so attraktiver für neue Kunden werden.

Natürlich kommen nicht alle Gäste auf die Idee, direkt nach ihrem Besuch dir eine Bewertung zu hinterlassen. Keine Zeit, vergessen oder sogar noch nie eine Bewertung vorher verfasst? Dem kannst du ganz einfach entgegenwirken, indem du deine Gäste bittest, dir eine Rezension zu hinterlassen. So steigst du im Ranking, da du mehr und hoffentlich positive Bewertungen bekommst. Außerdem kannst du so aus dem Feedback deiner Gäste wesentlich effizienter analysieren und dich verbessern.

Am einfachsten ist es, wenn du eines der folgenden Tricks verwendest, um deinen Gästen das Bewerten so einfach wie möglich zu machen. Wenn es zu kompliziert wird, haben die Kunden wenig Lust, noch eine ausführliche Bewertung abzugeben.

-   **Individualisierte Fragebögen**
Erstelle einen simplen Fragebogen, der den Gästen hilft, sich an der Bewertung entlang zu hangeln. Frage zum Beispiel, wie sie den Service fanden, die Wartezeiten etc. Gerade wenn du explizit etwas verbessern oder verändern möchtest, hilft dir diese Methode, um herauszufinden, was deine Gäste wirklich wollen.

-   **Via E-Mail zum Bewerten einladen**
Wenn Gäste ihre Reservierungen via E-Mail machen, kannst du es ausnutzen, ihre E-Mailadresse zu haben und sie direkt nach ihrem Besuch freundlich auffordern, dir eine Bewertung zu hinterlassen.

-   **Bewertungseinladung per SMS**
Du hast zwar nicht die E-Mail-Adresse, aber sie haben über ihre Mobilfunknummer reserviert? Dann schicke ihnen doch per SMS oder WhatsApp die Bitte, dich zu bewerten!

-   **WLAN einrichten**
Wenn du WLAN in deinem Restaurant hast, können deine Gäste direkt vor Ort bewerten und vergessen es nicht, auf dem Weg nach Hause! Dafür benötigen deine Gäste nur ihr Smartphone.

-   **Bewerten vor Ort per Tablet**
Haben deine Gäste kein Smartphone dabei? Dann könntest du ihnen ein Tablet anbieten, um ihre Bewertung noch während ihres Aufenthaltes zu schreiben. Du kannst somit auch ein persönliches Gespräch mit deinen Gästen führen, in dem mögliche Kritikpunkte angesprochen und geklärt werden können.

- **Bewerten via QR Code**
Wenn weder E-Mail, noch Smartphone oder WLAN verfügbar sind, kannst du deine Gäste mithilfe eines QR Codes zum Bewertungsformular führen! Du kannst den Code ganz einfach auf deine Karte drucken, einen kleinen Aufsteller auf den Tisch stellen oder auf die Rechnung drucken. Deine Gäste scannen den Code und landen direkt bei der Bewertung.

**Fazit**
Wie du siehst, gibt es zahlreiche Möglichkeiten, um deine Gäste die Möglichkeit zu bieten, dich zu bewerten. Es ist deine Entscheidung, welche Methode für dich und dein Restaurant am sinnvollsten ist und womit du die besten Bewertungen erzielen kannst.

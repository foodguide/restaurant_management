import BaseRepository from './BaseRepository'

class ContactRequestRepository extends BaseRepository {
  get managedCollection() {
    return 'contactRequest'
  }

  get documentAttributes() {
    return [
      /**
             * normal | business
             */
      'type',
      'lastName',
      'firstName',
      'email',
      'message',
      'phone',
      'date',
      'time',
      'timestamp',
    ]
  }

  writeContactRequest(data) {
    const filtered = this.filterAllowedAttributes({
      type: 'normal',
      firstName: data.firstName,
      lastName: data.lastName,
      email: data.email,
      message: data.message,
      timestamp: new Date().toLocaleString(),
    })
    return this.commitWrite(this.collection.add(filtered))
  }

  writeBusinessContactRequest(data) {
    const filtered = this.filterAllowedAttributes({
      type: 'business',
      firstName: data.firstName,
      lastName: data.lastName,
      email: data.email ? data.email : '',
      date: data.date ? data.date : '',
      time: data.time ? data.time : '',
      phone: data.phone,
      timestamp: new Date().toLocaleString(),
    })
    return this.commitWrite(this.collection.add(filtered))
  }
}

export { ContactRequestRepository as default }

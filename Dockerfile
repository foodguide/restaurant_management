FROM node:9
RUN apt-get update \
      && apt-get -y install \
        git \
        wget \
        vim \
        zsh
RUN wget https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh -O - | zsh || true
# Create app directory
RUN  npm install --save-dev @vue/test-utils
WORKDIR ./usr/app
# the volume is added to the container at the end of this process
# so we have to copy the package.json manually at first to run then
# the installation of the dependencies
COPY package.json ./
#Install app dependencies
RUN npm install --force
# start the dev server
CMD webpack-dev-server --mode development --port 9090 --progress --hot --open --config ./build/webpack.base.conf.js

import UserApi from './User.js'
import ResponseApi from './Response.js'
import GoogleCustomSearch from './GoogleCustomSearch'
import { facebookApi } from './Facebook'
import GoogleAuth from './GoogleApi/GoogleAuthorization'
import Backend from './Backend'
import Chargebee from './Chargebee'
import FileLoader from './FileLoader';

const backend = new Backend()
UserApi.addIdTokenObserver(backend)
const chargebee = new Chargebee(backend)

export default {
  user: UserApi,
  response: ResponseApi,
  facebook: facebookApi,
  google: {
    auth: new GoogleAuth(),
    search: GoogleCustomSearch,
  },
  fileLoader: FileLoader,
  backend,
  chargebee,
}

import { Firebase } from '../Firebase'
import { facebook } from '../../helper/Scopes'

const defaultParameters = {
  display: 'popup',
}

const provider = new Firebase.auth.FacebookAuthProvider()
facebook.asList().forEach(scope => provider.addScope(scope))
provider.setCustomParameters(defaultParameters)

export default provider

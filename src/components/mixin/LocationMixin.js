import { mapGetters } from 'vuex'

export default {
  computed: {
    ...mapGetters([
      'getLocationsForCurrentUser',
      'getAllPlatforms',
    ]),
    platforms() {
      return this.getAllPlatforms
    },
    getRepresentationsForLocation() {
      return this.location && this.location.representations ? this.location.representations : []
    },
    hasLocationPresentations() {
      return this.getRepresentationsForLocation.length > 0
    },
    getPlatformNamesForRepresentations() {
      return this.getRepresentationsForLocation.map(representation => representation.platform.toLowerCase())
    },
  },
  methods: {
    isPlatformConnected(platform) {
      return this.getPlatformNamesForRepresentations.includes(platform.name.toLowerCase())
    },
    getPlatformForRepresentation(representation) {
      return this.getAllPlatforms.find(platform => platform.name === representation.platform)
    },
  },
}

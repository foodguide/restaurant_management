import axios from 'axios'

export default {
  search(text) {
    const key = process.env.GOOGLE_SEARCH_API_KEY
    const cx = process.env.GOOGLE_SEARCH_SCOPE_ID
    return axios.get(this.url, {
      params: {
        q: text,
        cx,
        key,
      },
      baseURL: 'https://www.googleapis.com/customsearch/v1',
    })
  },
}

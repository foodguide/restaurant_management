import Method from './HTTPMethod'

export default (obj) => {
  const promise = new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest()
    xhr.responseType = obj.responseType !== undefined ? obj.responseType : 'json'
    const method = obj.method ? obj.method : Method.get
    xhr.open(method, obj.url)
    if (obj.headers) {
      Object.keys(obj.headers).forEach((key) => {
        xhr.setRequestHeader(key, obj.headers[key])
      })
    }
    xhr.onload = () => {
      if (xhr.status >= 200 && xhr.status < 300) {
        resolve(xhr.response)
      } else {
        reject(xhr.statusText)
      }
    }
    xhr.onerror = () => reject(xhr.statusText)
    xhr.send(JSON.stringify(obj.body))
  })
  return promise
}

describe('Test Connector Page for location without connections', () => {
  before(() => {
    cy.login()
    cy.get('[data-cy=login-button]').eq(0).click()
    cy.get('[data-cy=location-action]').eq(0).click({ force: true })
  })

  it('offers correct platforms', () => {
    cy.get('[data-cy=platform-item]').should('have.length', 6)
    cy.get('[data-cy=platform-item]').eq(0).contains('FACEBOOK')
    cy.get('[data-cy=platform-item]').eq(1).contains('INSTAGRAM')
    cy.get('[data-cy=platform-item]').eq(2).contains('GOOGLE')
    cy.get('[data-cy=platform-item]').eq(3).contains('YELP')
    cy.get('[data-cy=platform-item]').eq(4).contains('TRIPADVISOR')
    cy.get('[data-cy=platform-item]').eq(5).contains('OPENTABLE')
  })

  it('opens crawler dialog on click', () => {
    cy.get('[data-cy=platform-item]').eq(3).click()
    cy.get('[data-cy=crawler-dialog]').should('be.visible')
  })

  it('hides back button', () => {
    cy.get('[data-cy=finish-button]').should('be.disabled')
  })
})

describe('Test Connector Page for location with one connection', () => {
  before(() => {
    cy.login()
    cy.get('[data-cy=login-button]').eq(0).click()
    cy.get('[data-cy=location-action]').eq(0).click({ force: true })
  })

  it('offers correct platforms', () => {
    cy.get('[data-cy=platform-item]').should('have.length', 6)
    cy.get('[data-cy=platform-item]').eq(0).contains('FACEBOOK')
    cy.get('[data-cy=platform-item]').eq(1).contains('INSTAGRAM')
    cy.get('[data-cy=platform-item]').eq(2).contains('GOOGLE')
    cy.get('[data-cy=platform-item]').eq(3).contains('YELP')
    cy.get('[data-cy=platform-item]').eq(4).contains('TRIPADVISOR')
    cy.get('[data-cy=platform-item]').eq(5).contains('OPENTABLE')
  })

  it('shows Facebook as connected connection', () => {
    // check for checkmark
    cy.get('[data-cy=success-icon]').should('have.length', 6)
    cy.get('[data-cy=success-icon]').eq(0).should('be.visible')
  })

  it('shows back button', () => {
    cy.get('[data-cy=cancel-button]').should('be.visible')
    cy.get('[data-cy=finish-button]').should('be.visible')
  })
})

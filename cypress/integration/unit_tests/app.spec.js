import { spec } from 'cypress/support/mountVue'
import App from 'src/App'
import routes from 'cypress/support/testroutes'
import storeOptions from 'src/components/store/index'
import { HookId, HookListener } from '@/components/store/modules/hooks'

storeOptions.modules.user.mutations.setToUnknown = (state) => {
  state.currentUser.isUnknown = true
}

describe('App.vue', () => {
  let wrapper; let app; let
    unitTest
  before((done) => {
    cy.unitTest(App, routes, storeOptions).then((result) => {
      unitTest = result
      wrapper = unitTest.wrapper
      app = unitTest.mountedComponent
      done()
    })
  })

  afterEach((done) => {
    wrapper.vm.$store.state.user.currentUser.isUnknown = true
    wrapper.vm.$store.dispatch('logout').then(() => {
      wrapper.vm.$store.commit('setToUnknown')
      wrapper.vm.$nextTick(() => done())
    })
  })

  const showsLoadingScreen = done => cy.get('.loading-container').get('.ball').then(() => done())

  it('has setup correctly', (done) => {
    expect(wrapper.vm.$route.path).to.eq('/start')
    showsLoadingScreen(done)
  })

  it('still shows loading before checked subscription status after login check', (done) => {
    wrapper.vm.$store.commit('setLoginStatus', true)
    showsLoadingScreen(done)
  })

  it('still shows loading before checked login status but already checked subscription status', (done) => {
    wrapper.vm.$store.commit('setActiveSubscriptions', false)
    wrapper.vm.$store.commit('setHasRequestedSubscriptionStatus')
    showsLoadingScreen(done)
  })

  it('routes to payment when user has no subscription', () => {
    wrapper.vm.$store.commit('setLoginStatus', true)
    wrapper.vm.$store.commit('setHasRequestedSubscriptionStatus')
    wrapper.vm.$store.commit('setActiveSubscriptions', false)
    cy.get('[data-cy=login-button]').click().then(() => {
      expect(wrapper.vm.$route.path.startsWith('/test/')).to.be.true
    })
  })

  it('routes to app page when user is logged in and has an active subscription', () => {
    wrapper.vm.$store.commit('login')
    wrapper.vm.$store.commit('setLoginStatus', true)
    wrapper.vm.$store.commit('setHasRequestedSubscriptionStatus')
    wrapper.vm.$store.commit('setActiveSubscriptions', true)
    const listener = new HookListener({
      id: HookListener.Identifier.SubscriptionStatus,
      handler: (event) => {
        expect(event.payload.activeSubscriptionsResult).to.be.true
        expect(wrapper.vm.$route.path).to.eq('/app/locations')
        wrapper.vm.$nextTick(() => {
          expect(wrapper.html()).to.contain('class="location-layout"')
        })
      },
    })

    wrapper.vm.$store.dispatch('onHookEvent', { hookId: HookId.LoginStatus, payload: { isLoggedIn: true } })
    wrapper.vm.$store.dispatch('onHookEvent', { hookId: HookId.SubscriptionStatus, payload: { activeSubscriptionsResult: true } })
  })

  it('routes to landingpage after logout', (done) => {
    // at first we set the user to logged in
    wrapper.vm.$store.commit('setLoginStatus', true)
    wrapper.vm.$store.commit('setHasRequestedSubscriptionStatus')
    wrapper.vm.$store.commit('setActiveSubscriptions', true)
    cy.get('[data-cy=login-button]').click().then(() => {
      cy.get('.location-layout').then(() => {
        app.onLogout()
        wrapper.vm.$nextTick(() => {
          // expect(wrapper.vm.$route.path.startsWith('/start')).to.be.true
          expect(wrapper.vm.$route.path).to.eq('/start')
          done()
        })
      })
    })
  })

  it('shows errors and hides them when confirmed', (done) => {
    const message = 'This is an error'
    const defaultErrorTitle = 'Fehler'

    const error = {
      active: true,
      message,
    }
    wrapper.vm.$store.commit('updateError', error)
    spec.get('.md-dialog-title').then((element) => {
      spec.get('.md-dialog-content').then((content) => {
        expect(element).to.exist
        expect(element).to.be.visible
        expect(Cypress.$(element).text()).to.contain(defaultErrorTitle)
        expect(content).to.exist
        expect(Cypress.$(content).text()).to.eq(message)
        app.onErrorConfirmed()
        window.setTimeout(() => {
          spec.get('.md-dialog-title', 500).then((element) => {
            expect(element).to.be.null
            done()
          })
        }, 300)
      })
    })
  })
})

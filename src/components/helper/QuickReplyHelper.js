export const findFittingReply = (replies, review) => {
  const locationReplies = replies.filter(reply => reply.locationId === review.locationId)
  const ratingReplies = locationReplies.filter(reply => reply.options.find(option => option.type === 'rating'))

  switch (review.platform) {
    case 'instagram':
      return ratingReplies.find(reply => reply.options.find(option => option.value === 0))
    case 'facebook':
      if (this.review.rating) {
        return ratingReplies.find(reply => reply.options.find(option => option.value === review.rating))
      }
      return ratingReplies.find(reply => reply.options.find(option => option.value === review.ratingType))

    default:
      return ratingReplies.find(reply => reply.options.find(option => option.value === review.rating))
  }
}

export default {
  findFittingReply,
}

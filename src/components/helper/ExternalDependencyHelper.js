export default {
  importFromUrl(url, doc) {
    return new Promise((resolve, reject) => {
      const script = doc.createElement('script')
      script.type = 'text/javascript'
      script.src = url
      script.addEventListener('load', () => resolve(script), false)
      script.addEventListener('error', () => reject(script), false)
      doc.body.appendChild(script)
    })
  },
  resolveGlobalVariable(varName, url, doc) {
    return new Promise((resolve, reject) => {
      if (window[varName]) {
        resolve(window[varName])
      } else {
        this.importFromUrl(url, doc).then(() => {
          if (window[varName]) {
            resolve(window[varName])
          } else {
            reject(new Error(`Didn't found external lib: ${url}`))
          }
        })
      }
    })
  },
}

describe('Test Login', () => {
  beforeEach(() => {
    cy.viewport(1024, 768)
    cy.visit('/')
    cy.window().then((win) => {
      win.__store__.commit('setLoginStatus', false)
    })
    cy.get('[data-cy=login-button]').click()
  })

  it('has all elements', () => {
    cy.get('[data-cy=login-btn]').should('be.visible')
    cy.get('[data-cy=password-button]').should('be.visible')
    cy.get('[data-cy=facebook-button]').should('be.visible')
    cy.get('[data-cy=email-input]').should('be.visible')
    cy.get('[data-cy=email-error]').not('be.visible')
    cy.get('[data-cy=password-input]').should('be.visible')
    cy.get('[data-cy=password-error]').not('be.visible')
  })

  it('handles correct inputs correctly', () => {
    cy.get('[data-cy=email-input]').type('test@test.de')
    cy.get('[data-cy=email-error]').not('be.visible')
    cy.get('[data-cy=password-input]').type('11111111')
    cy.get('[data-cy=password-error]').not('be.visible')
  })

  it('handles wrong inputs correctly', () => {
    cy.get('[data-cy=email-input]').type('test@test')
    cy.get('[data-cy=email-error]').should('be.visible')
  })

  it('handles empty inputs correctly', () => {
    cy.get('[data-cy=login-btn]').click()
    cy.get('[data-cy=email-error]').should('be.visible')
    cy.get('[data-cy=password-error]').should('be.visible')
  })

  it('opens forget password dialog correctly', () => {
    cy.get('[data-cy=password-button]').click()
    cy.get('[data-cy=password-dialog]').should('be.visible')
  })
})

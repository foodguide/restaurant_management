const state = {
  headline: '',
  applicationError: false,
}

const getters = {
  getHeadline() {
    return state.headline
  },
  hasApplicationError() {
    return state.applicationError
  },
}

const mutations = {
  setHeadline(context, payload) {
    if (payload.text) state.headline = payload.text
  },
  setApplicationError(context, payload) {
    state.applicationError = payload
  },
}

export default {
  state,
  getters,
  mutations,
}

import IconResolver from 'src/components/helper/IconResolver'
import facebookLogo from '@/assets/platforms/facebook-logo.svg'


describe('IconResolver.js', () => {
  const name = 'facebook'

  it('should return the # value of a color for a given platform name', () => {
    expect(IconResolver.getPlatformColorByName(name)).to.eq('#4267b2')
  })

  it('should return the logo as a svg file for a given platform name', () => {
    const icon = IconResolver.getPlatformIconByName(name)
    expect(icon).to.exist
    expect(icon).to.eq(facebookLogo)
  })

  it('should return the logo as a svg file for a given platform', () => {
    const platform = {
      id: 1,
      name,
    }
    const icon = IconResolver.getPlatformIcon(platform)
    expect(icon).to.exist
    expect(icon).to.eq(facebookLogo)
  })
})

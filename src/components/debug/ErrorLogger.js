import flat from 'flat'
import getUserFriendlyMessage from 'components/helper/ErrorHelper'
import Slack from '../api/Slack'
import store from '../store'
import EventRepository from '../store/repositories/EventRepository'
import { EventName, EventType } from '../store/modules/event'

const eventRepository = new EventRepository()

const VueErrorLogger = (err, vm, info) => {
  if (process.env.NODE_ENV === 'testing' || process.env.NODE_ENV === 'development') {
    throw err
  }
  const resolvedVm = resolveVm(vm)
  const data = createErrorAttachement({
    userId: store.state.user.currentUser.id,
    email: store.state.user.currentUser.nick,
    source: info,
    message: err.message,
    browser: navigator.appVersion,
    url: location.hash,
    locationId: resolvedVm ? resolvedVm.locationId : null,
    locationName: resolvedVm ? resolvedVm.locationName : null,
    componentName: resolvedVm ? resolvedVm.componentName : null,
    representationId: resolvedVm ? resolvedVm.representationId : null,
    representationPlatform: resolvedVm ? resolvedVm.representationPlatform : null,
    reviewId: resolvedVm ? resolvedVm.reviewId : null,
    stacktrace: err.stack,
  })
  Slack.logError([data])

  eventRepository.addEvent({
    name: EventName.Error,
    type: EventType.Error,
    data,
  })

  return true
}

const VuexStoreLogger = (action, state) => {
  if (action.type === 'showError') {
    if (process.env.NODE_ENV === 'testing' || process.env.NODE_ENV === 'development') {
      return
    }
    const err = action.payload
    const data = createErrorAttachement({
      userId: state.user.currentUser.id,
      email: state.user.currentUser.nick,
      source: 'Vuex - showError',
      message: err.message,
      browser: navigator.appVersion,
      url: location.hash,
      userFriendlyMessage: getUserFriendlyMessage(err),
      payload: JSON.stringify(err),
      stacktrace: err.stack,
    })
    Slack.logError([data])

    eventRepository.addEvent({
      name: EventName.Error,
      type: EventType.Error,
      data,
    })
  }
}

const PromiseRejectionLogger = {

  handleRejection: (event) => {
    if (!(process.env.NODE_ENV === 'testing' || process.env.NODE_ENV === 'development')) {
      const target = event.target || event.srcElement
      const locationHash = target.location.hash
      const browser = target.clientInformation.appVersion
      const attachment = {
        userId: store.state ? store.state.user.currentUser.id : null,
        email: store.state ? store.state.user.currentUser.nick : null,
        source: 'Unhandled Rejection',
        url: locationHash,
        browser,
        reason: typeof event.reason === 'string' ? event.reason : null,
        detail: JSON.stringify(event.detail),
        event: JSON.stringify(event),
      }

      if (typeof event.reason === 'object') {
        const flattenedReason = flat(event.reason)
        for (const key in flattenedReason) {
          attachment[key] = flattenedReason[key]
        }
      }

      const data = createErrorAttachement(attachment)

      Slack.logError([data])

      eventRepository.addEvent({
        name: EventName.Error,
        type: EventType.Error,
        data,
      })
    }
  },
}

function resolveVm(vm) {
  let componentName


  let locationId


  let locationName


  let representationId


  let representationPlatform


  let reviewId = null

  if (!vm || !vm.$options) {
    return null
  }
  const options = vm.$options
  if (options.name) componentName = options.name
  if (options.propsData) {
    const { propsData } = options

    // LOCATION DATA
    if (propsData.location) {
      if (propsData.location.id) locationId = propsData.location.id
      if (propsData.location.name) locationName = propsData.location.name
    }
    // REPRESENTATION DATA
    if (propsData.representation) {
      if (propsData.representation.id) representationId = propsData.representation.id
      if (propsData.representation.platform) representationPlatform = propsData.representation.platform
    }
    // REVIEW DATA
    if (propsData.review) {
      if (propsData.review.id) reviewId = propsData.review.id
    }
  }

  return {
    componentName,
    locationId,
    locationName,
    representationId,
    representationPlatform,
    reviewId,
  }
}

function createErrorAttachement(data) {
  const fields = []
  const longFields = ['message', 'stacktrace', 'browser', 'url']

  for (const key in data) {
    if (data[key] && data[key] !== null) {
      fields.push({
        title: key,
        value: data[key],
        short: !longFields.includes(key),
      })
    }
  }

  return {
    fallback: `Error occured in ${data.source}, ${data.lineNumber}, message: ${data.message}`,
    text: 'An *ERROR* occured! \n',
    color: '#FF0800',
    fields,
  }
}

export default {
  VueErrorLogger,
  RejectionLogger: PromiseRejectionLogger,
  VuexStoreLogger,
}

export const installErrorLogging = (Vue, store) => {
  Vue.config.errorHandler = VueErrorLogger
  window.addEventListener('unhandledrejection', PromiseRejectionLogger.handleRejection)
  store.subscribeAction(VuexStoreLogger)
}

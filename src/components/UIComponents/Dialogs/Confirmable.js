export default class Confirmable {
  constructor(executor) {
    this._executor = executor
  }

  onConfirm(onConfirm, onCancel = null) {
    if (onConfirm) this._executor.onConfirm = onConfirm
    if (onCancel) this._executor.onCancel = onCancel
  }

  onCancel(onCancel) {
    if (onCancel) this._executor.onCancel = onCancel
  }
}

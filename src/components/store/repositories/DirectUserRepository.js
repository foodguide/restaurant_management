import BaseRepository from './BaseRepository'

class DirectUserRepository extends BaseRepository {
  get managedCollection() {
    return 'directUser'
  }
}

export { DirectUserRepository as default }

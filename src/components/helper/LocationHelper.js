export const isEqualLocation = (loc1, loc2) => {
  if (!loc1 && !loc2) return true
  if (!loc1) return false
  if (!loc2) return false
  if (loc1.id !== loc2.id) return false
  if (loc1.name !== loc2.name) return false
  if (loc1.userId !== loc2.userId) return false
  if (typeof loc1.representations !== typeof loc2.representations) return false
  if (!loc1.representations && !loc2.representations) return true
  if (loc1.representations.length !== loc2.representations.length) return false
  return loc1.representations.filter(rep1 => !!loc2.representations.find(rep2 => rep1.platform === rep2.platform)).length === loc1.representations.length
}

export default {
  isEqualLocation,
}

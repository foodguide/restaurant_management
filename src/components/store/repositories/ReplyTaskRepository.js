import BaseRepository from './BaseRepository'
import { database } from '../../api/Firebase'

export default class ReplyTaskRepository extends BaseRepository {
  listenOnReplyTasks(onUpdated, onRemoved) {
    const refs = ['apiReplyQueue/tasks', 'crawlerReplyQueue/tasks']
    refs.forEach((ref) => {
      const query = database.ref(ref).orderByChild('userId').equalTo(this.getCurrentUserUid())
      query.on('value', (snapshot) => {
        onUpdated(snapshot.val())
      })
      query.on('child_removed', (oldChildSnapshot) => {
        onRemoved(oldChildSnapshot.val())
      })
    })
  }
}

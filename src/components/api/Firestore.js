import firebase from './Firebase'

const firestore = firebase.firestore()
const settings = {}
firestore.settings(settings)
export default firestore

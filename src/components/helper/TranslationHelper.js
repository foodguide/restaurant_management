const isTranslationSnippet = (text) => {
  if (!text || text.length === 0) return false
  return text.match('([a-z]+)((\\.[a-z]+)+)')
}

const currentLocale = () => {
  if (!window.navigator) return 'en'
  if (window.Cypress) return 'de'

  return (window.navigator.userLanguage || window.navigator.language).split('-')[0]
}

export default { isTranslationSnippet, currentLocale }

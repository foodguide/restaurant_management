import AdminDashboard from '../components/Admin/Layout/AdminDashboard.vue'

// GeneralViews
import NotFound from '../components/Admin/Views/NotFound.vue'

// Main pages
const User = () => import('../components/Admin/Views/User.vue')
const UserDetail = () => import('../components/Admin/Views/UserDetail.vue')
const LocationDetail = () => import('../components/Admin/Views/LocationDetail.vue')
const Status = () => import('../components/Admin/Views/Status.vue')
const Login = () => import('../components/Admin/Views/Login.vue')
const CrawlerTaskDetail = () => import('../components/Admin/Views/CrawlerTaskDetail.vue')
const ContactRequestDetail = () => import('../components/Admin/Views/ContactRequestDetail.vue')
const AdminActions = () => import('../components/Admin/Views/AdminActions.vue')
const UpdateReviews = () => import('../components/Admin/Views/UpdateReviews.vue')
const GenericRequest = () => import('../components/Admin/Views/GenericRequest.vue')
const ResyncRepresentation = () => import('../components/Admin/Views/ResyncRepresentation.vue')
const DeleteReviews = () => import('../components/Admin/Views/DeleteReviews.vue')
const RepresentationReviews = () => import('../components/Admin/Views/RepresentationReviews.vue')
const SendNewsletter = () => import('../components/Admin/Views/SendNewsletter.vue')
const Direct = () => import('../components/Admin/Views/Direct.vue')

export const createRoutes = (User, Status) => [
  {
    path: '/',
    component: AdminDashboard,
    redirect: '/login',
    children: [
      {
        path: 'user',
        component: User,
      },
      {
        path: 'user/:id',
        component: UserDetail,
      },
      {
        path: 'location/:id',
        component: LocationDetail,
      },
      {
        path: 'status',
        component: Status,
      },
      {
        path: 'direct',
        component: Direct,
      },
      {
        path: 'crawler-task/:id',
        component: CrawlerTaskDetail,
      },
      {
        path: 'contact-request/:id',
        component: ContactRequestDetail,
      },
      {
        path: 'review/:id',
        component: RepresentationReviews,
      },
      {
        path: 'actions',
        component: AdminActions,
        children: [
          {
            path: 'generic-send',
            component: GenericRequest,
          },
          {
            path: 'update-reviews',
            component: UpdateReviews,
          },
          {
            path: 'resyncing-representation',
            component: ResyncRepresentation,
          },
          {
            path: 'delete-reviews',
            component: DeleteReviews,
          },
          {
            path: 'send-newsletter',
            component: SendNewsletter,
          },
        ],
      },
    ],
  },
  {
    path: '/login',
    component: Login,
  },
  { path: '*', component: NotFound },
]

export default createRoutes(User, Status)

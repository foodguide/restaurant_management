﻿# So verbesserst du dein Restaurant Management in 10 Tagen

Es ist ein harter Job, alle Aufgaben zu erfüllen, die im täglichen Leben eines Restaurants anfallen. Diese Tipps können dein Restaurant Management vereinfachen und legen die Aufmerksamkeit auf die wirklich wichtigen Dinge.

**-   Lerne zu delegieren**    

Du kannst nicht alles selber machen. Lerne kleinere Aufgaben abzugeben, damit du dich auf wichtigere Vorgänge konzentrieren kannst und nichts vernachlässigen musst. Suche dabei Mitarbeiter aus, bei denen du weißt, dass sie dich nicht im Stich lassen. Das bereitet sie nicht nur auf spätere Verantwortlichkeiten vor, sondern lässt sie auch deine Anerkennung spüren.

-   **Rede mit den Gästen**
 
Halte dich bei den Gästen auf und frage sie nicht nur, ob es geschmeckt hat, sondern auch, wie das sonstige Erlebnis war. Sehr aufschlussreich ist die Frage, was sie ihren Freunden über den Besuch erzählen würden. So fühlen sich die Gäste nicht nur umsorgt und ernst genommen, das Feedback ist außerdem sehr nützlich für die weitere Entwicklung des Restaurants. Falls du auf der Fläche selber nicht genug Feedback einholen kannst nutze die digitalen Kanäle wie Bewertungsplattformen für dich, in dem du das Feedback analysiert und geschickt darauf eingehst. Respondo hilft dir gern dabei den Überblick zu behalten und Zeit zu sparen.

-   **Das Servicepersonal muss die Gerichte und Getränke kennen**
    
Diese Details machen den Besuch in einem sehr guten Restaurant aus. Die Kellner können nicht nur beschreiben, wie die Speisen schmecken, sondern auch welcher Wein dazu passt und warum. Auch bei Aushilfspersonal, das nur manchmal da ist und meist keine Ausbildung in der Gastronomie hat, sollte dieses Wissen Voraussetzung sein. Das ist nicht nur schön für den Gast, sondern notwendig, beispielsweise bei Allergien von Besuchern.

-   **Nutze Social Media**
    
Es überlebenswichtig für Restaurants Werbung zu machen - nur so erfahren die Leute von der eigenen Existenz. Online gibt es mittlerweile viele günstige Wege auf sich aufmerksam zu machen. Profile auf Social Media sind kostenlos und sehr effektiv, denn tolle Food-Fotos werden mit Vorliebe geteilt und geliked. Vor allem die junge Zielgruppe erreicht man nur über eine starke, moderne Online-Präsenz. Viele Gastronomen zählen auch die Bewertungsplattformen als Social Media. Entscheide selber unter welchem Überbegriff die diese Kanäle behandeln willst und briefe entsprechend dein Personal.

-   **Beobachte deine Kellner**
    
Das Personal ist ausschlaggebend für das Erlebnis des Gastes, deswegen ist es sehr wichtig, die Kellner genau zu beobachten. Ist immer ein Lächeln im Gesicht, wird sich vorgestellt und werden Empfehlungen ausgesprochen? Es ist gut, die Mitarbeiter nach solchen Beobachtungen zu loben, damit sich die Vorgänge einprägen. Kritik wird am besten nett verpackt in kleine Hinweise, damit der Abend mit der gleichen Motivation weitergehen kann.

-   **Lächeln nicht vergessen**
    
Die Stimmung des Managers überträgt sich auf das Servicepersonal und damit auf die Gäste. Also ist es Pflicht, als Führungsperson mit gut gelaunten Beispiel voran zu gehen. Liebevolle Strenge ist der richtige Weg, damit Stimmung und Konzentration nicht flöten gehen.

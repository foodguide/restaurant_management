import { spec } from 'cypress/support/mountVue'
import LoadingDialog from 'src/components/GeneralViews/LoadingDialog'

describe('LoadingDialog.vue', () => {
  let unitTest
  before((done) => {
    cy.unitTest(LoadingDialog, null, null).then((mountedResult) => {
      unitTest = mountedResult
      done()
    })
  })

  it('should show up', (done) => {
    unitTest.componentWrapper.setProps({
      show: true,
      description: 'Loading',
    })
    spec.get('.loading-dialog').then((element) => {
      expect(element).to.exist
      expect(element).to.be.visible
      done()
    })
  })

  it('should show the given description', (done) => {
    const description = 'This is a descriptive text for this loading process'
    unitTest.componentWrapper.setProps({
      show: true,
      description,
    })
    spec.get('.loading-dialog').then(() => {
      spec.get('.md-dialog-content p').then((p) => {
        expect(Cypress.$(p).text()).to.eq(description)
        done()
      })
    })
  })
})

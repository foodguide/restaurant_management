// Plugins
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import VueScrollTo from 'vue-scrollto'
import VueAnalytics from 'vue-analytics'
import VueMatchHeights from 'vue-match-heights'
import Datepicker from 'vuejs-datepicker'
import VueMaterial from 'vue-material'
import VTooltip from 'v-tooltip'
import VueLazyload from 'vue-lazyload'
import Notifications from 'vue-notification'
import VueTour from 'vue-tour'
import VueImg from 'v-img'
import Placeholder from './assets/placeholder.jpg'
import VueFB from './components/api/Facebook'
import SideBar from './components/UIComponents/SidebarPlugin'
import { Slack } from './components/api/Slack'

export const plugins = [
  Vuex, VueRouter, Slack,
  SideBar, VueFB, VueMatchHeights, Datepicker, VueMaterial,
  VTooltip, Notifications, VueTour, VueImg,
]

export const adminPlugins = [
  Vuex, VueRouter, Slack, VueMaterial,
]

export const installPlugins = (Vue) => {
  plugins.forEach(plugin => Vue.use(plugin))
  Vue.use(VueScrollTo, {
    container: 'body',
    duration: 500,
    easing: 'ease',
    offset: 0,
    cancelable: true,
    onStart: false,
    onDone: false,
    onCancel: false,
    x: false,
    y: true,
  })
  Vue.use(VueLazyload, {
    loading: Placeholder,
  })
}

export const installAdminPlugins = (Vue) => {
  adminPlugins.forEach(plugin => Vue.use(plugin))
}

const CypressAnalytics = {
  install(Vue, options) {
    // 4. add an instance method
    Vue.prototype.$ga = {
      event(...args) {
        console.log('analytics doesn\'t work during tests')
      },
    }
  },
}

export const installPluginsWithRouter = (Vue, router) => {
  if (window.Cypress) {
    Vue.use(CypressAnalytics)
  } else {
    Vue.use(VueAnalytics, {
      id: process.env.ANALYTICS_ID,
      router,
      debug: {
        sendHitTask: process.env.NODE_ENV === 'production',
      },
    })
  }
}

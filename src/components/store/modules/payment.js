import Api from '../../api/Api'
import { HookId } from './hooks'
import { EventName, EventType } from './event'

const state = {
  hasActiveSubscriptions: false,
  hasCancelledSubscriptions: false,
  hasRequestedSubscriptionStatus: false,
  plans: [
    {
      id: 'respondo-basic',
      priceLabel: 'payment.basicPriceLabel',
      timeLabel: null,
      planLabel: null,
    },
    {
      id: 'start4',
      priceLabel: 'payment.testPriceLabel',
      timeLabel: 'payment.testTimeLabel',
      planLabel: 'respondo-starter',
    },
  ],
}

const getters = {
  hasActivePayment: state => state.hasActiveSubscriptions,
  hasCancelledPayment: state => state.hasCancelledSubscriptions,
  getBasicPaymentPlan: state => state.plans.find(plan => plan.id === 'respondo-basic'),
  getStartPlans: state => state.plans.filter(plan => plan.id !== 'respondo-basic'),
  getStartPlanByLabel: (state, getters) => label => getters.getStartPlans.find(plan => plan.planLabel === label),
  hasRequestedSubscriptionStatus: state => state.hasRequestedSubscriptionStatus,
}
const mutations = {
  setActiveSubscriptions(state, hasActiveSubscriptions) {
    state.hasActiveSubscriptions = hasActiveSubscriptions
  },
  setCancelledSubscriptions(state, hasCancelledSubscriptions) {
    state.hasCancelledSubscriptions = hasCancelledSubscriptions
  },
  setHasRequestedSubscriptionStatus(state) {
    state.hasRequestedSubscriptionStatus = true
  },
  paymentStateReset(state) {
    state.hasActiveSubscriptions = false
    state.hasCancelledSubscriptions = false
    state.hasRequestedSubscriptionStatus = false
  },
  setEmptySubscriptions(state) {
    state.hasActiveSubscriptions = false
    state.hasCancelledSubscriptions = false
    state.hasRequestedSubscriptionStatus = true
  },
}
const actions = {
  checkoutNewPlan(context, planId) {
    context.dispatch('writeEvent', {
      name: EventName.CheckoutStart,
      type: EventType.Action,
      data: { planId },
    })
    return Api.chargebee.checkoutNewPlan(planId).then(() => {
      context.commit('setHasRequestedSubscriptionStatus')
      context.commit('setActiveSubscriptions', true)
      context.dispatch('onHookEvent', { hookId: HookId.SubscriptionStatus, payload: { activeSubscriptionsResult: true } })
      context.dispatch('writeEvent', {
        name: EventName.CheckoutFinish,
        type: EventType.Action,
        data: { planId },
      })
      return true
    })
  },
  checkoutNewPlanWithTestplan(context, testPlanId) {
    const basicPlanId = context.getters.getBasicPaymentPlan.id
    context.dispatch('writeEvent', {
      name: EventName.CheckoutTestPlanStart,
      type: EventType.Action,
      data: { planId: basicPlanId },
    })
    return Api.chargebee.checkoutNewPlan(testPlanId).then(hostedPageId => Api.backend.payment.getHostedPage(hostedPageId).then((data) => {
      const subscriptionId = data.content.subscription.id
      return Api.backend.payment.upgradeSubscriptionToBasicPlan(subscriptionId).then((response) => {
        context.commit('setHasRequestedSubscriptionStatus')
        context.commit('setActiveSubscriptions', true)
        context.dispatch('onHookEvent', { hookId: HookId.SubscriptionStatus, payload: { activeSubscriptionsResult: true } })
        context.dispatch('writeEvent', {
          name: EventName.CheckoutTestPlanFinish,
          type: EventType.Action,
          data: { planId: basicPlanId },
        })
        return response.data
      })
    }))
  },
  openPaymentPortal(context) {
    return Api.chargebee.openPortal()
  },
  hasSubscriptions(context) {
    const promises = []
    let activeSubscriptionsResult = false
    let cancelledSubscriptionsResult = false

    promises.push(Api.backend.payment.hasActiveSubscriptions().then((hasActiveSubscriptions) => {
      activeSubscriptionsResult = hasActiveSubscriptions
    }))
    promises.push(Api.backend.payment.hasCancelledSubscriptions().then((hasCancelledSubscriptions) => {
      cancelledSubscriptionsResult = hasCancelledSubscriptions
    }))

    return Promise.all(promises).then(() => setSubscriptionState(context, activeSubscriptionsResult, cancelledSubscriptionsResult)).catch(() => {
      context.commit('logout')
      setSubscriptionState(context, activeSubscriptionsResult, cancelledSubscriptionsResult)
    })
  },
  updateCurrentSubscriptionLocationCount(context, numberOfLocations) {
    return Api.backend.payment.getActiveSubscriptions().then((entries) => {
      if (entries.length === 0) {
        // This means the location was created during the onboarding process
        // The plan will be set afterwards
        // see the action 'checkoutNewPlanWithTestplan' above
        return Promise.resolve()
      }

      const { subscription } = entries[0]
      return Api.backend.payment.updateSubscription(subscription.id, { numberOfLocations }).then(response => response.data)
    })
  },
}

let setSubscriptionState = (context, activeSubscriptionsResult, cancelledSubscriptionsResult) => {
  context.commit('setHasRequestedSubscriptionStatus')
  context.commit('setActiveSubscriptions', activeSubscriptionsResult)
  context.commit('setCancelledSubscriptions', cancelledSubscriptionsResult)
  context.dispatch('onHookEvent', { hookId: HookId.SubscriptionStatus, payload: { activeSubscriptionsResult } })
  return Promise.resolve()
}

if (process.env.NODE_ENV !== 'production') {
  actions.setTestPayment = (context) => {
    context.commit('setHasRequestedSubscriptionStatus')
    context.commit('setActiveSubscriptions', true)
    context.dispatch('onHookEvent', { hookId: HookId.SubscriptionStatus, payload: { activeSubscriptionsResult: true } })
  }
}

export default {
  state,
  getters,
  actions,
  mutations,
}

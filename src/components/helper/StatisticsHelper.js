import moment from 'moment'
import DateHelper from './DateHelper'

const generateId = () => Math.floor(Math.random() * 1000000000)

/**
 * This class is a model class for the statistic of a representation and is
 * based on the typescript class from the backend.
 */
export class RepresentationStatistic {
  constructor() {
    this.locationId = 0
    this.representationId = null
    this.date = null
    this.totalReviews = 0
    this.avgRating = 0
    this.answered = 0
    this.ratingOne = 0
    this.ratingTwo = 0
    this.ratingThree = 0
    this.ratingFour = 0
    this.ratingFive = 0
    this.recommendationPositive = 0
    this.recommendationNegative = 0
  }
}

export class EmptyRepresentationStatistic {
  constructor({
    locationId, representationId, userId, date,
  }) {
    this.locationId = locationId
    this.representationId = representationId
    this.userId = userId
    this.answered = 0
    this.avgRating = 0
    this.date = date
    this.id = generateId()
    this.totalReviews = 0
    this.isFiller = true
  }
}

/**
 * This StatisticsHelper offers some utility functions that are related to the statistics.
 */
export default class StatisticsHelper {
  /**
   * Calculating the rating average based on the given review objects.
   *
   * @param reviews
   * @return {string}
   */
  static getAverage(reviews) {
    const reviewsWithRating = reviews.filter(review => !!review.rating || !!review.ratingType)
    if (reviewsWithRating.length === 0) {
      return '-'
    }
    let totalStars = 0
    reviewsWithRating.forEach((review) => {
      if (review.rating) {
        totalStars += review.rating
      } else if (review.ratingType) {
        const rating = review.ratingType === 'positive' ? 5 : 1
        totalStars += rating
      }
    })
    return (totalStars / reviewsWithRating.length).toFixed(2)
  }

  /**
   * This function sorts the given statistic objects based on the date.
   *
   * @param statistics
   * @return {*}
   */
  static sort(statistics) {
    const getDate = DateHelper.getDateFromTimestamp
    return statistics.sort((stats1, stats2) => getDate(stats1.date).getTime() - getDate(stats2.date).getTime())
  }

  /**
   * This function calculates the total rating sum based on the given statistic objects.
   *
   * @param statistics
   * @return {*}
   */
  static getRatingSum(statistics) {
    return statistics
      .map(statsData => statsData.totalReviews * statsData.avgRating)
      .reduce((total, amount) => total + amount)
  }

  /**
   * This function calculates the total number of ratings for all the given statistic objects.
   *
   * @param statistics
   * @return {*}
   */
  static getRatingCount(statistics) {
    return statistics
      .map(statsData => statsData.totalReviews)
      .reduce((total, amount) => total + amount)
  }

  /**
   * This function calculates the average rating of the given statistic objects.
   *
   * @param statistics
   * @return {number}
   */
  static getAverageRating(statistics) {
    const ratingSum = StatisticsHelper.getRatingSum(statistics)
    const ratingCount = StatisticsHelper.getRatingCount(statistics)
    return (ratingSum / ratingCount)
  }

  /**
   * This function combines the statistic objects given as array and calculates the values for the different fields.
   *
   * @param statistics
   * @return {null|RepresentationStatistic}
   */
  static unify(statistics) {
    const unified = new RepresentationStatistic()
    if (!statistics) return null
    if (statistics.length === 0) return unified
    if (statistics[0].locationId) unified.locationId = statistics[0].locationId
    if (statistics[0].date) unified.date = statistics[0].date
    statistics.forEach((stat) => {
      unified.totalReviews += stat.totalReviews
      unified.answered += stat.answered
      unified.ratingOne += stat.ratingOne
      unified.ratingTwo += stat.ratingTwo
      unified.ratingThree += stat.ratingThree
      unified.ratingFour += stat.ratingFour
      unified.ratingFive += stat.ratingFive
      unified.recommendationPositive += stat.recommendationPositive
      unified.recommendationNegative += stat.recommendationNegative
    })
    unified.avgRating = StatisticsHelper.getAverageRating(statistics)
    return unified
  }

  /**
   * This static function compares two given statistic objects by their date.
   *
   * @param left
   * @param right
   * @return {number}
   */
  static compare(left, right) {
    return DateHelper.getDateFromTimestamp(left.date) - DateHelper.getDateFromTimestamp(right.date)
  }

  /**
   * This static function returns the older statistic object of the two given ones.
   *
   * @param left
   * @param right
   * @return {*}
   */
  static getOlderStatistic(left, right) {
    return StatisticsHelper.compare(left, right) < 0 ? left : right
  }

  /**
   * This static function returns the older statistic object of the two given ones.
   *
   * @param left
   * @param right
   * @return {*}
   */
  static getNewerStatistic(left, right) {
    return StatisticsHelper.compare(left, right) < 0 ? right : left
  }

  /**
   * Returns the oldest statistic of the given ones
   *
   * @param statistics
   * @return {*}
   */
  static getOldest(statistics) {
    return statistics.reduce((left, right) => StatisticsHelper.getOlderStatistic(left, right))
  }

  /**
   * Returns the newest statistic of the given ones
   *
   * @param statistics
   * @return {*}
   */
  static getNewest(statistics) {
    return statistics.reduce((left, right) => StatisticsHelper.getNewerStatistic(left, right))
  }

  /**
   * This static method creates an RepresentationStatistic object that holds the diff calculations
   * between the two statistic objects.
   *
   * @param left
   * @param right
   * @return {null|RepresentationStatistic}
   */
  static getDeltaStatistics(left, right) {
    if (!left || !right) return null
    if (left.locationId !== right.locationId) {
      throw new Error('Calculating differences between two statistics which aren\'t '
        + 'from the same location doesn\'t make sense.')
    }
    const oldStat = StatisticsHelper.getOlderStatistic(left, right)
    const newStat = StatisticsHelper.getNewerStatistic(left, right)
    const delta = new RepresentationStatistic()
    delta.totalReviews = newStat.totalReviews - oldStat.totalReviews
    delta.answered = newStat.answered - oldStat.answered
    delta.ratingOne = newStat.ratingOne - oldStat.ratingOne
    delta.ratingTwo = newStat.ratingTwo - oldStat.ratingTwo
    delta.ratingThree = newStat.ratingThree - oldStat.ratingThree
    delta.ratingFour = newStat.ratingFour - oldStat.ratingFour
    delta.ratingFive = newStat.ratingFive - oldStat.ratingFive
    delta.recommendationPositive = newStat.recommendationPositive - oldStat.recommendationPositive
    delta.recommendationNegative = newStat.recommendationNegative - oldStat.recommendationNegative
    delta.avgRating = StatisticsHelper.getDeltaAverage(delta)
    delta.trend = newStat.avgRating - oldStat.avgRating
    return delta
  }

  /**
   * Creating the avgRating for a delta statistic. The ratingOne, ratingTwo and so needs to be already
   * calculated.
   *
   * @param delta
   * @return {number}
   */
  static getDeltaAverage(delta) {
    if (delta.totalReviews === 0) return 0
    let sum = 0
    sum += delta.ratingOne
    sum += delta.ratingTwo * 2
    sum += delta.ratingThree * 3
    sum += delta.ratingFour * 4
    sum += delta.ratingFive * 5
    sum += delta.recommendationPositive * 5
    sum += delta.recommendationNegative
    return sum / delta.totalReviews
  }

  /**
   * This functions compares a statistic object against an empty statistic.
   *
   * @param stats
   * @return {{trend: null}}
   */
  static getDeltaToEmptyStatistics(stats) {
    return { ...stats, trend: null }
  }

  /**
   * Mapping all the statistics into an object that has the representation ids as properties
   *
   * @param statistics
   */
  static getStatisticsPerRepresentation(statistics) {
    // Creating an empty object that will have the represetation ids as properties in the end
    const statisticsPerRepresentation = {}
    /* During the iteration we want to keep track of the oldest statistic entry. For the start
       the oldestMoment should be in the future and will then be overwritten when there is at
       least one statistic entry */
    let oldestMoment = DateHelper.getFutureMoment()
    // Creating a moment that represents yesterday to fill up the statistics until yesterday
    const yesterday = moment().subtract(1, 'day').endOf('day')
    // In this forEach the statistics per representation will be created with the given data
    statistics.forEach((statistic) => {
      const { representationId, date } = statistic
      const statsMoment = DateHelper.getMomentFromTimestamp(date)
      if (statsMoment.isBefore(oldestMoment)) oldestMoment = statsMoment
      if (!statisticsPerRepresentation[representationId]) statisticsPerRepresentation[representationId] = [statistic]
      else statisticsPerRepresentation[representationId].push(statistic)
    })
    /* We already have created the statistics per representations but we're not done yet. We need
    *  to fill up the statistics data to make sure that the data for each representation starts at the same date,
    *  ends at the same day and has a statistic for every date. */
    Reflect.ownKeys(statisticsPerRepresentation).forEach((representationId) => {
      const oldestStatisticForRepresentation = StatisticsHelper.getOldest(statisticsPerRepresentation[representationId])
      const { locationId, userId } = oldestStatisticForRepresentation
      /* This function is used to add missing representations. It will be created for the given moment
      *  and uses the given previous statistics for it's data. */
      const add = (statMoment, previous = new EmptyRepresentationStatistic({
        locationId,
        representationId,
        userId,
        date: null,
      })) => {
        statisticsPerRepresentation[representationId].push({
          ...previous,
          date: statMoment.toDate(),
          id: generateId(),
        })
      }
      // Creating the statistics entries before the first review for a representation has happened
      const oldestRepresentationMoment = DateHelper.getMomentFromTimestamp(oldestStatisticForRepresentation.date)
      while (oldestRepresentationMoment.isSameOrAfter(oldestMoment, 'day')) {
        oldestRepresentationMoment.subtract(1, 'day')
        add(oldestRepresentationMoment)
      }
      /* Creating the statistic entries after the newest statistic entries until we have statistic data for
         every representation until yesterday
       */
      const newestStatistic = StatisticsHelper.getNewest(statisticsPerRepresentation[representationId])
      const newestRepresentationMoment = DateHelper.getMomentFromTimestamp(newestStatistic.date)
      while (newestRepresentationMoment.isSameOrBefore(yesterday, 'day')) {
        newestRepresentationMoment.add(1, 'day')
        add(newestRepresentationMoment, newestStatistic)
      }
      /* In the end we check if we have enough statistic entries for the overall time range
         and if not we find and fill the gaps
       */
      const daysCount = yesterday.diff(oldestMoment, 'days')
      if (statisticsPerRepresentation[representationId].length < daysCount) {
        let previous = null
        while (oldestMoment.isSameOrBefore(yesterday)) {
          const statistic = StatisticsHelper.getStatisticForMoment(statistics, oldestMoment)
          if (!statistic) {
            previous = StatisticsHelper.getStatisticForPreviousDay(statistics, oldestMoment)
            if (!previous) {
              add(oldestMoment)
            } else {
              add(oldestMoment, previous)
            }
          }
          oldestMoment.add(1, 'day')
        }
      }
      /* In the end we sort the statistics by date so that all the components can work just with the indexes
         and don't have to use some cost intensive date, moment creations
       */
      const getDate = ({ date }) => (date.seconds ? date.toDate() : date)
      const sorter = (left, right) => getDate(left) - getDate(right)
      statisticsPerRepresentation[representationId] = statisticsPerRepresentation[representationId].sort(sorter)
    })
    return statisticsPerRepresentation
  }

  /**
   * Returns the statistic for a specific date from the given statistics array.
   *
   * @param statistics
   * @param dayMoment
   * @return {*}
   */
  static getStatisticForMoment(statistics, dayMoment) {
    return statistics.find(({ date }) => DateHelper.getMomentFromTimestamp(date).isSame(dayMoment, 'day'))
  }

  /**
   * Returns the statisitics for the previous date of a given date. This function clones the given moment because it
   * is given as a reference and we want to prevent side effects here.
   *
   * @param statistics
   * @param dayMoment
   * @return {*}
   */
  static getStatisticForPreviousDay(statistics, dayMoment) {
    const previousDay = dayMoment.clone()
    previousDay.subtract(1, 'day')
    return StatisticsHelper.getStatisticForMoment(statistics, previousDay)
  }
}

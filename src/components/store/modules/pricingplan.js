const standardFeatures = [
  'more.pricing.features.one',
  'more.pricing.features.two',
  'more.pricing.features.three',
  'more.pricing.features.four',
  'more.pricing.features.five',
]

const state = {
  pricingPlans: [
    {
      name: 'more.pricing.standard.name',
      price: 'more.pricing.standard.price',
      active: true,
      buttonText: 'message.startNow',
      to: '/onboarding',
      features: standardFeatures,
      description: 'more.pricing.standard.description',
      hideDescription: false,
      isHighlighted: true,
      order: 1,
    },
    // {
    //   name: 'more.pricing.plus.name',
    //   price: 'more.pricing.plus.price',
    //   active: false,
    //   buttonText: 'more.pricing.plus.button',
    //   to: null,
    //   features: [...standardFeatures, 'more.pricing.features.chatbot'],
    //   description: 'more.pricing.plus.description',
    //   isHighlighted: false,
    //   hideDescription: false,
    //   order: 2,
    // },
    {
      name: 'more.pricing.premium.name',
      price: 'more.pricing.premium.price',
      active: true,
      buttonText: 'more.pricing.premium.button',
      to: null,
      href: 'mailto:support@respondo.app',
      features: [...standardFeatures, 'more.pricing.features.personally'],
      description: 'more.pricing.premium.description',
      isHighlighted: false,
      hideDescription: false,
      order: 3,
    },
  ],
}

const getters = {
  getOrderedPricingPlans: state => state.pricingPlans.sort((a, b) => a.order - b.order),
}

export default {
  state,
  getters,
  mutations: {},
  actions: {},
}

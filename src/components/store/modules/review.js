import Vue from 'vue'
import ReviewRepository from '../repositories/ReviewRepository'
import Api from '../../api/Api'
import { EventName, EventType } from './event'
import { Slack } from '../../api/Slack'
import {capitalize} from "../../helper/StringHelper";

const storeState = {
  reviews: [],
}

const getters = {
  getReviewsForCurrentUser() {
    return storeState.reviews
  },
}

const mutations = {

  fetchReviews(state, payload) {
    state.reviews = payload
  },
  reply(state, payload) {
    const reviewToMutate = state.reviews.find(review => review.id === payload.reviewId)
    if (!reviewToMutate) return
    const index = state.reviews.indexOf(reviewToMutate)
    reviewToMutate.response = payload.response
    reviewToMutate.state = 'seen'
    // Following link has the explanation why using Vue.set() here is necessary
    // https://stackoverflow.com/questions/38819289/why-is-computed-value-not-updated-after-vuex-store-update
    Vue.set(state.reviews, index, reviewToMutate)
  },
  setState(state, payload) {
    const reviewToMutate = state.reviews.find(review => review.id === payload.reviewId)
    if (!reviewToMutate) return
    const index = state.reviews.indexOf(reviewToMutate)
    reviewToMutate.state = payload.state
    Vue.set(state.reviews, index, reviewToMutate)
  },
  reviewStateReset(state) {
    state.reviews = []
  },
}

const reviewRepository = new ReviewRepository()

const actions = {
  fetchReviews(context, force = false) {
    if (context.state.reviews.length === 0 || force) {
      return reviewRepository.findAllForCurrentUser().then((data) => {
        Slack.logText(`counting all reviews for current user ${reviewRepository.getCurrentUserUid()}: ${data.length}`)
        context.commit('fetchReviews', data)
        return data
      })
    }
    return Promise.resolve(context.state.reviews)
  },
  setState(context, payload) {
    return reviewRepository.setReviewState(payload.reviewId, payload.state).then(() => {
      context.commit('setState', payload)
      return payload
    })
  },
  deleteRepresentationReviews(context, payload) {
    const representationInformation = payload.id.split('_')
    const locationId = representationInformation[1]

    let platform = ''
    switch (representationInformation[0]) {
      case 'fb':
        platform = 'facebook'
        break
      case 'go':
        platform = 'google'
        break
      case 'ig':
        platform = 'instagram'
        break
      case 'yp':
        platform = 'yelp'
        break
      case 'ta':
        platform = 'tripadvisor'
        break
      default:
        break
    }

    const keepCondition = review => !(review.locationId === locationId && review.platform === platform)
    storeState.reviews = storeState.reviews.filter(keepCondition)
  },
  deleteLocationReviews(context, payload) {
    const keepCondition = review => review.locationId !== payload.id
    storeState.reviews = storeState.reviews.filter(keepCondition)
  },
  syncApiReviews(context) {
    const lastSync = context.getters.lastApiReviewSync
    const oneHourTimeout = 60 * 60 * 1000
    if (!lastSync || Date.now() - lastSync > oneHourTimeout) {
      context.commit('setLastApiReviewSync', Date.now())
      const apiPlatforms = context.getters.getAllPlatforms
        .filter(platform => platform.type === 'api')
        .map(platform => platform.name)
      return Api.backend.sync.api(apiPlatforms).then(() => {
        window.setTimeout(() => {
          context.dispatch('fetchReviews', true)
        }, 15 * 1000)
      })
    }
    return Promise.resolve()
  },
  sendReply(context, { review, text }) {
    context.dispatch('writeEvent', {
      name: EventName.SendReplyStart,
      type: EventType.Action,
      data: { reviewId: review.id, text },
    })
    context.dispatch('addReplyTask', { id: review.id })
    return Api.backend.review.reply(review, text).then(() => {
      window.setTimeout(() => {
        context.dispatch('setState', {
          reviewId: review.id,
          state: 'seen',
        })
      }, 1500)
    })
  },
  setReviewReplied(context, { reviewId, text }) {
    context.commit('reply', {
      reviewId,
      response: {
        id: `reply_${reviewId}`,
        text,
      },
    })

    context.dispatch('writeEvent', {
      name: EventName.SendReplyFinish,
      type: EventType.Action,
      data: { reviewId },
    })
  },
}

if (process.env.NODE_ENV !== 'production') {
  actions.setTestReviews = (context, payload) => {
    context.commit('fetchReviews', payload)
  }
}


export default {
  state: storeState,
  getters,
  actions,
  mutations,
}

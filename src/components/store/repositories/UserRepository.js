import BaseRepository from './BaseRepository'
import { Slack } from '../../api/Slack'
import TranslationHelper from '../../helper/TranslationHelper'

export const AccessTokenStatus = {
  ACTIVE: 'active',
  ACCESSTOKEN_EXPIRED: 'accesstoken_expired',
  PERMISSION_DECLINED: 'permission_declined',
  ACCESSTOKEN_REQUIRED: 'accesstoken_required',
}

class UserRepository extends BaseRepository {
  get managedCollection() {
    return 'user'
  }

  get documentAttributes() {
    return ['credentials', 'id', 'subscriptions', 'lastActiveTime', 'pipedriveId', 'locale']
  }

  create(data) {
    const filtered = this.filterAllowedAttributes(data)
    const uid = this.getCurrentUserUid()
    if (!uid) return
    filtered.userId = uid
    return this.commitWrite(this.collection.doc(uid).set(filtered)).then(() => data)
  }

  findByUid(uid) {
    return this.commitRead(this.collection.doc(uid).get())
  }

  getCurrentUser() {
    const uid = this.getCurrentUserUid()
    return uid ? this.findByUid(uid) : Promise.resolve()
  }

  createOrUpdateCredential(userData, credential) {
    const credentialToUpdate = userData.credentials.find(token => token.id === credential.id)
    const data = { ...credentialToUpdate, ...credential }
    if (!userData.id) userData.id = this.getCurrentUserUid()
    if (credentialToUpdate) {
      Slack.logText(`Updating credential ${credentialToUpdate.id} from user ${userData.id}. Old values: ${JSON.stringify(credentialToUpdate)}. New Values: ${JSON.stringify(data)}`)
      const index = userData.credentials.indexOf(credentialToUpdate)
      userData.credentials[index] = data
    } else {
      Slack.logText(`Creating credential for user ${userData.id}. New Values: ${JSON.stringify(data)}`)
      userData.credentials.push(data)
    }
    return this.updateOrCreate(userData.id, userData).then(() => userData).catch((error) => {
      console.log(error)
      throw error
    })
  }

  updateLocale(userId, locale) {
    return this.updateOrCreate(userId, { locale })
  }

  addAccessToken(userData, accessToken, platformName, credentialId, status = AccessTokenStatus.ACTIVE) {
    if (!accessToken) {
      if (status !== AccessTokenStatus.ACCESSTOKEN_EXPIRED && status !== AccessTokenStatus.ACCESSTOKEN_REQUIRED) {
        return Promise.reject(new Error('No valid token given.'))
      }
    }

    if (!userData.credentials) userData.credentials = []

    const credentialToUpdate = userData.credentials.find(credential => credential.id === credentialId)

    if (!credentialToUpdate) {
      const newTokenData = { platform: platformName.toLowerCase(), status }
      if (accessToken) {
        newTokenData.accessToken = accessToken
      }
      userData.credentials.push(newTokenData)
    } else {
      const index = userData.credentials.indexOf(credentialToUpdate)
      if (credentialToUpdate.accessToken === accessToken) {
        return Promise.resolve(userData)
      }
      if (accessToken) credentialToUpdate.accessToken = accessToken
      credentialToUpdate.platform = platformName.toLowerCase()
      credentialToUpdate.status = status
      userData.credentials[index] = credentialToUpdate
    }
    if (!userData.id) userData.id = this.getCurrentUserUid()
    return this.updateOrCreate(userData.id, userData).then(() => userData).catch((error) => {
      console.log(error)
      console.log(JSON.stringify(error))
      throw error
    })
  }

  onAfterAuthenticated(credentials = []) {
    // If user doesn't exist in DB, we assume it's a new registration and add locale immediately. It's essential for the welcome e-mail.
    return this.getCurrentUser().then(user => (user ? this.updateCredentials(user, credentials) : this.create({ credentials, locale: TranslationHelper.currentLocale() })))
  }

  updateCredentials(user, credentials = []) {
    if (!user.credentials) {
      user.credentials = credentials
      return this.update(user.id, user).then(() => user)
    }
    const promises = []
    credentials.forEach((credential) => {
      promises.push(this.addAccessToken(user, credential.accessToken, credential.platform, credential.id))
    })
    return Promise.all(promises)
  }
}

export default UserRepository

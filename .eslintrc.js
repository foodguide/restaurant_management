// http://eslint.org/docs/user-guide/configuring

module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module'
  },
  env: {
    browser: true,
    jquery: true
  },
  // https://github.com/airbnb/javascript
  extends: ['plugin:vue/essential', 'airbnb', 'plugin:cypress/recommended'],
  plugins: ['vue', 'cypress'],
  // add your custom rules here
  rules: {
    semi: [2, "never"],
    "max-len": [1, 120]
  }
}

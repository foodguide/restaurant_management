﻿# Die Macht der Online Reputation - Bewertungsplattformen gewinnen stetig an Relevanz

Unter Online Reputation versteht man den Ruf einer Person, eines Unternehmens oder einer Einrichtung im Internet. Genau so, wie man in der realen Welt einen guten oder schlechten Ruf haben kann, läuft es auch im Digitalen ab. Oftmals nur leider in einer viel größeren Dimension, denn das Internet vergisst nichts, ist weltweit verfügbar und ermöglicht es, Meldungen, Fotos oder Videos in Windeseile zu verbreiten – Stichwort: Fake News. Wie ein Laubfeuer verteilen sich die Gerüchte und können – ob wahr oder falsch – nicht kontrolliert werden und oftmals ganze Existenzen zerstören. Zugleich bietet das Netz aber auch Menschen eine Stimme, die unter den gegebenen Umständen keinerlei Möglichkeit hätten, solch ein großes Publikum zu erreichen, und kreiert eine besondere Aufmerksamkeit – bestes Beispiel zur Zeit: #metoo.

  

**Überlassen Sie Ihre Online Reputation nicht dem Zufall**
Diese Entwicklung und die mit ihr einhergehende Dynamik betrifft uns alle, ob wir wollen oder nicht. Denn auf das, was andere über uns schreiben, was Dritte im Internet über uns veröffentlichen – darauf haben wir keinen Einfluss.

  

Was wir allerdings tun können, ist aktiv dagegen zu steuern: mit einem proaktiven Online Reputations Management gelingt es, gezielt authentische und den eigenen Vorstellungen entsprechende Informationen über die eigene Person, das Unternehmen oder die angebotene Dienstleistung zu verbreiten und so ein stimmiges Image aufzubauen.

  

Für Unternehmen und Dienstleister liegt ein Schwerpunkt hierbei sicherlich im aktiven Managen der Kundenbewertungen. So wie früher Empfehlungen mündlich und im Freundes-und Familienkreis weitergegeben wurden, passiert dies nun online: ob Hotel oder Arzt, Kosmetikstudio oder Restaurant – vor Kundenbewertungen auf den großen Bewertungsportalen im Internet bleibt heute niemand mehr “verschont”.

  

Grund genug, diese Bewertungen und die damit einhergehende Beurteilung ihres Unternehmens nicht dem Zufall zu überlassen. Denn rund 90 % aller Nutzer informieren sich via Rezensionen über ein Unternehmen, bevor sie dieses besuchen oder die angebotenen Dienste in Anspruch nehmen.

  

Aufgrund der Vielzahl an unterschiedlichen Bewertungsplattformen – von Facebook bis Google oder branchenspezifischeren wie TripAdvisor oder Jameda – fällt es im Arbeitsalltag oft schwer, den Überblick zu behalten und ausreichend Ressourcen bereitzustellen, um angemessen auf das Feedback zu reagieren.

  

Mit aktivem Bewertungsmanagement zu einer besseren Online Reputation

Zeit – und somit auch Geldersparnis bietet Ihnen hier der Einsatz einer professionellen Bewertungs-Management-Lösung, mit der Sie Ihre Kunden gezielt um Bewertungen bitten, diese sammeln, analysieren und auswerten und auf den für Sie relevanten Plattformen veröffentlichen können.

  

Digitalisierung sei Dank verläuft ein Großteil dieser Prozesse automatisch und die moderne Technik übernimmt für Sie einen Großteil der Arbeit, die Sie ansonsten manuell erledigen müssten. So profitieren Sie nicht nur von mehr – und im Schnitt besseren – Bewertungen, Sie haben auch einen Einfluss darauf, wo diese veröffentlicht werden und können das Feedback Ihrer Kunden nutzen, um Ihr Angebot stetig zu verbessern, Schwachstellen zu identifizieren und zu beheben.

  

Gleichzeitig wird es Ihnen auf lange Sicht gelingen, sich im Ranking der jeweiligen Bewertungsplattformen kontinuierlich weiter nach oben zu verbessern und die gute Position zu halten. Auch in den Google Suchergebnissen können Sie mit einem besseren Ergebnis rechnen. Somit sind Sie für potentielle Kunden nicht nur leichter zu finden, sondern wirken zugleich auch attraktiver und vertrauenswürdiger.

  

**Online Reputations Management: Mehr als “nur” Bewertungen sammeln**
Mit dem aktiven Managen Ihrer Bewertungen haben Sie schon einen entscheidenden Beitrag zu einem gelungenen Online Reputations-Management geleistet. Doch gibt es weitere Faktoren, die eine wichtige Rolle für einen langfristigen Erfolg spielen, und die ebenfalls Teil des professionellen Online Reputations Managements sind.

  

**Googeln Sie sich selbst!**
Googeln Sie sich (bzw. Ihr Unternehmen) doch einmal selbst und schauen Sie sich die Suchergebnisse genauer an. Öffnen Sie einen neuen Tab im Browser im Inkognito-Modus, denn nur so werden Ihnen die neutralen Suchergebnisse angezeigt, wie andere Nutzer sie ebenfalls sehen. Ihre Suchmaschine kennt Sie, Ihre Vorlieben und Gewohnheiten, und ohne Inkognito-Modus erhalten Sie ein auf Sie persönlich angepasstes Ergebnis, das nicht repräsentativ ist.

  

**Was sehen Sie? Und gefällt Ihnen, was Sie sehen?**
Wie oben bereits erwähnt, haben Sie wenig Einfluss darauf, was Dritte über Sie posten. Was Sie hingegen steuern können, ist das, was Sie selbst veröffentlichen oder in Ihrem Auftrag von anderen, wie Bloggern oder Kooperationspartnern, veröffentlichen lassen.

Ihre eigene Homepage sollte in der Suche an oberster Stelle erscheinen. Ist dies nicht der Fall, nehmen Sie sich Zeit und optimieren die Webseite, indem Sie beispielsweise regelmäßig neuen Content veröffentlichen – denn die Suchmaschinen lieben frischen Content und danken es mit einem besseren Platz im Ranking.

Des Weiteren empfiehlt es sich, auf allen relevanten Portalen mit einem eigenen Profil vertreten zu sein. Auch so belegen Sie gleich mehrere Plätze in den Suchergebnissen und verdrängen gegebenenfalls unliebsame Einträge auf die hinteren Plätze. Gleichzeitig stärken Sie Ihre Reputation, denn viele Kunden und Gäste vertrauen den großen, bekannten Plattformen – und dieses Vertrauen überträgt sich direkt auf das jeweilige Profil Ihres Unternehmens.

**Online Reputations Management in den sozialen Netzwerken**
Auch hier gilt: sofern noch nicht geschehen, legen Sie auf den für Ihr Unternehmen relevanten Plattformen Accounts an. Ob Facebook, Instagram, Twitter oder Snapchat – Sie müssen längst nicht überall vertreten sein, zwei soziale Netzwerke sollten Sie jedoch bespielen, um Ihre Präsenz im Netz – und somit Ihre Auffindbarkeit und Ihre Online Reputation – zu stärken.

Veröffentlichen Sie regelmäßig neue Inhalte. Überlegen Sie sich hierbei genau, welches Bild Sie von sich und Ihrem Unternehmen in der Öffentlichkeit präsentieren möchten.
Sie können die sozialen Medien nicht nur nutzen, um Angebote und Aktionen zu promoten, neue Kunden zu gewinnen und mit Stammkunden in Kontakt zu bleiben.
Gleichzeitig fungieren Portale wie Facebook oder Instagram wie ein virtuelles Schaufenster, das einen Blick in Ihr Unternehmen gewährt. Veröffentlichen Sie also auch regelmäßig Content zu Ihren Alleinstellungsmerkmalen, die Sie von Ihren Wettbewerbern unterscheiden.

Oder lassen Sie doch einfach mal Ihre Mitarbeiter übernehmen – ein sogenanntes Instagram-Takeover. Leiten Sie ein Hotel, dann lassen Sie eine Woche lang Ihren Koch Fotos und kurze Videos (die natürlich gerne der für Social Media zuständige Mitarbeiter posten kann) aus dem Küchenalltag und spannende Insights, die der Gast sonst nicht zu Gesicht bekommt, aufnehmen. In der Folge-Woche ist dann vielleicht der Gärtner dran oder Ihr Concierge. So haben Sie nicht nur originelle Inhalte abseits der allgegenwärtigen Food- und Pool-Fotos, sondern werden für Ihre Gäste auch nahbarer und stellen eine persönliche Bindung her.

Animieren Sie Ihre Gäste, ebenfalls Content zu posten und Sie darauf zu verlinken. Kreieren Sie einen eigenen, individuellen Hashtag für Ihr Unternehmen. Und, besonders wichtig:

**Die Privatsphäre-Einstellung**
Ja, ja, es ist 2018 und eigentlich ist dieses Thema fast schon eine olle Kamelle. Doch leider kann man es nicht oft genug sagen: Überprüfen Sie regelmäßig Ihre Privatsphäre-Einstellungen und achten Sie darauf, dass die Welt nur das sieht, was Sie auch sehen soll und was das professionelle Image Ihrer Reputation unterstützt.

**Korrekte Angaben**
Achten Sie darauf, dass auf jeder Webseite, auf jedem Portal, die richtigen und aktuellen Informationen bereitgestellt sind. Dies betrifft beispielsweise Ihre Öffnungszeiten, aber auch den Standort Ihres Unternehmens bei Google Maps. Sie möchten doch schließlich nicht, dass Ihre Kunden vor verschlossener Türe oder der falschen Adresse stehen?

**Setzen Sie auf Qualität**
Wir leben in einer visuellen Welt. Photoshop und Filter legen einen perfektionistischen Schleier über die Bilderflut, die tagtäglich auf uns einprasselt. Das verändert den Blick und somit auch die Ansprüche und Erwartungen. Nehmen Sie sich Zeit und beurteilen Sie die Fotos Ihres Hotels, Ihres Restaurants mit den Augen Ihrer Gäste. Wirken Sie einladend, hell, freundlich und trotzdem authentisch? Oder gibt es hier Nachbesserungsbedarf? Die Investition in einen guten Fotografen wird sich lohnen, denn Sie können die Bilder nicht nur für Ihre eigene Webseite, sondern auch für Portale wie HolidayCheck, TripAdvisor, Facebook, Yelp und Co. nutzen und sich von den unscharfen Handyfotos Ihrer Mitbewerber abheben.

import getUserFriendlyErrorMessage from 'src/components/helper/ErrorHelper'
import { getUserFriendlyCrawlerErrorMessage } from 'src/components/api/Crawler'

describe('ErrorHelper.js', () => {
  it('creates a generic error message for an unknown error', () => {
    expect(getUserFriendlyErrorMessage(null)).to.eq('error.fallback')
  })

  it('creates an error for wrong input of a crawler url', () => {
    const url = 'https://review.thefoodguide.de/crawl'
    const startUrl = 'http://this.is.a.bad.address'
    const error = new Error()
    error.request = {
      responseURL: url,
    }
    error.response = {
      data: {
        code: 3,
        received: {
          start_url: startUrl,
          user_id: 'asdasdasdasdasd',
          location_id: 'asdasdasd',
          platform: 'google',
        },
      },
    }
    expect(getUserFriendlyCrawlerErrorMessage(error)).to.eq(`Die eingegebene Adresse ${startUrl} ist ungültig.`)
  })

  it('creates an error for a wrong password', () => {
    const error = new Error()
    error.code = 'auth/wrong-password'
    expect(getUserFriendlyErrorMessage(error)).to.eq('error.passwordMailInvalid')
  })

  it('creates a readable error when the used account is already in use', () => {
    const error = new Error()
    error.code = 'auth/credential-already-in-use'
    expect(getUserFriendlyErrorMessage(error)).to.eq('error.emailInUse')
  })
})

﻿# Wie verbessere ich meine Platzierung im Ranking der Suchmaschinen - das SEO Einmaleins

Eine berechtigte Frage, in die wir direkt mit einer Definition einsteigen.

**Was genau versteht man eigentlich unter SEO Traffic?**
SEO ist die Abkürzung für “Search Engine Optimization”, hierzulande auch bekannt als Suchmaschinenoptimierung. Oftmals wird unter SEO Traffic jener Anteil der Webseitenbesucher verstanden, der über eine Suchmaschine auf die jeweilige Seite gelangt. Diese Bezeichnung verwässert das Ergebnis jedoch, schließlich ist der SEO Traffic eine relevante Kennzahl, wenn es um das Messen des Erfolges geht. Korrekter ist es, alle Besucher, die über eine Suchmaschine auf die Seite kommen, als “Suchmaschinen-Traffic” zu bezeichnen, und die Definition für SEO Traffic, als Teil dieser großen Gruppe, enger zu fassen. Denn SEO Traffic basiert eben auch auf erfolgreicher SEO-Arbeit, also dem gezielten Optimieren der Inhalte einer Webseite, um ein besseres Ranking in den Suchergebnissen und somit mehr Nutzer zu generieren.

**Wie funktioniert SEO?**
Nun gibt es zahlreiche Tipps und schlaue Anleitungen, wie man SEO am besten einsetzt, um den organischen Traffic zu erhöhen und somit mehr Nutzer auf die eigene Seite lockt. Von Broken-Link-Building ist da die Rede und von Studien, die behaupten, das 70 % des SEO Traffics durch Longtail-Keywörter generiert werden.

Diese Strategien haben durchaus ihre Daseinsberechtigung, keine Frage. Aber es ist nun mal so, dass der Algorithmus der Suchmaschinen, allen voran Google, recht undurchsichtig ist und sich sowieso ständig ändert. Und nicht umsonst gibt es die Berufsgruppe der SEO-Manager, die darauf spezialisiert sind, die Suchmaschinenoptimierung von Webseiten zu perfektionieren. So wie Sie, als Hotelier oder Gastronom, darauf spezialisiert sind, Ihren Gästen das bestmögliche Erlebnis in Ihrem Hause zu bieten.

Sparen Sie es sich also, sich in den undurchdringlichen Dschungel der Suchmaschinenoptimierung zu stürzen, Zeit und Nutzen stehen hierbei in keinem Verhältnis – man munkelt bereits, dass das Metier der SEO-Manager eine vom Aussterben bedrohte Rasse ist. Nutzen Sie stattdessen schlicht und einfach die Ressourcen, die Ihnen zur Verfügung stehen, und setzen Sie diese effizient ein.

**SEO Traffic steigern mit Online Bewertungen**
Die Bewertungen Ihrer zufriedenen Gäste, die Sie auf öffentlichen Portalen wie Facebook, TripAdvisor, Google oder HolidayCheck erhalten, sind Content, der Ihr Unternehmen für die Suchmaschinen interessant macht. Jede neuer Bewertung ist frischer Content – Suchmaschinen lieben frischen Content. Gleichzeitig liefern Bewertungen einen großen Mehrwert für andere Nutzer, und werden von den Suchmaschinen dementsprechend als besonders relevant eingestuft.

Sie gewinnen mit hochwertigem Content also nicht nur die Suchmaschinen für sich, sondern vor allem auch die Nutzer, denen Sie wertvolle und nützliche Informationen liefern.

Am meisten profitieren Sie von dieser Tatsache, wenn Sie sich nicht nur auf die Rezensionen der großen Bewertungsportale verlassen, sondern gezielt Ihre Gäste um Bewertungen bitten, diese sammeln und auf Ihrere eigenen Webseite einbinden. Dies gelingt mit Hilfe einer XML Integration. Die Einbindung Ihrer Gästebewertungen via XML hat einen positiven Effekt auf die Auffindbarkeit Ihrer Seite im Internet, denn Suchmaschinen bewerten Webseiten, deren Inhalte regelmäßig aktualisiert und ergänzt werden, besonders gut. Dadurch, dass die Bewertungen in Ihrer eigenen Webseite integriert werden, und Ihre Gäste immer neue Bewertungen schreiben, aktualisieren Sie, ohne selbst zeit oder Geld investieren zu müssen, ständig Ihren Webseiten Inhalt. Und Google wiederum belohnt wechselnde Inhalte mit einem besseren Ranking.

Für unsere Kunden stellen wir alle benötigten Daten zur Einbindung des XML Feeds – selbstverständlich in einem individuellen, von Ihnen gestalteten Design – zur Verfügung. Entscheiden Sie selbst, welche Bewertungen, Rankings und Statistiken Sie darstellen lassen möchten. Kontaktieren Sie uns einfach und nutzen Sie Ihre Bewertungen, um Ihren SEO Traffic zu steigern. Nachdem der XML-Feed eingebunden ist, geben Sie unserem Kundenservice bitte unbedingt kurz Bescheid, damit wir das Zertifikat auf “no-index” setzen können. Somit werden die Bewertungen von den Crawlern der Suchmaschinen nur noch auf Ihrer Webseite gefunden.

**Übrigens…**
Bei “noindex” handelt es sich um einen Befehl, der in den Meta-Informationen einer Webseite untergebracht wird. Dieser Befehl verhindert, dass die jeweilige Webseite in den Index der Suchmaschinen aufgenommen wird.

Nun könnte man annehmen, dass es doch eher von Vorteil ist, die eigenen Gästebewertungen auf so vielen verschiedenen Seiten wie möglich veröffentlicht zu haben. Jein. Natürlich hat Ihre Präsenz auf den öffentlichen Plattformen und das breite Streuen Ihrer Rezensionen einen positiven Effekt auf Ihre Online Reputation. Tauchen nun aber die selben Bewertungen auf verschiedenen Webseiten auf, ist das “Duplicate Content”, also doppelt vorhandener Inhalt. Und solchen gilt es zu vermeiden, denn es ist mehr als kontraproduktiv, wenn der gleiche Content auf mehreren unterschiedlichen Webseiten dargestellt wird. Denn die Suchmaschinen filtern Duplicate Content gerne heraus und berücksichtigen ihn nicht bei der Indexierung – oder, noch schlimmer, bewerten ihn, und somit Ihre Webseite, negativ.

Suchmaschinen wie Google gehen davon aus, dass Sie bewusst doppelten Content gestreut haben, um Ihr Ranking positiv zu beeinflussen und mehr Zugriffe zu erhalten und strafen dieses Vorgehen rigoros ab. Die Folge: Statt Ihr Ranking, wie erhofft, zu steigern, werden sie in den Suchergebnissen weit weniger berücksichtigt als zuvor. Ihre potentiellen Kunden können Sie über die organische Suche nur noch bedingt finden, Ihre Seitenzugriffe nehmen ab – und schließlich auch Ihr Umsatz.

**Fazit**
Es ist also unabdingbar und – aus Unternehmenssicht – lebenswichtig, den SEO-relevanten Content, also Ihre Gästebewertungen, gezielt zu veröffentlichen und den Suchmaschinen leicht zugänglich zu machen. So steigern Sie nicht nur den SEO Traffic Ihrer Webseite, sondern investieren in Ihren Umsatz und somit in Ihre eigene Zukunft.

Gerne helfen wir Ihnen dabei, die Bewertungen Ihrer Gäste gezielt zur Steigerung Ihres SEO Traffics einzusetzen. Vereinbaren Sie einen Termin mit einem unserer Mitarbeiter, nutzen Sie unsere Lösung kostenlos und unverbindlich für 30 Tage und überzeugen Sie sich selbst!

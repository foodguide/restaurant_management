import DateHelper from 'src/components/helper/DateHelper'
import * as firebase from 'firebase'

describe('DateHelper.js', () => {
  it('can convert instagram timestamps to the correct date', () => {
    const date = new Date()
    const instagramTimestamp = date.getTime() / 1000
    const calculatedDate = DateHelper.getDateFromInstagramTimestamp(instagramTimestamp)
    expect(date - calculatedDate).to.be.below(1000) // because millisecond precision is cutted away in the instagram timestamp
  })

  it('fails in creating a date from an undefined object', () => {
    expect(DateHelper.getDateFromTimestamp(null)).to.be.null
  })

  it('should create a date from a valid string representation', () => {
    const string = 'Tue Oct 09 2018 13:45:12 GMT+0200 (CEST)'
    const date = DateHelper.getDateFromTimestamp(string)
    expect(date).to.exist
    expect(date.getDate()).to.eq(9)
    expect(date.getHours()).to.eq(13)
    expect(date.getMinutes()).to.eq(45)
    expect(date.getSeconds()).to.eq(12)
  })

  it('should return the date when the timestamp is already a date object', () => {
    const date = new Date()
    const returnedDate = DateHelper.getDateFromTimestamp(date)
    expect(date - returnedDate).to.eq(0)
  })

  it('should create a date object when a firestore timestamp is given', () => {
    const string = 'Tue Oct 09 2018 13:45:12 GMT+0200 (CEST)'
    const startDate = DateHelper.getDateFromTimestamp(string)
    const seconds = Math.abs((new Date('1970-01-01T00:00:00Z')).getTime() - startDate.getTime()) / 1000
    const fireStoreTimestamp = new firebase.firestore.Timestamp(seconds, 0)
    const date = DateHelper.getDateFromTimestamp(fireStoreTimestamp)
    expect(date).to.exist
    expect(date.getDate()).to.eq(9)
    expect(date.getHours()).to.eq(13)
    expect(date.getMinutes()).to.eq(45)
    expect(date.getSeconds()).to.eq(12)
  })
})

import uuidv1 from 'uuid/v1'
import Api from 'src/components/api/Api'
import { FacebookErrorCodes, FacebookPermissionStatus } from '../../api/Facebook'
import { RepresentationStatus } from '../repositories/LocationRepository'
import { facebook } from '../../helper/Scopes'
import { Slack } from '../../api/Slack'

const neededPermissions = [...facebook.asList(), 'public_profile']

const initialState = {
  hasCheckedPermissions: false,
  hasValidPermissions: false,
  currentUnhealthyFacebookCredential: null,
}

const state = initialState

const getters = {
  getCurrentUnhealthyFacebookCredential: state => state.currentUnhealthyFacebookCredential,
}


const mutations = {
  setPermissionCheck(state, value) {
    state.hasCheckedPermissions = value
  },
  setHasValidPermissions(state, value) {
    state.hasValidPermissions = value
  },
  setCurrentUnhealthyFacebookCredential(state, value) {
    state.currentUnhealthyFacebookCredential = value
  },
  facebookStateReset(state) {
    state.hasCheckedPermissions = false
    state.hasValidPermissions = false
    state.currentUnhealthyFacebookCredential = null
  },
}

const actions = {
  requestLongLivingAccessToken(context, payload) {
    const { credential } = payload
    return Api.backend.facebook.accessToken(credential).then((response) => {
      const credential = response.data
      if (credential.access_token) {
        credential.accessToken = credential.access_token
        delete credential.access_token
      }

      credential.platform = 'facebook'
      credential.id = uuidv1()

      return credential
    })
  },
  requestFacebookPages(context, payload) {
    return context.dispatch('checkFacebookPermissions', payload.credential)
      .then(() => Api.facebook.getPages(payload.credential))
  },
  requestFacebookAccount(context, payload) {
    const { credential } = payload
    return Api.backend.facebook.account(credential)
  },
  requestFacebookLocations(context, payload) {
    return context.dispatch('checkFacebookPermissions', payload)
      .then(() => Api.facebook.getLocations(payload.accessToken, payload.id))
  },
  requestFacebookLocationsForPages(context, pages) {
    return Promise.all(pages.map(page => context.dispatch('requestFacebookLocations', { accessToken: page.access_token, id: page.id })))
      .reduce((previous, current) => (previous.data ? previous.data.concat(current.data) : previous.concat(current.data)))
  },
  requestFacebookReviews(context, location) {
    return Api.backend.sync.apiForLocation(location.id, ['facebook'])
  },
  checkFacebookPermissions(context, credential) {
    const invalidPermissionError = createInvalidPermissionError()
    if (context.state.hasCheckedPermissions) {
      return context.state.hasValidPermissions ? Promise.resolve() : Promise.reject(invalidPermissionError)
    }
    return Api.facebook.getPermissions(credential).then((permissions) => {
      if (!permissions) return Promise.reject(createNoPermissionsFoundError())
      const declinedPermissions = getDeclinedPermissions(permissions)
      const hasValidPermissions = declinedPermissions.length === 0
      context.commit('setHasValidPermissions', hasValidPermissions)
      context.commit('setPermissionCheck', true)
      if (!hasValidPermissions) {
        context.commit('setCurrentUnhealthyFacebookCredential', credential)
        invalidPermissionError.declinedPermissions = declinedPermissions
        return Promise.reject(invalidPermissionError)
      }
      return Promise.resolve()
    }).catch((error) => {
      const userId = context.rootGetters.getCurrentUserId
      Slack.logText(`User ${userId} has no valid Facebook authorization for Respondo: ${JSON.stringify(credential, null, 2)}`)
      context.commit('setPermissionCheck', true)
      context.commit('setCurrentUnhealthyFacebookCredential', credential)
      return Promise.reject(error)
    })
  },
}

export default {
  state,
  getters,
  actions,
  mutations,
}

const createInvalidPermissionError = () => {
  const invalidPermissionError = new Error('The connection to facebook has declined permissions.')
  invalidPermissionError.platform = 'facebook'
  invalidPermissionError.code = FacebookErrorCodes.MISSING_PERMISSIONS
  invalidPermissionError.newStatus = RepresentationStatus.PERMISSION_DECLINED
  return invalidPermissionError
}

const createNoPermissionsFoundError = () => {
  const invalidPermissionError = new Error('No Facebook permissions found.')
  invalidPermissionError.platform = 'facebook'
  invalidPermissionError.code = FacebookErrorCodes.CUSTOM_ERROR
  invalidPermissionError.newStatus = RepresentationStatus.PERMISSION_DECLINED
  invalidPermissionError.userFriendlyMessage = 'error.reauthenticate.facebook.noPermissions'
  return invalidPermissionError
}

const getDeclinedPermissions = (permissions) => {
  const declinedPermissions = permissions
    .filter(permission => permission.status === FacebookPermissionStatus.DECLINED)
    .filter(permission => permission.permission !== 'email')
  if (permissions.length !== neededPermissions.length) {
    const excludedPermissions = neededPermissions.filter(permission => !permissions.find(p => p.permission === permission))
    excludedPermissions.forEach((excluded) => {
      declinedPermissions.push({ permission: excluded, status: FacebookPermissionStatus.DECLINED })
    })
  }
  return declinedPermissions
}

import { flatMap } from 'src/components/helper/ArrayHelper'

describe('ArrayHelper.js', () => {
  it('can flatmap nested arrays', () => {
    const nested = [[1, 2], [2, 3, 5]]
    const flattenend = flatMap(element => element, nested)
    expect(flattenend.length).to.eq(5)
    expect(flattenend).to.deep.eq([1, 2, 2, 3, 5])
  })
})

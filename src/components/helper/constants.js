export default {
  no_rating: 0,
  bad_rating: 1,
  good_rating: 2,
  star_rating: 3,
}


export const TaskEnum = Object.freeze({
  new: 0,
  user_notified: 1,
  reply_received: 2,
  reply_sent: 3,
  reply_confirmation: 4,
  no_reply: 5,
})

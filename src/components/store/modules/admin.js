import Vue from 'vue'
import Api from '../../api/Api'
import CrawlerRepository from '../repositories/CrawlerRepository'
import LocationRepository from '../repositories/LocationRepository'
import ReviewRepository from '../repositories/ReviewRepository'
import EventRepository from '../repositories/EventRepository'
import DirectTaskRepository from '../repositories/DirectTaskRepository'
import QuickReplyRepository from '../repositories/QuickReplyRepository'
import DirectUserRepository from '../repositories/DirectUserRepository'

const state = {
  currentUser: {
    isLoggedIn: false,
  },
  users: [],
  isLoadingUsers: true,
  crawlerTasks: [],
  directTasks: [],
  directUsers: [],
  locations: [],
  reviews: {},
  events: {},
  quickReplies: {},
}

const getters = {
  isLoggedIn: state => state.currentUser.isLoggedIn,
  isLoadingUsers: state => state.isLoadingUsers,
  users: state => state.users,
  crawlerTasks: state => state.crawlerTasks,
  doneCrawlerTasks: state => state.crawlerTasks.filter(task => task.state === 'DONE'),
  notDoneCrawlerTasks: state => state.crawlerTasks.filter(task => task.state !== 'DONE'),
  zeroProgressCrawlerTasks: state => state.crawlerTasks.filter(task => task.state !== 'DONE').filter(task => task.progress === 0),
  locations: state => state.locations,
  adminReviews: state => state.reviews,
  adminEvents: state => state.events,
  adminQuickReplies: state => state.quickReplies,
  directTasks: state => state.directTasks,
  directUsers: state => state.directUsers,
}
const mutations = {
  login(state) {
    state.currentUser.isLoggedIn = true
  },
  logout(state) {
    state.currentUser.isLoggedIn = false
  },
  fetchUsers(state, payload) {
    state.users = payload
    state.isLoadingUsers = false
  },
  fetchDirectUsers(state, payload) {
    state.directUsers = payload
  },
  onCrawlerTasks(state, tasks) {
    state.crawlerTasks = tasks
  },
  updateLocationsAndRepresentations(state, payload) {
    state.locations.push(payload)
  },
  fetchReviewsForLocation(state, payload) {
    Vue.set(state.reviews, payload.locationId, payload.reviews)
  },
  fetchEventsForLocation(state, payload) {
    Vue.set(state.events, payload.userId, payload.events)
  },
  fetchQuickRepliesForUser(state, payload) {
    Vue.set(state.quickReplies, payload.userId, payload.quickReplies)
  },
  updatePipedriveId(state, payload) {
    state.users.find(user => user.id === payload.userId).pipedriveId = payload.pipedriveId
  },
  onDirectTask(state, task) {
    const shouldNotBeShown = (task.review.response || task.state === 4 || task.state === 5)
    const index = state.directTasks.findIndex(directTask => directTask.id === task.id)
    if (index >= 0) {
      if (shouldNotBeShown) {
        // Don't show in Admin area if one of the final states is reached.
        Vue.delete(state.directTasks, index)
      } else {
        Vue.set(state.directTasks, index, task)
      }
    } else {
      if (!shouldNotBeShown) {
        Vue.set(state.directTasks, state.directTasks.length, task)
      }
    }
  },
}

const crawlerRepository = new CrawlerRepository()
const locationRepository = new LocationRepository()
const reviewRepository = new ReviewRepository()
const eventRepository = new EventRepository()
const directTaskRepository = new DirectTaskRepository()
const quickReplyRepository = new QuickReplyRepository()
const directUserRepository = new DirectUserRepository()

const actions = {
  fetchUsers(context) {
    return Api.backend.admin.getUsers().then((users) => {
      context.commit('fetchUsers', users)
      return users
    })
  },
  listenOnAllCrawlerTasks(context) {
    crawlerRepository.listenOnAllCrawlerTasks((tasks) => {
      context.commit('onCrawlerTasks', tasks)
    })
  },
  fetchDirectUsers(context) {
    return directUserRepository.findAllForAdmin().then(users => context.commit('fetchDirectUsers', users))
  },
  listenOnDirectTasks(context) {
    context.getters.directUsers.forEach((user) => {
      directTaskRepository.addDirectTaskListener(user.userId, (data) => {
        reviewRepository.findById(data.reviewId).then((review) => {
          const directTask = {
            ...data,
            review: review[0],
          }
          context.commit('onDirectTask', directTask)
        })
      })
    })
  },
  updateDirectTaskState(context, task) {
    const newState = task.state + 1
    directTaskRepository.update(task.id, { state: newState })
  },
  dontReplyDirectTask(context, task) {
    directTaskRepository.update(task.id, { state: 5 })
  },
  updateDirectTaskRepliedDate(context, task) {
    directTaskRepository.update(task.id, { replied: new Date() })
  },
  replyOnDirectTask(context, { task, text }) {
    return Api.backend.admin.directReply({
      reviewId: task.reviewId,
      userId: task.userId,
      text,
    })
  },
  fetchLocations(context) {
    return locationRepository.findAllForAdmin().then(locations => Promise.all(locations.map(location => locationRepository.getRepresentationByLocationId(location.id).then((data) => {
      location.representations = data
      context.commit('updateLocationsAndRepresentations', location)
    }))))
  },
  fetchReviewsForLocation(context, payload) {
    const { locationId } = payload

    if (state.reviews[locationId]) {
      return Promise.resolve()
    }

    return reviewRepository.findAllForLocation(locationId).then((data) => {
      context.commit('fetchReviewsForLocation', {
        locationId,
        reviews: data,
      })
    })
  },
  fetchEventsForUser(context, payload) {
    const { userId } = payload

    if (state.events[userId]) {
      return Promise.resolve()
    }

    return eventRepository.findAllForUser(userId).then((data) => {
      context.commit('fetchEventsForLocation', {
        userId,
        events: data,
      })
    })
  },
  fetchQuickRepliesForUser(context, payload) {
    const { userId } = payload

    if (state.events[userId]) {
      return Promise.resolve()
    }

    return quickReplyRepository.findAllForUser(userId).then((data) => {
      context.commit('fetchQuickRepliesForUser', {
        userId,
        quickReplies: data,
      })
    })
  },
  checkInitialLoginStatus(context) {
    return new Promise((resolve) => {
      Api.user.observeAdminStatus((response) => {
        if (response.isLoggedIn && response.idTokenResult.claims.admin) {
          context.commit('login')
          resolve({ isLoggedIn: true })
          return
        }
        const optionalLogout = response.isLoggedIn ? context.commit('logout') : Promise.resolve()
        optionalLogout.then(() => {
          context.commit('logout')
          resolve({ isLoggedIn: false })
        })
      })
    })
  },
  login(context, payload) {
    return new Promise((resolve, reject) => {
      Api.user.login(payload.email, payload.password).then(() => {
        Api.user.getIdTokenResult().then((idTokenResult) => {
          if (!idTokenResult.claims.admin) {
            context.dispatch('logout').then(() => {
              reject(new Error('This user is not an admin.'))
            })
          } else {
            context.commit('login')
            resolve()
          }
        })
      })
    })
  },
  logout(context) {
    return Api.user.logout().then(() => {
      context.commit('logout')
    })
  },
  updatePipedriveId(context, payload) {
    return Api.backend.admin.updatePipedriveId(payload.userId, payload.pipedriveId).then(() => context.commit('updatePipedriveId', payload))
  },
  bulkUpdateReviews(context, payload) {
    return Api.backend.admin.bulkUpdateReviews(payload)
  },
  genericSend(context, url) {
    return Api.backend.admin.genericSend(url)
  },
  resyncRepresentation(context, payload) {
    return Api.backend.admin.resyncRepresentation(payload)
  },
  bulkDeleteReviews(context, payload) {
    return Api.backend.admin.bulkDeleteReviews(payload)
  },
  sendNewsletter(context, payload) {
    return Api.backend.admin.sendNewsletter(payload)
  },
}

export default {
  state,
  getters,
  mutations,
  actions,
}

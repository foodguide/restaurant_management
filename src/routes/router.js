import { HookEvent, HookListener } from '@/components/store/modules/hooks'

export const isPathOf = (path, pathComponent) => path.startsWith(`/${pathComponent}`) || path.startsWith(pathComponent)

export const isPathOneOf = (path, ...pathComponents) => {
  while (pathComponents.length > 0) {
    const pathComponent = pathComponents.pop()
    const isPath = isPathOf(path, pathComponent)
    if (isPath) return true
  }
  return false
}

const isAppPath = path => path.matched.some(record => record.meta.isApplication)

const isErrorPath = path => isPathOneOf(path, '/error')

const isLoginPath = path => isPathOneOf(path, '/auth/login', '/auth/register')

const isOnboardingPath = path => isPathOf(path, '/onboarding')

const isPaymentPath = path => isPathOneOf(path, '/payment', '/test')

const isEmailPath = path => isPathOneOf(path, '/email')

/**
 *
 * This const is a function that looks if the user status is known or unknown at first.
 * When it is know it returns the login state and otherwise it subscribes for the login
 * status hook. This difference between known and unknown is hided away because this
 * function always returns a Promise with a boolean that represents the known login state.
 *
 * @param store A vuex store
 * @returns {Promise<boolean>}
 */
const onDeterminedLoginStatus = store => new Promise((resolve) => {
  if (!store.getters.isUnknown) resolve(store.getters.isLoggedIn)
  else {
    const listener = new HookListener({
      hookId: HookListener.Identifier.LoginStatus,
      id: 'onDeterminedLoginStatus-router.js',
      /**
       *
       * @param {HookEvent} event
       */
      handler: (event) => {
        resolve(event.payload.isLoggedIn)
      },
    })
    store.commit('addHookListener', listener)
  }
})

/**
 *
 * This function starts with looking if the current state for a payed subscription has been made.
 * If so, it returns the state for the active payment as boolean inside a Promise. Otherwise
 * it will subscribe for the hook that fires when the subscription status is known and then returns
 * the state as a boolean inside a Promise.
 *
 * @param store
 * @returns {Promise<boolean>}
 */
const onDeterminedSubscriptionStatus = store => new Promise((resolve) => {
  if (store.getters.hasRequestedSubscriptionStatus) resolve(store.getters.hasActivePayment)
  else {
    const listener = new HookListener({
      hookId: HookListener.Identifier.SubscriptionStatus,
      id: 'onDeterminedSubscriptionStatus-router.js',
      /**
                 *
                 * @param {HookEvent} event
                 */
      handler: (event) => {
        resolve(event.payload.activeSubscriptionsResult)
      },
    })
    store.commit('addHookListener', listener)
  }
})

/**
 *
 * This addGuard function adds special logic to the vue-router for every navigation inside this app.
 * All pages that have an url that starts with /app should only be visitable by logged in and paying users.
 *
 * @param router
 * @param store
 */
export const addGuard = (router, store) => {
  router.beforeEach((to, from, next) => {
    // General routing

    // If the user wants to navigate to any page outside of app, the routing should NOT be adjusted
    if (!isAppPath(to)) {
      next()
      return
    }

    // App routing

    // If error page should be shown, we don't need to check logic for other pages
    if (isErrorPath(to.path)) {
      next()
      return
    }

    // If user is already logged in and wants to navigate on login or onboarding page, we forward him to app page
    if (isLoginPath(to.path) || isOnboardingPath(to.path)) {
      return onDeterminedLoginStatus(store).then((isLoggedIn) => {
        if (!isLoggedIn) {
          next()
        } else {
          // If user is logged in and still in onboarding process, keep them there
          if (isOnboardingPath(from.path)) {
            next()
          } else {
            next({ path: '/app', replace: true })
          }
        }
      })
    }

    // If user wants to navigate to payment page, the routing should NOT be adjusted
    if (isPaymentPath(to.path)) {
      next()
      return
    }

    if (isEmailPath(to.path)) {
      next()
      return
    }

    return onDeterminedLoginStatus(store).then((isLoggedIn) => {
      if (!isLoggedIn) {
        // If user wants to access app page and is NOT logged in, we forward him to login page
        next({ path: '/auth/login', replace: true })
      } else {
        return onDeterminedSubscriptionStatus(store).then((hasActiveSubscription) => {
          if (store.getters.hasOnlyFacebookProvider) {
            next({ path: '/email', replace: true })
          } else if (!hasActiveSubscription)
          // User is logged in and has NO active payment, we forward him to the payment page
          { next({ path: '/payment', replace: true }) } else
          // User is logged in and has active payment, we forward him to the desired app page
          { next() }
        })
      }
    })
  })
}

/**
 *
 * This addRunningApplicationGuard function adds special logic to the vue-router for every navigation inside this app.
 * All pages that have an url which indicates to be part of the Respondo application and
 * not its area around (Landing, LearnMore, AGB, Imprint, ...), are checked if the application is running.
 * Otherwise an error page will be shown.
 *
 * @param router
 * @param store
 */
export const addRunningApplicationGuard = (router, store) => {
  router.beforeEach((to, from, next) => {
    if (isAppPath(to) && store.getters.hasApplicationError) {
      if (!isErrorPath(to.path)) {
        next({ path: '/error' })
        return
      }
      next()
    }

    next()
  })
}

const loginPush = (router) => {
  // When the login or the registration happens during the onboarding process
  // we don't want the redirect behaviour. This will be done manually during the onboarding
  router.onReady(() => {
    if (isPathOneOf(router.currentRoute.path, 'onboarding', 'unsubscribe')) return
    if (isAppPath(router.currentRoute)) router.replace('/app')
  })
}

const replaceWithPayment = (router) => {
  // When is user is on onboarding page, the request for payment status could have been done anyway in the background.
  // In this case DON'T redirect the user, so he could continue onboarding
  if (!isOnboardingPath(router.currentRoute.path)) router.replace('/payment')
}

const logoutPush = (router) => {
  console.log('logout')
  if (isAppPath(router.currentRoute)) router.replace('/start')
}

export const subscribeToStore = (router, store) => {
  store.subscribe((mutation) => {
    // Store mutations should NOT affect user who navigates on homepage part
    if (!isAppPath(router.currentRoute)) {
      return
    }

    if (mutation.type === 'login') {
      loginPush(router)
    } else if (mutation.type === 'setLoginStatus') {
      if (!mutation.payload) {
        if (isPathOneOf(router.currentRoute.path, 'onboarding', 'unsubscribe')) return
        router.replace('/start')
      }
    } else if (mutation.type === 'logout') {
      logoutPush(router)
    } else if (mutation.type === 'setActiveSubscriptions') {
      mutation.payload ? loginPush(router) : replaceWithPayment(router)
    }
  })
}

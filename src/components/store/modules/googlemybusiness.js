import Api from '../../api/Api'

/**
 * If the user has already stored credentials, the server should handle these credentials via userId and credentialId.
 * Otherwise the credentials are submitted directly.
 *
 * Therefor we need to check if credentials are already stored.
 * @param context
 * @param credential
 * @returns {boolean}
 */
const isStoredCredential = (context, credential) => {
  const userCredentials = context.getters.getCredentials
  return userCredentials.findIndex(userCredential => userCredential.id === credential.id) >= 0
}

const actions = {
  requestGoogleMyBusinessAccounts(context, { credential }) {
    return (isStoredCredential(context, credential))
      ? Api.backend.gmb.accountsForUser(context.getters.getCurrentUserId, credential.id)
      : Api.backend.gmb.accounts(credential)
  },
  requestGoogleMyBusinessLocations(context, { credential }) {
    return (isStoredCredential(context, credential))
      ? Api.backend.gmb.locationsForUser(context.getters.getCurrentUserId, credential.id)
      : Api.backend.gmb.locations(credential)
  },
  requestGoogleMyBusinessReviews(context, location) {
    return Api.backend.sync.apiForLocation(location.id, ['google'])
  },
}

export default {
  actions,
}

var utils = require('./utils')
var webpack = require('webpack')
var config = require('../config')
var merge = require('webpack-merge')
var baseWebpackConfig = require('./webpack.base.conf')
var HtmlWebpackPlugin = require('html-webpack-plugin')
var FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin')
let WebappWebpackPlugin = require('webapp-webpack-plugin')
const VueLoaderPlugin = require('vue-loader/lib/plugin')

// add hot-reload related code to entry chunks
Object.keys(baseWebpackConfig.entry).forEach(function (name) {
    baseWebpackConfig.entry[name] = ['./build/dev-client'].concat(baseWebpackConfig.entry[name])
})

var devWebpackConfig = merge(baseWebpackConfig, {
    mode: 'development',
    module: {
        rules: utils.styleLoaders({sourceMap: config.dev.cssSourceMap})
    },
    // cheap-module-eval-source-map is faster for development
    devtool: '#cheap-module-eval-source-map',
    plugins: [
        new webpack.DefinePlugin({
            'process.env': config.dev.env
        }),
        new VueLoaderPlugin(),
        // https://github.com/glenjamin/webpack-hot-middleware#installation--usage
        new webpack.HotModuleReplacementPlugin(), new webpack.NoEmitOnErrorsPlugin(),
        // https://github.com/ampedandwired/html-webpack-plugin
        new HtmlWebpackPlugin({
            chunks: ['app'],
            filename: 'index.html',
            template: 'index.html'
        }),
        new HtmlWebpackPlugin({
            chunks: ['admin'],
            filename: 'admin.html',
            template: 'admin.html'
        }),
        new WebappWebpackPlugin({
            logo: './src/assets/respondo_logo_only_square.svg',
            cache: true,
            inject: true,
        }),
        new FriendlyErrorsPlugin()
    ]
})

devWebpackConfig.entry = {
    app: './src/main.js',
    admin: './src/admin.js'
}

module.exports = devWebpackConfig

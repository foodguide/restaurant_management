export default class ReviewHelper {
  static getReviewerName(review) {
    if (!review) {
      return null
    }
    if (!review.reviewer || !review.reviewer.name) {
      return null
    }
    return review.reviewer.name
  }
}

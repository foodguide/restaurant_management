// At first we setup things not related to vue
// install jQuery
// Then we setup vue and start installation
import Vue from 'vue/dist/vue.esm.js'

// Load styling files
import 'bootstrap/dist/css/bootstrap.css'
import './assets/sass/paper-dashboard.scss'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import 'vue-tour/dist/vue-tour.css'

// Installing plugins

// Setup Vuex Store
import Vuex from 'vuex'
import storeOptions from 'components/store/index.js'

// Setup Vue Router
import VueRouter from 'vue-router'
import { installErrorLogging } from 'components/debug/ErrorLogger'
import VueI18n from 'vue-i18n'
import axios from 'axios'
import { directive as vClickOutside } from 'vue-clickaway'
import routes from './routes/routes'

// Import and call additional setup routines
import { installPlugins, installPluginsWithRouter } from './plugins'

import { addGuard, subscribeToStore, addRunningApplicationGuard } from './routes/router'

// Main Component
import App from './App'
import { messages } from './lang/en'


window.$ = require('jquery')
window.JQuery = require('jquery')

// Overwriting the Promise class from the standard lib with the implementation from Bluebird
Promise = require('bluebird')

Promise.config({
  // Enables all warnings except forgotten return statements.
  warnings: {
    wForgottenReturn: false,
  },
})

// Adding service worker for precaching
if (process.env.NODE_ENV === 'production' && 'serviceWorker' in navigator) {
  navigator.serviceWorker.register('/service-worker.js')
}
installPlugins(Vue)
const store = new Vuex.Store(storeOptions)
if (window.Cypress) {
  window.__store__ = store
}
const router = new VueRouter({
  routes,
  linkActiveClass: 'active',
})
installPluginsWithRouter(Vue, router)
installErrorLogging(Vue, store)
addRunningApplicationGuard(router, store)
addGuard(router, store)
subscribeToStore(router, store)

// Setup Vue directives
Vue.directive('click-outside', vClickOutside)

// Setup i18n
const defaultLocale = 'en'
Vue.use(VueI18n)
const i18n = new VueI18n({
  locale: defaultLocale,
  fallbackLocale: defaultLocale,
  messages,
})
const loadedLanguages = [defaultLocale]
const supportedLanguages = [defaultLocale, 'de']

function setI18nLanguage(lang) {
  i18n.locale = lang
  axios.defaults.headers.common['Accept-Language'] = lang
  document.querySelector('html').setAttribute('lang', lang)
  return lang
}

export function loadLanguageAsync(lang) {
  // If the same language
  if (i18n.locale === lang) {
    return Promise.resolve(setI18nLanguage(lang))
  }

  // If language is supported
  if (!supportedLanguages.includes(lang)) {
    return Promise.resolve()
  }

  // If the language was already loaded
  if (loadedLanguages.includes(lang)) {
    return Promise.resolve(setI18nLanguage(lang))
  }

  // If the language hasn't been loaded yet
  return import(/* webpackChunkName: "lang-[request]" */ `./lang/${lang}.js`).then(
    (newMessages) => {
      i18n.setLocaleMessage(lang, newMessages.messages[lang])
      loadedLanguages.push(lang)
      return setI18nLanguage(lang)
    },
  )
}

export const createRootComponent = (el, component) => new Vue({
  i18n,
  el,
  store,
  render: h => h(component),
  router,
})

// Setting up root component
const main = createRootComponent('#app', App)
// export default main
export const { Main } = main

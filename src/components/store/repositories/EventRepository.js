import BaseRepository from './BaseRepository'

class EventRepository extends BaseRepository {
  get managedCollection() {
    return 'event'
  }

  get documentAttributes() {
    return ['name', 'type', 'timestamp', 'data', 'userId']
  }

  addEvent(data) {
    if (!this.getCurrentUserUid()) return

    try {
      data.userId = this.getCurrentUserUid()
      data.timestamp = Date.now()
      const document = this.filterAllowedAttributes(data)
      return this.create(document)
    } catch (error) {}
  }
}

export default EventRepository

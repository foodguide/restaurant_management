import { mapGetters } from 'vuex'
import DateHelper from '../../../helper/DateHelper'
import IconResolver from '../../../helper/IconResolver'

export default {
  props: {
    statisticsData: {
      type: Object,
      required: true,
    },
    representations: {
      type: Array,
      default() {
        return []
      },
    },
  },
  computed: {
    ...mapGetters({
      allPlatforms: 'getAllPlatforms',
    }),
    representationsAndStatisticsAvailable() {
      return !!this.statisticsData
        && !!this.representations
        && Reflect.ownKeys(this.statisticsData).length > 0
        && this.representations.length > 0
    },
    includedRepresentations() {
      const filter = representation => !this.excludedPlatforms.find(platform => representation.platform === platform)
      return this.representations.filter(filter)
    },
    timeline() {
      if (!this.representationsAndStatisticsAvailable || !this.randomRepresentationId) return null
      const mapper = stat => DateHelper.getDateFromTimestamp(stat.date).toLocaleDateString()
      return this.statisticsData[this.randomRepresentationId].map(mapper)
    },
    randomRepresentationId() {
      const keys = Reflect.ownKeys(this.statisticsData)
      if (keys.length === 0) return null
      return keys[0]
    },
    defaultValues() {
      if (!this.representationsAndStatisticsAvailable) return null
      const series = []
      Reflect.ownKeys(this.statisticsData).forEach((representationId) => {
        if (!this.isRepresentationIncluded(representationId)) return
        const data = this.getData(this.statisticsData[representationId])
        const representation = this.representations.find(repr => repr.id === representationId)
        const platform = this.allPlatforms.find(platf => platf.name === representation.platform)
        series.push({
          data,
          id: platform.id,
          name: platform.displayName,
          type: 'line',
          color: IconResolver.getPlatformColorByName(platform.name),
        })
      })

      if (series.length > 1) {
        series.push({
          id: 0,
          name: 'Gesamt',
          data: this.getOverallData({
            series,
            statisticsData: this.statisticsData,
          }),
          type: 'line',
          color: '#000',
        })
      }

      return series
    },
  },
  methods: {
    isRepresentationIncluded(representationId) {
      return this.includedRepresentations.findIndex(({ id }) => id === representationId) > -1
    },
  },
}

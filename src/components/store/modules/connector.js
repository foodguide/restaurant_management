import Vue from 'vue'
import { EventName, EventType } from './event'

const state = {
  location: null,
  continueConnectionAfterLogin: false,
  currentConnectionPlatform: null,
  createLocationAfterRegistration: false,
  writingPromise: null,
  credentials: [],
}

const getters = {
  getTemporaryLocation: state => state.location,
  getContinueConnectionAfterLogin: state => state.continueConnectionAfterLogin,
  getCurrentConnectionPlatform: state => state.currentConnectionPlatform,
  getCreateLocationAfterRegistration: state => state.createLocationAfterRegistration,
  getTemporaryGoogleCredentials: state => state.credentials.filter(credential => credential.platform === 'google'),
  getTemporaryFacebookCredentials: state => state.credentials.filter(credential => credential.platform === 'facebook'),
}

const writeRepresentation = (context, name, data) => {
  context.dispatch('writeEvent', { name: EventName.CreateRepresentationStart, type: EventType.Action, data: { data } })
  if (!state.location.name) {
    context.commit('setTemporaryLocationName', name)
  }
  context.commit('addTemporaryRepresentation', data)
}

const createOpenGraphRepresentation = (page, isInstagram = false) => ({
  externalId: page.id,
  name: page.label,
  accessToken: page.accessToken,
  credentialId: page.credentialId,
  type: 'api',
  platform: isInstagram ? 'instagram' : 'facebook',
  platformShortcut: isInstagram ? 'ig' : 'fb',
  info: {// TODO
    name: page.label,
    description: null,
    address: null,
  },
})

const writeInstagramRepresentation = (context, page) => {
  const representation = createOpenGraphRepresentation(page, true)
  writeRepresentation(context, page.label, representation)
}

function writeFacebookRepresentation(context, page) {
  const representation = createOpenGraphRepresentation(page)
  writeRepresentation(context, page.label, representation)
}

function writeGoogleRepresentation(context, payload) {
  const representationData = payload.representation

  const representation = {
    externalId: representationData.name,
    name: representationData.locationName,
    type: 'api',
    platform: 'google',
    platformShortcut: 'go',
    credentialId: representationData.credentialId,
    info: {
      name: representationData.locationName,
      address: representationData.address,
    },
  }
  writeRepresentation(context, representation.info.name, representation)
}

function writeCrawlerRepresentation(context, payload, platform, shouldAddName = true) {
  const representation = {
    url: payload.url,
    platform: platform.name,
    type: platform.type,
    platformShortcut: platform.shortcut,
  }
  writeRepresentation(context, payload.name, representation)
}

function writeNewCredentials(context) {
  const existingCredentials = context.getters.getCredentials
  let credentialsToAdd = state.credentials
  if (existingCredentials.length > 0) {
    const existingCredentialIds = existingCredentials.map(credential => credential.id)
    credentialsToAdd = credentialsToAdd.filter(credential => existingCredentialIds.indexOf(credential) < 0)
  }
  credentialsToAdd.forEach((credential) => {
    context.dispatch('createCredential', credential)
  })
}

const mutations = {
  setTemporaryLocation(state, payload) {
    if (!payload) payload = {}
    state.location = payload
  },
  setTemporaryLocationName(state, payload) {
    state.location.name = payload
  },
  clearTemporaryLocation(state) {
    state.location = null
    state.continueConnectionAfterLogin = false
    state.currentConnectionPlatform = null
    state.createLocationAfterRegistration = false
    state.writingPromise = null
    state.credentials = []
  },
  addTemporaryRepresentation(state, payload) {
    const { location } = state
    if (!location.representations) location.representations = []
    location.representations.push(payload)
    state.location = Object.assign({}, state.location, location)
  },
  addTemporaryCredential(state, payload) {
    state.credentials.push(payload)
  },
  updateTemporaryCredential(state, payload) {
    const index = state.credentials.findIndex(credential => credential.id === payload.id)
    if (index < 0) return
    const credential = state.credentials[index]
    credential.account = payload.account
    Vue.set(state.credentials, index, credential)
  },
  setContinueConnectionAfterLogin(state, payload) {
    state.continueConnectionAfterLogin = payload
  },
  setCurrentConnectionPlatform(state, payload) {
    state.currentConnectionPlatform = payload
  },
  setCreateLocationAfterRegistration(state, payload) {
    state.createLocationAfterRegistration = payload
  },
  setLocationWritingPromise(state, payload) {
    state.writingPromise = payload
  },
}

const actions = {
  addTemporaryRepresentation(context, payload) {
    const { platform } = payload
    switch (platform.name) {
      case 'facebook': {
        writeFacebookRepresentation(context, payload.representation)
        break
      }
      case 'instagram': {
        writeInstagramRepresentation(context, payload.representation)
        break
      }
      case 'yelp':
      case 'tripadvisor':
      case 'opentable': {
        writeCrawlerRepresentation(context, payload.representation, platform)
        break
      }
      case 'google': {
        writeGoogleRepresentation(context, payload)
        break
      }
      default:
        break
    }
  },
  createLocationAndRepresentations(context) {
    if (context.state.writingPromise) return context.state.writingPromise
    const writingPromise = context.dispatch('createLocation', { name: state.location.name }).then((location) => {
      const promises = state.location.representations.map((representation) => {
        representation.locationId = location.id
        return context.dispatch('addRepresentationToLocation', representation)
      })
      return Promise.all(promises)
    })
    context.commit('setLocationWritingPromise', writingPromise)
    writeNewCredentials(context)
    return writingPromise
  },
  createRepresentationsWithoutIds(context, payload) {
    writeNewCredentials(context)
    return Promise.all(state.location.representations.map((representation) => {
      if (representation.id) return Promise.resolve()
      representation.locationId = payload.id
      return context.dispatch('addRepresentationToLocation', representation)
    })).then(representations => representations.filter(rep => !!rep))
  },
}

export default {
  state,
  getters,
  actions,
  mutations,
}

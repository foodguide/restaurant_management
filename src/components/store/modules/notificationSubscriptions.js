import Api from 'components/api/Api'

const state = {
  guestNotificationSubscriptions: [],
}

const getters = {
  guestNotificationSubscriptions: state => state.guestNotificationSubscriptions,
}

const mutations = {
  setGuestNotificationSubscriptions(state, guestNotificationSubscriptions) {
    state.guestNotificationSubscriptions = guestNotificationSubscriptions
  },
}

const actions = {
  fetchGuestNotificationSubscriptions(context, payload) {
    return Api.backend.user.guestNotificationSubscriptions({ userId: payload }).then((response) => {
      context.commit('setGuestNotificationSubscriptions', response)
    })
  },
  updateNotificationSubscriptions(context, payload) {
    return Api.backend.user.updateGuestNotificationSubscriptions({
      userId: payload.userId,
      subscriptions: payload.subscriptions,
    })
  },
}

export default {
  state,
  getters,
  actions,
  mutations,
}

const state = {
  tooltips: [
    {
      id: 'review-stats',
      message: 'tooltips.reviewStats',
    },
    {
      id: 'stats-average',
      message: 'tooltips.statsAverage',
    },
    {
      id: 'stats-growth',
      message: 'tooltips.statsGrowth',
    },
    {
      id: 'stats-distribution-platform',
      message: 'tooltips.statsDistributionPlatform',
    },
    {
      id: 'stats-distribution-rating',
      message: 'tooltips.statsDistirbutionRating',
    },
    {
      id: 'settings-payment',
      message: 'tooltips.settingsPayment',
    },
    {
      id: 'settings-facebook-mail',
      message: 'tooltips.settingsFacebookMail',
    },
    {
      id: 'settings-facebook-password',
      message: 'tooltips.settingsFacebookPassword',
    },
  ],
  tooltipsEnabled: true,
}

const getters = {
  getTooltipTextById: state => id => state.tooltips.find(tooltip => tooltip.id === id),
  isTooltipsEnabled: state => state.tooltipsEnabled,
}

export default {
  state,
  getters,
}

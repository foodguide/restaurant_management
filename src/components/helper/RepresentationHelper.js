export default class RepresentationHelper {
  static canReply(platform, representation) {
    if (platform === 'facebook' || platform === 'google' || platform === 'instagram') {
      return true
    }
    return representation.canReply
  }
}

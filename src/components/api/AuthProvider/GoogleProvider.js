import { Firebase } from '../Firebase'

const provider = new Firebase.auth.GoogleAuthProvider()
process.env.GOOGLE_SCOPES.split(' ').forEach(scope => provider.addScope(scope))
export default provider

export const googleCredential = (idToken, accessToken) => Firebase.auth.GoogleAuthProvider.credential(idToken, accessToken)

export var facebook = {}
facebook.asList = () => facebookPermissions
facebook.asString = () => facebookPermissions.join(', ')

const facebookPermissions = [
  'manage_pages',
  'publish_pages',
  'pages_show_list',
  'instagram_basic',
  'instagram_manage_comments',
]

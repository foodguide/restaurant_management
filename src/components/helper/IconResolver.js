import facebookLogo from '../../assets/platforms/facebook-logo.svg'
import instagramLogo from '../../assets/platforms/instagram-logo.svg'
import googleLogo from '../../assets/platforms/google-logo.svg'
import yelpLogo from '../../assets/platforms/yelp-logo.svg'
import tripLogo from '../../assets/platforms/tripadvisor.svg'
import opentableLogo from '../../assets/platforms/opentable-logo.svg'

const iconResolver = {}

iconResolver.getPlatformIconById = function (platformId) {
  switch (platformId) {
    case 1:
      return facebookLogo
    case 2:
      return instagramLogo
    case 3:
      return googleLogo
    case 4:
      return yelpLogo
    case 5:
      return tripLogo
    case 6:
      return opentableLogo
    default:
      return ''
  }
}
iconResolver.getPlatformIcon = function (platform) {
  return iconResolver.getPlatformIconById(platform.id)
}
iconResolver.getPlatformIconByName = (name) => {
  switch (name.toLowerCase()) {
    case 'facebook':
      return facebookLogo
    case 'instagram':
      return instagramLogo
    case 'google':
      return googleLogo
    case 'yelp':
      return yelpLogo
    case 'tripadvisor':
      return tripLogo
    case 'opentable':
      return opentableLogo
    default:
      return ''
  }
}
iconResolver.getPlatformIconByRepresentation = representation => iconResolver.getPlatformIconByName(representation.platform)
iconResolver.getPlatformColorByName = (name) => {
  switch (name.toLowerCase()) {
    case 'facebook':
      return '#4267b2'
    case 'instagram':
      return '#000'
    case 'google':
      return '#cd1f20'
    case 'yelp':
      return '#ed372c'
    case 'tripadvisor':
      return '#009c75'
    case 'opentable':
      return '#da3743'
    default:
      return ''
  }
}

export default iconResolver

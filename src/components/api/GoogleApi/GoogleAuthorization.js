import axios from 'axios'

export default class {
  constructor() {
    this.initialized = false
    this.loaded = false
    this.authInstance = null
    this.options = {
      apiKey: process.env.FIREBASE_API_KEY,
      client_id: process.env.GOOGLE_CLIENT_ID,
      scope: process.env.GOOGLE_SCOPES,
      discoveryDocs: process.env.GOOGLE_DISCOVERY_DOCS.split(' '),
    }
    const initGapi = () => {
      window.gapi.load('auth2', () => {
        this.authInstance = window.gapi.auth2.init(this.options)
      })
    }
    // We load the google API (gapi) in the index.html in an asynchronous way. Usually that loading
    // has already happened, when this constructor is called and we can start the
    // initialization immediately. If not we register a global callback that is referenced in the
    // loading process.
    if (window.gapi) {
      initGapi()
    } else {
      window.gapiLoaded = initGapi
    }
  }

  isReady() {
    return !!this.authInstance
  }

  getToken() {
    return this.getAuthResponse().then(authResponse => (authResponse ? authResponse.access_token : null))
  }

  getIdToken() {
    return this.getAuthResponse().then(authResponse => (authResponse ? authResponse.id_token : null))
  }

  getOfflineAccessOptions() {
    return {
      scope: this.options.scope,
    }
  }

  getCurrentUser() {
    if (!this.isReady()) throw new Error('Google Auth API not initialized')
    return this.authInstance.currentUser.get()
  }

  getAuthResponse() {
    return this.getCurrentUser().then(user => (user ? user.getAuthResponse() : null))
  }

  isSignedIn() {
    if (!this.isReady()) throw new Error('Google Auth API not initialized')
    return this.authInstance.isSignedIn.get()
  }

  signIn() {
    if (!this.isReady()) throw new Error('Google Auth API not initialized')
    const options = this.getOfflineAccessOptions()
    return this.authInstance.grantOfflineAccess(options).then(response => this.getTokens(response.code))
  }

  getTokens(code) {
    const url = `${process.env.BACKEND_BASE_URL}auth/google/code`
    return axios.post(url, { code }).then(response => response.data)
  }

  signOut() {
    if (!this.isReady()) throw new Error('Google Auth API not initialized')
    return this.authInstance.signOut()
  }
}

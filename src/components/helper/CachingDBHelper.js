import Dexie from 'dexie'

export default class CachingDBHelper {
  static getSchema() {
    return {
      representationStatistics: 'id, userId, representationId, locationId',
    }
  }

  constructor() {
    this.db = new Dexie('respondo_caching_db')
    this.db.version(1).stores(CachingDBHelper.getSchema())
  }
}

import { createLocalVue, createWrapper, mount } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Vuex from 'vuex'
import VueI18n from 'vue-i18n'
import { installPlugins, installPluginsWithRouter, plugins } from '../../src/plugins'
import { messages } from '../../src/lang/de'

// Load styling files
import 'bootstrap/dist/css/bootstrap.css'
import '../../src/assets/sass/paper-dashboard.scss'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import { addGuard, subscribeToStore } from '../../src/routes/router'

window.$ = Cypress.$
window.JQuery = Cypress.$

function setXMLHttpRequest(w) {
  // by grabbing the XMLHttpRequest from app's iframe
  // and putting it here - in the test iframe
  // we suddenly get spying and stubbing 😁
  window.XMLHttpRequest = w.XMLHttpRequest
  return w
}

function setAlert(w) {
  window.alert = w.alert
  return w
}

const parentDocument = window.parent.document
const projectName = Cypress.config('projectName')
const appIframeId = `Your App: '${projectName}'`
export const appIframe = parentDocument.getElementById(appIframeId)
export const specIframe = parentDocument.getElementsByClassName('spec-iframe')[0]

export const spec = {
  get(selector, timeout = 3000, start = new Date(), intervall = 100) {
    const element = specIframe.contentDocument.querySelector(selector)
    if (element) return Promise.resolve(element)
    if (Math.abs(new Date() - start) > timeout) return Promise.resolve(null)
    return new Promise((resolve) => {
      window.setTimeout(() => {
        resolve(spec.get(selector, timeout, start, intervall))
      }, intervall)
    })
  },
}

const copy = (component, map, selector, appendSelector) => {
  const elementsToCopy = map.has(component) ? map.get(component) : document.querySelectorAll(selector)
  if (elementsToCopy && elementsToCopy.length) {
    console.log(`will inject ${elementsToCopy.length} elements`)
    map.set(component, elementsToCopy)
    const parentNode = appIframe.contentDocument.querySelector(appendSelector)
    elementsToCopy.forEach(element => parentNode.appendChild(element))
  }
}

const stylesHeadCache = new Map()
const copyStyles = component => copy(component, stylesHeadCache, 'head style', 'head')
const linksHeadCache = new Map()
const copyHeadLinks = component => copy(component, linksHeadCache, 'head link', 'head')
const linksBodyCache = new Map()
const copyBodyLinks = component => copy(component, linksHeadCache, 'body link', 'body')

export default (component, w, routes, storeOptions) => {
  console.log('starting mounting.')
  const element = appIframe.contentDocument.querySelector('div.container')

  const localVue = createLocalVue()
  console.log('created local vue')
  installPlugins(localVue)

  const options = {
    el: element,
    render: h => h(component),
  }

  if (routes) {
    options.router = new VueRouter({
      routes,
      linkActiveClass: 'active',
    })
    installPluginsWithRouter(localVue, options.router)
  }

  if (storeOptions) {
    localVue.use(Vuex)
    options.store = new Vuex.Store(storeOptions)
  }

  if (routes && storeOptions) {
    addGuard(options.router, options.store)
    subscribeToStore(options.router, options.store)
  }

  // Setup i18n
  const defaultLocale = 'de'
  localVue.use(VueI18n)
  const i18n = new VueI18n({
    locale: defaultLocale,
    fallbackLocale: defaultLocale,
    messages,
  })
  options.i18n = i18n

  const main = new localVue(options)
  console.log('created main')
  if (window.Cypress) {
    window.Cypress.vue = main
    setAlert(w)
    setXMLHttpRequest(w)
  }
  copyStyles(main)
  copyHeadLinks(main)
  copyBodyLinks(main)

  const wrapperOptions = {
    mocks: {
      $route: 'https://localhost:9090',
    },
  }

  const wrapper = createWrapper(main, wrapperOptions)
  console.log('created wrapper')
  const mountedComponent = wrapper.vm.$children[0]
  const componentWrapper = createWrapper(mountedComponent)
  console.log('created compoentn wrapper')
  const thenOnNext = () => new Promise(resolve => wrapper.vm.$nextTick(() => resolve()))
  w.Vue = wrapper.vm
  const result = {
    wrapper,
    mountedComponent,
    componentWrapper,
    $next: fn => wrapper.vm.$nextTick(() => fn()),
    spec: {
      get: selector => new Promise((resolve) => {
        wrapper.vm.$nextTick(() => resolve(spec.get(selector)))
      }),
    },
    type: (selector, text) => {
      const input = componentWrapper.find(selector)
      input.setValue(text)
      return thenOnNext()
    },
    click: (selector) => {
      const clickable = componentWrapper.find(selector)
      clickable.trigger('click')
      return thenOnNext()
    },
    getStore: () => wrapper.vm.$store,
    getState: () => (wrapper.vm.$store ? wrapper.vm.$store.state : null),
  }
  return result
}

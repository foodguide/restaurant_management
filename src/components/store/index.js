import createPersistedState from 'vuex-persistedstate'
import platform from './modules/platform'
import review from './modules/review'
import user from './modules/user'
import location from './modules/location'
import localStorage from './modules/localstorage'
import instagram from './modules/instagram'
import headline from './modules/ui-config'
import googleSearch from './modules/googlesearch'
import contactRequest from './modules/contactrequest'
import facebook from './modules/facebook'
import googlemybusiness from './modules/googlemybusiness'
import statistics from './modules/statistics'
import error from './modules/error'
import payment from './modules/payment'
import blog from './modules/blog'
import crawler from './modules/crawler'
import tooltip from './modules/tooltip'
import progress from './modules/progress'
import pricingplan from './modules/pricingplan'
import connector from './modules/connector'
import hooks from './modules/hooks'
import quickreply from './modules/quickreply'
import backend from './modules/backend'
import admin from './modules/admin'
import event from './modules/event'
import widget from './modules/widget'
import notificationSubscriptions from './modules/notificationSubscriptions'
import tos from './modules/tos'
import replytask from './modules/replytask'

export default {
  modules: {
    user,
    location,
    review,
    platform,
    instagram,
    headline,
    facebook,
    googlemybusiness,
    googleSearch,
    contactRequest,
    localStorage,
    statistics,
    error,
    payment,
    blog,
    crawler,
    tooltip,
    progress,
    pricingplan,
    connector,
    quickreply,
    hooks,
    backend,
    event,
    widget,
    notificationSubscriptions,
    tos,
    replytask
  },
  plugins: [
    createPersistedState({
      key: 'fgrvmngr',
      paths: [
        'localStorage',
      ],
    }),
  ],
}

export const adminStoreOptions = {
  modules: [
    location,
    review,
    platform,
    localStorage,
    error,
    contactRequest,
    crawler,
    pricingplan,
    backend,
    admin,
  ],
}

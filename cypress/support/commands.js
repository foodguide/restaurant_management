import {
  locations as testLocations, user as testUser, reviews as testReviews, statsData as testStatsData,
} from '../fixtures/testdata'
import mountVue, { specIframe, appIframe } from './mountVue'

Cypress.Commands.add('login', () => {
  cy.visit('/')
  cy.window().should('have.property', '__store__')
  return cy.window().then((win) => {
    win.__store__.dispatch('setTestLogin', testUser)
    win.__store__.dispatch('setTestPayment')
    win.__store__.dispatch('setTestLocation', testLocations)
    win.__store__.dispatch('setTestReviews', testReviews)
    win.__store__.dispatch('setTestStatistics', testStatsData)
  })
})

Cypress.Commands.add('setLocations', (locations) => {
  cy.window().then((win) => {
    win.__store__.dispatch('setTestLocation', locations)
  })
})

Cypress.Commands.add('setReviews', (reviews) => {
  cy.window().then((win) => {
    win.__store__.dispatch('setTestReviews', reviews)
  })
})

Cypress.Commands.add('customLogin', (user, locations, reviews) => {
  cy.visit('/')
  cy.window().should('have.property', '__store__')
  cy.window().then((win) => {
    win.__store__.dispatch('setTestLogin', user)
    win.__store__.dispatch('setTestPayment')
    win.__store__.dispatch('setTestLocation', locations)
    win.__store__.dispatch('setTestReviews', reviews)
    win.__store__.dispatch('setTestStatistics', [])
  })
})


Cypress.Commands.add('logout', () => {
  cy.window().then((win) => {
    cy.window().should('have.property', '__store__')
    win.__store__.commit('logout')
    win.__store__.commit('instagramStateReset')
    win.__store__.commit('locationStateReset')
    win.__store__.commit('reviewStateReset')
  })
})

Cypress.Commands.add('unitTest', (component, routes = null, storeOptions = null) => cy.window().then((win) => {
  const destination = Cypress.$(appIframe).offset()
  const width = Cypress.$(appIframe).width()
  const height = Cypress.$(appIframe).height()
  Cypress.$(specIframe).css({
    top: destination.top,
    left: 0,
    width,
    height,
    visibility: 'visible',
    position: 'absolute',
  })
  const unitTest = mountVue(component, win, routes, storeOptions)
  return cy.wrap(unitTest.wrapper.vm).then(() => unitTest)
}))

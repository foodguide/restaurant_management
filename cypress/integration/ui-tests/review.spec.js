import moment from 'moment'
import {
  shop2, user, reviews, locations,
} from '../../fixtures/testdata'

describe('Test Review Page with two reviews', () => {
  before(() => {
    // cy.logout()
    cy.customLogin(user, locations, reviews)
    cy.get('[data-cy=login-button]').eq(0).click()
    cy.get('[data-cy=location-card]').eq(0).click()
  })

  it('hides no reviews text', () => {
    cy.get('[data-cy=no-reviews-text]').should('not.exist')
  })

  it('shows unseen old reviews dialog', () => {
    cy.get('[data-cy=unseen-dialog]').should('be.visible')
  })

  it('closes old review dialog on click', () => {
    cy.contains('Nein').click()
    cy.get('[data-cy=unseen-dialog]').should('not.exist')
  })

  it('shows all reviews on click', () => {
    cy.get('[data-cy=review-card]').should('have.length', 1)
    cy.get('[data-cy=all-button]').click()
    cy.get('[data-cy=review-card]').should('have.length', 2)
  })

  it('calculates statistics correct', () => {
    cy.get('[data-cy=review-count-text]').contains('Anzahl Bewertungen: 2')
    cy.get('[data-cy=review-average-text]').contains('Durchschnittliche Bewertung: 5.0')
    cy.get('[data-cy=statistics-button]').should('be.visible')
  })

  it('displays correct review information', () => {
    cy.get('[data-cy=review-reviewer]').eq(0).contains(reviews[0].reviewer.name)
    cy.get('[data-cy=review-reviewer]').eq(0).should('be.visible')
    cy.get('[data-cy=platform-icon]').eq(0).should('be.visible')
    cy.get('.md-icon.connected.icon-facebook').eq(0).should('be.visible')
    cy.get('[data-cy=rating-icon]').eq(0).should('be.visible')
    cy.get('[data-cy=rating-icon]').eq(0).should('have.length', 1)
    cy.get('[data-cy=review-text1]').eq(0).contains(reviews[0].text)
    cy.get('[data-cy=review-comment]').eq(0).contains(reviews[0].comments[0].message)
    cy.get('[data-cy=review-comment-date]').eq(0).contains(reviews[0].comments[0].from.name)
    moment.locale('de')
    const testDate = moment('2018-07-20, 13:39:42').format('LLL')
    cy.get('[data-cy=review-comment-date]').eq(0).contains(testDate)
    cy.get('[data-cy=review-response]').eq(0).should('be.visible')
    cy.get('[data-cy=review-response]').eq(0).contains(reviews[0].comments[0].message)
    cy.get('[data-cy=review-reviewer]').eq(1).contains('Unbekannter Facebook-Nutzer')
    cy.get('[data-cy=platform-icon]').eq(1).not('not.exist')
    cy.get('.md-icon.connected.icon-facebook').eq(1).not('not.exist')
    cy.get('[data-cy=review-text1]').eq(1).contains(reviews[1].text)
    cy.get('[data-cy=review-comment]').eq(1).should('not.exist')
    cy.get('[data-cy=review-comment-date]').eq(1).should('not.exist')
    cy.get('[data-cy=review-response]').eq(1).should('not.exist')
    cy.get('[data-cy=review-response-field]').should('have.length', 1)
    cy.get('[data-cy=review-response-area]').should('be.empty')
    cy.get('[data-cy=review-response-button]').should('be.disabled')
    cy.get('[data-cy=review-response-area]').type('Antwort')
    cy.get('[data-cy=review-response-button]').not('be.disabled')
  })
})

describe('Test Review Page without reviews', () => {
  before(() => {
    cy.customLogin(user, [shop2], [])
    cy.get('[data-cy=login-button]').eq(0).click()
    cy.get('[data-cy=location-card]').eq(0).click()
  })

  it('shows no reviews text', () => {
    cy.get('[data-cy=no-reviews-text]').should('be.visible')
  })
})

describe('Test Review Page on mobile device', () => {

  before(() => {
    cy.customLogin(user, locations, reviews)
    cy.get('[data-cy=login-button]').eq(0).click()
    cy.get('[data-cy=location-card]').eq(0).click()
  })

  beforeEach(() => {
    cy.viewport('iphone-6')
  })

  it('Shows Mobile review component', () => {
    cy.get('[data-cy=review-mobile-page]').should('be.visible')
  })
})
